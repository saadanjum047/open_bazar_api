@extends('layout.base')
@section('content')
<link rel="stylesheet" href="{{ URL::asset('theme/assets/skins/dropify/css/dropify.css') }}">
<div id="page_content">
     <div id="page_content_inner">

        <h3 class="heading_b uk-margin-bottom">Add Sub-Category</h3>
        <div class="md-card">
            <div class="md-card-content large-padding">
                <form method="post" action="{{ route('addsubcategory') }}" id="form_validation" enctype="multipart/form-data" class="uk-form-stacked task-form">
                	@csrf
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <label for="fullname">Category<span class="req">*</span></label>
                            <div class="parsley-row">
                                <input type="hidden" value="{{ $data->id }}" name="category">
                                <input type="text" value="{{ $data->en_name }}" name="category_name" class="md-input task" readonly>
                                {{-- <select id="select_demo_4" name="category" data-md-selectize >
                                    <option value="">Select Category</option>
                                    @foreach($data as $key)
                                    <option value="{{ $key->id }}">{{ $key->en_name }}</option>
                                    @endforeach
                                </select> --}}
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label for="fullname">Sub-Category Name<span class="req">*</span></label>
                            <div class="parsley-row">
                                <input type="text"   name="en_name"  autocomplete="off" placeholder="Enter sub-category name in English" required class="md-input task" />
                            </div>
                        </div>
                    {{--  </div>
                    <div class="uk-grid" data-uk-grid-margin>  --}}
                        <div class="uk-width-medium-1-2">
                            <label for="fullname">Sub-Category Name<span class="req">*</span></label>
                            <div class="parsley-row">
                                <input type="text"   name="ar_name"  autocomplete="off" placeholder="Enter sub-category name in Dutch" required class="md-input task" />
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label for="fullname">Position<span class="req">*</span></label>
                            <div class="parsley-row">
                                <input type="text"   name="position" autocomplete="off" placeholder="Enter position of sub-category" required class="md-input task" />
                            </div>
                        </div>

                        {{--  dir="rtl"  --}}
                        <div class="uk-width-medium-1-2">
                            <div class="md-card">
                                <div class="md-card-content">
                                    <h3 class="heading_a uk-margin-small-bottom">
                                        Upload Image<span class="req">*</span>
                                    </h3>
                                    <input type="file" id="input-file-a" class="dropify" name="image" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                        <!-- accept=".png, .jpg, .jpeg" -->

                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <button type="submit" class="md-btn" style="background-color:#516A7C;color: white;">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection