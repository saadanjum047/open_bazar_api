@extends('layout.base')
@section('content')
    <style>
        .uk-pagination>li.uk-active>a, .uk-pagination>li.uk-active>span{background: #516A7C;}
        .icheck-inline{margin: 0px !important;} .modal-product{width: 66% !important;}
    </style>
    <div id="page_content">
        <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
            <h1>{{ ucfirst($data->productname) }}</h1>
            <span class="uk-text-muted uk-text-upper uk-text-small">@if($data->main_category == 1) Sell Your Stuff @elseif($data->main_category == 2) Rent Your Stuff @elseif($data->main_category == 3) Offer Services @else Start Your Shop @endif - {{ ucfirst($data->category_name) }} - {{ ucfirst($data->subcategory_name) }}</span>
        </div>
        <div id="page_content_inner">

            <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
                <div class="uk-width-xLarge-2-10 uk-width-large-3-10">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text">
                                Photos
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <div class="uk-margin-bottom uk-text-center">
                                <img src="{{ URL::asset('/public/images/'.$data->image) }}" alt="" class="img_medium" />
                            </div>
                        </div>
                    </div>
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text">
                                Basic Details
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <div class="uk-width-large-1-1">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Main Category</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <span class="uk-text-large uk-text-middle">@if($data->main_category == 1) Sell Your Stuff @elseif($data->main_category == 2) Rent Your Stuff @elseif($data->main_category == 3) Offer Services @else Start Your Shop @endif</span>
                                    </div>
                                </div>

                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Category</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <span class="uk-text-large uk-text-middle">{{ ucfirst($data->category_name) }}</span>
                                    </div>
                                </div>

                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Sub-Category</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <span class="uk-text-large uk-text-middle">{{ ucfirst($data->subcategory_name) }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="uk-width-xLarge-8-10  uk-width-large-7-10">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text">
                                Details(in English)
                            </h3>
                        </div>
                        <div class="md-card-content large-padding">
                            <div class="uk-grid uk-grid-divider uk-grid-medium">
                                <div class="uk-width-large-1-1">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">Product Name</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle">@if($data->productname!="") {{ $data->productname }} @else NA @endif</span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    @if(sizeof($data->variant)>0)
                                        @foreach($data->variant as $p=>$pro)
                                            @php
                                                $option="";
                                                foreach($pro as $per)
                                                {
                                                    $option = $option.$per.",";
                                                }
                                            @endphp
                                            <div class="uk-grid uk-grid-small">
                                                <div class="uk-width-large-1-3">
                                                    <span class="uk-text-muted uk-text-small">{{ $p }}</span>
                                                </div>
                                                <div class="uk-width-large-2-3">
                                                    <span class="uk-text-large uk-text-middle">@php echo rtrim($option,',');  @endphp</span>
                                                </div>
                                            </div>
                                            <hr class="uk-grid-divider">
                                        @endforeach
                                    @endif
                                    <hr class="uk-grid-divider uk-hidden-large">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text">
                                Details(in Dutch)
                            </h3>
                        </div>
                        <div class="md-card-content large-padding">
                            <div class="uk-grid uk-grid-divider uk-grid-medium">
                                <div class="uk-width-large-1-1">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">Product Name</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle">@if($data->productddname!="") {{ $data->productddname }} @else NA @endif</span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    @if(sizeof($data->variantdd)>0)
                                        @foreach($data->variantdd as $p=>$pro)
                                            @php
                                                $option="";
                                                foreach($pro as $per)
                                                {
                                                    $option = $option.$per.",";
                                                }
                                            @endphp
                                            <div class="uk-grid uk-grid-small">
                                                <div class="uk-width-large-1-3">
                                                    <span class="uk-text-muted uk-text-small">{{ $p }}</span>
                                                </div>
                                                <div class="uk-width-large-2-3">
                                                    <span class="uk-text-large uk-text-middle">@php echo rtrim($option,',');  @endphp</span>
                                                </div>
                                            </div>
                                            <hr class="uk-grid-divider">
                                        @endforeach
                                    @endif
                                    <hr class="uk-grid-divider uk-hidden-large">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary" href="{{ route('edit_product',['id'=>encrypt($data->id)]) }}" style="background-color:#516A7C;color:white;">
            <i class="material-icons">&#xE150;</i>
        </a>
    </div>
    {{--  ecommerce_product_edit.html  --}}
@endsection
