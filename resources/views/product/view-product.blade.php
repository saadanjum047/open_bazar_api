@extends('layout.base')
@section('content')
<style>
    .uk-pagination>li.uk-active>a, .uk-pagination>li.uk-active>span{background: #516A7C;}
    .icheck-inline{margin: 0px !important;} .modal-product{width: 66% !important;}
    .product-buttons{background-color:#516A7C;color:white;}
</style>
<div id="page_content">
    <div id="page_content_inner">
        <h4 class="heading_a uk-margin-bottom">View Product</h4>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        {{--  <a class="md-btn md-btn-small md-btn-wave-light md-btn-icon waves-effect waves-button waves-light add_button" title="Add Product" style="background-color:#516A7C;color:white;" href="{{ route('add-product') }}">
                            <i class="uk-icon-plus-circle no_margin" style="color:white;"></i>
                        </a>  --}}
                        {{--  <a href="javascript:void(0)" class="md-btn md-btn-small md-btn-wave-light md-btn-icon waves-effect waves-button waves-light delete_all"  title="Multiple Delete Products" style="background-color:#516A7C;color:white;">
                            <i class="uk-icon-trash no_margin" style="color:white;"></i>
                        </a>  --}}
                        <a href="javascript:void(0)"  data-uk-modal="{target:'#modal_del'}"  class="md-btn md-btn-small md-btn-wave-light md-btn-icon waves-effect waves-button waves-light product-buttons"  title="Multiple Delete Products">
                            <i class="uk-icon-trash no_margin" style="color:white;"></i>
                        </a>
    {{--  data-uk-modal="{target:'#modal_del'}"  --}}
                        <div class="uk-modal" id="modal_del">
                            <div class="uk-modal-dialog">
                                <div class="uk-modal-header">
                                    <h3 class="uk-modal-title">Are you sure you want to delete selected products? </h3>
                                </div>
                                <div class="uk-modal-content">
                                    <a href="javascript:void(0)" class="md-btn md-btn-small md-btn-wave-light md-btn-icon waves-effect waves-button waves-light delete_all" title="Multiple Delete Products" style="background-color:#516A7C;color:white;">
                                        Yes
                                    </a>
                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">No</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="uk-overflow-container">
                    <table id="dt_default" class="uk-table uk-table-striped uk-table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>
                                <span class="icheck-inline">
                                    <input type="checkbox" class="ts_checkbox_all" name="checkbox_demo_inline_mercury" id="checkbox_demo_inline_1" data-md-icheck />
                                </span>
                            </th>
                            <th> Image </th>
                            <th>Main Category</th>
                            <th>Category</th>
                            <th>Sub-Category</th>
                            <th>Product</th>
                            <th>Publish&nbsp;/<br>Unpublish</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(!$data->isEmpty())
                                @foreach($data as $key)
                                    <tr>
                                        <td>
                                            <span class="icheck-inline">
                                                <input type="checkbox" name="checkbox_demo_inline_mercury" class="sub_chk ts_checkbox" data-id="{{$key->id}}" id="checkbox_demo_inline_1" data-md-icheck />
                                            </span>
                                        </td>
                                        <td>
                                            <img src="{{URL::asset('/public/images/'.$key->image)}}" height="30" width="70" />
                                        </td>
                                        <td>
                                            @if($key->main_category==1)
                                                <span class="uk-badge">Sell Your Stuff</span>
                                            @elseif($key->main_category==2)
                                                <span class="uk-badge uk-badge-danger">Rent Your Stuff</span>
                                            @elseif($key->main_category==3)
                                                <span class="uk-badge uk-badge-primary">Offer Services</span>
                                            @else
                                                <span class="uk-badge uk-badge-warning">Start Your Shop</span>
                                            @endif
                                        </td>
                                        <td> {{ $key->category_name }} </td>
                                        <td> {{ $key->subcategory_name }} </td>
                                        <td>
                                            @if($key->product=="")
                                                NA
                                            @else
                                                {{ ucfirst( $key->productname ) }}
                                            @endif
                                        </td>
                                        <td> 
                                            <input type="checkbox" data-switchery data-switchery-color="#d32f2f" onchange="del({{$key->id}},{{$key->publish}})" @if($key->publish==1) checked @endif   id="switch_demo_danger"/>
                                        </td>
                                        <td>
                                            <a href="{{ route('view_product_details',['id'=>encrypt($key->id)]) }}" title="View" ><i class="uk-icon-eye uk-icon-small"></i></a>
                                            &nbsp;&nbsp;

                                            <a href="{{ route('edit_product',['id'=>encrypt($key->id)]) }}" title="EDIT"><i class="uk-icon-edit uk-icon-small"></i></a>
                                            &nbsp;&nbsp;
                                            
                                            <a href="#" onclick="UIkit.modal.confirm('Are you sure you want to delete product? ', function(){ 
                                                var url = '{{ route("delete_product", ":slug") }}';
                                                url = url.replace(':slug', '{{$key->id}}');
                                                window.location.href=url;
                                                });" title="DELETE">
                                                <i class="uk-icon-trash uk-icon-small"></i>
                                            </a>

                                            <br><br>

                                            <a href="{{ route('clone_product',['id'=>encrypt($key->id)]) }}" title="Clone Product" class="md-btn md-btn-small md-btn-wave-light md-btn-icon waves-effect waves-button waves-light product-buttons">Clone</a>
                                        </td>
                                    </tr>

                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6"> No Record Found</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="md-fab-wrapper">
    <a class="md-fab md-fab-primary" href="{{ route('add-product') }}" style="background-color:#516A7C;color:white;">
        <i class="uk-icon-plus"></i>
    </a>
</div>
<script>
    function del(id,status)
    {
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "POST",
            url: "{{ route('status_product') }}",
            data: {id: id,'_token': '{!!csrf_token()!!}'},
            cache: false,

            success :function(data)
            {
                if(status==1)
                {
                    if(data.message=="success")
                    {
                        UIkit.notify({ message: 'Unpublished Successfully!!..', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    else if(data.message=="warning")
                    {
                        UIkit.notify({ message: 'Something Went Wrong!!..', status: 'warning', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    else
                    {
                        UIkit.notify({ message: 'Category Not Found!!..', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    location.reload();
                }
                else
                {
                    if(data.message=="success")
                    {
                        UIkit.notify({ message: 'Published Successfully', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    else if(data.message=="warning")
                    {
                        UIkit.notify({ message: 'Something Went Wrong', status: 'warning', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    else
                    {
                        UIkit.notify({ message: 'Category Not Found', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    location.reload();
                }
            },
            error: function () 
            {
                }
        });
    }
</script>
<script src="{{ URL::asset('theme/assets/js/common.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".sub_chk").prop('checked', false);

        $('.ts_checkbox_all')
            .iCheck({
                checkboxClass: 'icheckbox_md',
                radioClass: 'iradio_md',
                increaseArea: '20%'
            })
            .on('ifChecked',function() {
                $('#dt_default')
                    .find('.ts_checkbox')
                    .prop('checked',true)
                    .iCheck('update')
                    .closest('tr')
                    .addClass('row_highlighted');
            })
            .on('ifUnchecked',function() {
                $('#dt_default')
                    .find('.ts_checkbox')
                    .prop('checked',false)
                    .iCheck('update')
                    .closest('tr')
                    .removeClass('row_highlighted');
            });
        $('.delete_all').on('click', function (e) {
            var allVals = [];
            $(".sub_chk:checked").each(function () {
                allVals.push($(this).attr('data-id'));
            });
            var join_selected_values = allVals;
            console.log(join_selected_values);
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: 'POST',
                url: "{{ route('product_delete_multiple') }}",
                dataType: "JSON",
                data: {"_token": "{{ csrf_token() }}",id: join_selected_values},
                success: function (data) {
                    $(".sub_chk:checked").each(function () {
                        $(this).parents("tr").remove();
                    });
                    var modal = UIkit.modal("#modal_del");

                    if ( modal.isActive() ) {
                        modal.hide();
                    }
                   // $('#modal_del').modal('false');
                    UIkit.notify({ message: 'Selected Product Deleted Successfully!!..', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                },
                error: function (data) {
                    UIkit.notify({ message: 'Something Went Wrong!!..', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                }
            });
  
            $.each(allVals, function (index, value) {
                $('table tr').filter("[data-row-id='" + value + "']").remove();
            });
        });
    });
</script>
@endsection