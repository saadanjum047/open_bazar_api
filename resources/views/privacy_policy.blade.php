<html>
    <head>
        <title>OpenBazar</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-offset-1 col-lg-10">
                    <div class="card">
                        {{--  <div class="card-header">
                            <img src="{{ URL::asset('public/Logo.png') }}" alt="" height="45" width="150" style="margin-left: 40%;">
                        </div>
                        <br>  --}}
                        <div class="card-body">
                            <h3 class="card-title text-center">Privacy Policy</h3>
                            <p>This policy describes Open Bazar Privacy Policy for the collection, use, storage, sharing and protection of your personal information. The same policy applies to the Website, and all sites, applications, services and tools related to the Open Bazar, regardless of how these services are accessed.</p>
                            <p>When you use the Open Bazar and any related services, you expressly grant the Site the right to collect, use and retain your personal information, as described in our Privacy Policy and Terms of Use for the Open Bazar.</p>
                            <p>You can visit the Open Bazar site without registering a personal account. When you decide to provide the Site with your personal information, you agree that such information will be sent and stored in its own servers.</p>
                            <p>When you visit the site or use the services provided by the site or to users in general, or respond to ads or other existing content, the site will collect information sent from your device automatically, whether a computer or mobile phone. This information includes:</p>
                            <ul>
                                <li>The information collected through your interaction on the site, which includes: device number, device type, location information, internet contact information, page view statistics, site visits, return links, ad data, protocol address and Internet access information.</li>
                                <li>The information collected by the site through cookies (trace logs), Internet players and similar technologies.</li>
                            </ul>
                            <p>Open Bazar collects any information that you enter on the site or that you provide when you use any of its services. This information includes: </p>
                            <ul>
                                <li>The information you provide to the Open Bazar when registering or using the services.</li>
                                <li>Additional information you provide to the Open Bazar through social media platforms or third party services.</li>
                                <li>Information provided in the course of settlement of disputes, correspondence via the site and messages sent to him/her.</li>
                                <li>Information about your location and location of your device; and your information ID whether you have already done this service on your mobile phone.</li>
                            </ul>
                            <p>Open Bazar may receive or collect additional information about you through third parties and add it to your account information. This information includes: demographic data, navigation data, contact data and additional data about you from other sources such as public authorities to the extent permitted by law.</p>
                            <p>You agree that the Open Bazar will use your personal information for the following purposes:</p>
                            <ul>
                                <li>To enable you to access his/her various services and customer service by email or phone number.</li>
                                <li>To prevent, detect and investigate when there is a possibility of prohibited activities where fraud and violation of the law and non-compliance with the terms of use of the site.</li>
                                <li>To customize, measure and improve Open Bazar services, content and advertisements.</li>
                                <li>To communicate with you via email, notifications, messages or telephone to inquire about open Bazar services for marketing purposes, updates and promotions based on message priorities (if any) or for any purposes as set forth in this Privacy Policy.</li>
                                <li>To provide more of the other services you have requested, as described in the information gathering mechanism on the site.</li>
                                <li>To be considered a potential candidate when you upload your CV on the Jobs page.</li>
                            </ul>
                            <p>Open Bazar may share your personal information:</p>
                            <ul>
                                    <li>To comply with legal obligations or a court order.</li>
                                    <li>If necessary to prevent, detect or protect against criminal offenses, such as fraud, fraud and lawsuits.</li>
                                    <li>If necessary to maintain our policy or to protect the rights and freedoms of others.</li>
                                </ul>
                        </div>
                        <div class="card-footer">
                            <h5>Open Bazar<h5>
                            <p>@All Rights Reserved 2017-2018 OpenBazar.nl</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>


