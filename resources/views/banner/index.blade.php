@extends('layout.base')
@section('content')
<style>
    .uk-pagination>li.uk-active>a, .uk-pagination>li.uk-active>span{background: #516A7C;}
</style>
<div id="page_content">
     <div id="page_content_inner">
        <h4 class="heading_a uk-margin-bottom">View Banners</h4>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <a class="md-btn md-btn-small md-btn-wave-light md-btn-icon waves-effect waves-button waves-light add_button" title="Add Category" style="background-color:#516A7C;color:white;" href="{{ route('add-banner') }}">
                            <i class="uk-icon-plus-circle no_margin" style="color:white;"></i>
                        </a>
                    </div>
                </div>
                <br>
                <div class="uk-overflow-container">
                    <table id="dt_default" class="uk-table uk-table-striped uk-table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Image</th>
                            <th>Position</th>
                           
                            <th>Action</th>
                        </tr>
                        </thead>
                         <tbody>
                            @if(!$data->isEmpty())
                                @foreach($data as $key)
                                    <tr>
                                    <td>
                                            <img src="{{ URL::asset('/public/images/banner/'.$key->banner_path) }}" height="60" width="70">
                                        </td>
                                        
                                        <td> {{ $key->banner_sortid }} </td>
                                       
                                       
                                       
                                        <td> 
                                            <a href="javascript:void(0);" title="View" data-uk-modal="{target:'#modal_default{{$key->bannerid}}'}"><i class="uk-icon-eye uk-icon-small"></i></a>
                                            &nbsp;&nbsp;

                                            <a href="{{ route('edit-banner',['id'=>encrypt($key->bannerid)]) }}" title="EDIT"><i class="uk-icon-edit uk-icon-small"></i></a>
                                            &nbsp;&nbsp;
                                            
                                            <a href="#" onclick="UIkit.modal.confirm('Are you sure you want to delete banner? ', function(){ 
                                                    var url = '{{ route("deletebanner", ":slug") }}';

                                                    url = url.replace(':slug', '{{$key->bannerid}}');

                                                    window.location.href=url;
                                                 });" title="DELETE">

                                                 <i class="uk-icon-trash uk-icon-small"></i>
                                            </a>
                                        </td>
                                    </tr>
                               
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">No Record Found</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="md-fab-wrapper">
    <a class="md-fab md-fab-primary" href="{{ route('add-category') }}" style="background-color:#516A7C;color:white;">
        <i class="uk-icon-plus"></i>
    </a>
</div>
<script>
    function del(id,status)
    {
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "POST",
            url: "{{ route('status_category') }}",
            data: {id: id,'_token': '{!!csrf_token()!!}'},
            cache: false,

            success :function(data)
            {
                if(status==1)
                {
                    if(data.message=="success")
                    {
                        UIkit.notify({ message: 'Unpublished Successfully!!..', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    else if(data.message=="warning")
                    {
                        UIkit.notify({ message: 'Something Went Wrong!!..', status: 'warning', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    else
                    {
                        UIkit.notify({ message: 'Category Not Found!!..', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    location.reload();
                }
                else
                {
                    if(data.message=="success")
                    {
                        UIkit.notify({ message: 'Published Successfully', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    else if(data.message=="warning")
                    {
                        UIkit.notify({ message: 'Something Went Wrong', status: 'warning', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    else
                    {
                        UIkit.notify({ message: 'Category Not Found', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    location.reload();
                }
            },
            error: function () 
            {
                }
        });
    }
</script>
@endsection