@extends('layout.base')
@section('content')
    <link rel="stylesheet" href="{{ URL::asset('theme/assets/skins/dropify/css/dropify.css') }}">
    <style>
        .label-visible{ font-size: 10px !important; }
    </style>
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom">Add Banner</h3>
            <div class="md-card">
                <div class="md-card-content large-padding">
                    <form method="post" action="{{ route('add-banner') }}" id="form_validation" enctype="multipart/form-data" class="uk-form-stacked task-form">
                        @csrf
                     
                      
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="md-card">
                                    <div class="md-card-content">
                                        <h3 class="heading_a uk-margin-small-bottom">
                                            Upload Image
                                        </h3>
                                        <input type="file" id="input-file-a" class="dropify" name="image" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="md-card">
                                    <div class="md-card-content">
                                        <h3 class="heading_a uk-margin-small-bottom">
                                           Sort number
                                        </h3>
                                        <input type="number" id="input-file-a"  name="sortnumber" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-grid">
                            <div class="uk-width-1-1">
                                <button type="submit" class="md-btn" style="background-color:#516A7C;color: white;">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ URL::asset('theme/assets/js/common.min.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <script type="text/javascript">
        function add_Field()
        {
            var value=$('#inc').val();
            var newid=parseInt(value)+1;
            var wrapper = $('.field_wrapper');
            if(newid>0){
                var fieldHTML ='<div class="row p-t-20">  <div class="uk-grid" data-uk-grid-margin>  <div class="uk-width-medium-1-2">  <label for="fullname">Label<span class="req">*</span></label>  <div class="parsley-row"> <input type="text"  name="label_'+newid+'" id="label_'+newid+'" placeholder="Enter label in English" required class="md-input task" /> </div> </div> <div class="uk-width-medium-1-2"> <label for="fullname">Label<span class="req">*</span></label> <div class="parsley-row"> <input type="text"   name="labeldd_'+newid+'" id="labeldd_'+newid+'" placeholder="Enter label in Dutch" required class="md-input task" /> </div> </div> <div class="uk-width-medium-1-2"> <label for="fullname">Variant<span class="req">*</span>&nbsp;&nbsp;&nbsp;</label> <div class="parsley-row"> <input type="text"  name="variant_'+newid+'" id="variant_'+newid+'" placeholder="Enter variant in English" required class="md-input task" /> <span class="label-visible"><i class="uk-icon-info-circle"></i><i>For multiple values enter it in comma separated</i></span> </div> </div> <div class="uk-width-medium-1-2"> <label for="fullname">Variant<span class="req">*</span>&nbsp;&nbsp;&nbsp;</label> <div class="parsley-row"> <input type="text"   name="variantdd_'+newid+'" id="variantdd_'+newid+'" placeholder="Enter variant in Dutch" required class="md-input task" /> <span class="label-visible"><i class="uk-icon-info-circle"></i><i>For multiple values enter it in comma separated</i></span> </div> </div> <a href="javascript:void(0);" class="remove_button set" style="float:right;" title="Remove badge"><i class="uk-icon-trash"></i></a> </div> <hr class="form_hr"><hr class="form_hr"> </div>';  
            }else{
                var fieldHTML ='<div class="row p-t-20"> <div class="uk-grid" data-uk-grid-margin> <div class="uk-width-medium-1-2"> <label for="fullname">Toolbar<span class="req">*</span></label> <div class="parsley-row"> <input type="text"  name="label_'+newid+'" id="label_'+newid+'" placeholder="Enter toolbar in English" required class="md-input task" /> </div> </div> <div class="uk-width-medium-1-2"> <label for="fullname">Toolbar<span class="req">*</span></label> <div class="parsley-row"> <input type="text"   name="labeldd_'+newid+'" id="labeldd_'+newid+'" placeholder="Enter toolbar in Dutch" required class="md-input task" /> </div> </div> <div class="uk-width-medium-1-2"> <label for="fullname">Attribute<span class="req">*</span>&nbsp;&nbsp;&nbsp;</label> <div class="parsley-row"> <input type="text"  name="variant_'+newid+'" id="variant_'+newid+'" placeholder="Enter attribute in English" required class="md-input task" /> </div> </div> <div class="uk-width-medium-1-2"> <label for="fullname">Attribute<span class="req">*</span>&nbsp;&nbsp;&nbsp;</label> <div class="parsley-row"> <input type="text"   name="variantdd_'+newid+'" id="variantdd_'+newid+'" placeholder="Enter attribute in Dutch" required class="md-input task" /> </div> <a href="javascript:void(0);" class="remove_button set" style="float:right;" title="Remove badge"><i class="uk-icon-trash"></i></a> </div> </div> <hr class="form_hr"><hr class="form_hr"> </div>';
            }
            $(wrapper).append(fieldHTML); 
            $('#inc').val(newid);
        }

        $(document).ready(function () {
            var wrapper = $('.field_wrapper'); 
            var x = 1;
            $(wrapper).on('click', '.remove_button', function (e) { 
                e.preventDefault();
                $(this).parent().parent('div').remove();
                x--; 
            });
        });
    </script>

    <script>
            function category_get(id){
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: "POST",
                    url: "{{ route('get_category') }}",
                    data: {id: id,'_token': '{!!csrf_token()!!}'},
                    cache: false,
                    success :function(response){
                        if(response.message=='success'){
                            $('#category').selectize();
                            var htmldata = '';
                            var new_value_options   = '[';
                            var htmldata1 = '<option value="">Select Category</option>';
                            var data = response.data;
                            for (var i = 0; i < data.length; i++) { 
                                htmldata += '<option value="'+data[i].id+'">'+data[i].en_name+'</option>';
                                var keyPlus = parseInt(i) + 1;
                                if (keyPlus == data.length) {
                                    new_value_options += '{text: "'+data[i].en_name+'", value: '+data[i].id+'}';
                                } else {
                                    new_value_options += '{text: "'+data[i].en_name+'", value: '+data[i].id+'},';
                                }
                            }
                            new_value_options   += ']';
                            new_value_options = eval('(' + new_value_options + ')');
                            if (new_value_options[0] != undefined) {
                                $("#category").html(htmldata);
                                var selectize = $("#category")[0].selectize;
                                selectize.clear();
                                selectize.clearOptions();
                                selectize.renderCache['option'] = {};
                                selectize.renderCache['item'] = {};
                                selectize.addOption(new_value_options);
                                selectize.setValue(new_value_options[0].value);
                            }
                        }
                        else{
                            UIkit.notify({ message: 'Category related to Main Category Not Found!!..', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                        }
                    },
                    error: function () 
                    {}
                });
            }
        function subcategory_get(id){
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: "POST",
                url: "{{ route('get_subcategory') }}",
                data: {id: id,'_token': '{!!csrf_token()!!}'},
                cache: false,
                success :function(response){
                    if(response.message=='success'){
                        $('#subcategory').selectize();
                        var htmldata = '';
                        var new_value_options   = '[';
                        var htmldata1 = '<option value="">Select Sub-category</option>';
                        var data = response.data;
                        for (var i = 0; i < data.length; i++) { 
                            htmldata += '<option value="'+data[i].id+'">'+data[i].en_name+'</option>';
                            var keyPlus = parseInt(i) + 1;
                            if (keyPlus == data.length) {
                                new_value_options += '{text: "'+data[i].en_name+'", value: '+data[i].id+'}';
                            } else {
                                new_value_options += '{text: "'+data[i].en_name+'", value: '+data[i].id+'},';
                            }
                        }
                        new_value_options   += ']';
                        new_value_options = eval('(' + new_value_options + ')');
                        if (new_value_options[0] != undefined) {
                            $("#subcategory").html(htmldata);
                            var selectize = $("#subcategory")[0].selectize;
                            selectize.clear();
                            selectize.clearOptions();
                            selectize.renderCache['option'] = {};
                            selectize.renderCache['item'] = {};
                            selectize.addOption(new_value_options);
                            selectize.setValue(new_value_options[0].value);
                        }
                    }
                    else{
                        UIkit.notify({ message: 'Sub-Category related to Category Not Found!!..', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                    }
                },
                error: function () 
                {}
            });
        }
    </script>
@endsection
