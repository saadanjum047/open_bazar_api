@extends('layout.base')
@section('content')
    <style>
        .uk-pagination>li.uk-active>a, .uk-pagination>li.uk-active>span{background: #516A7C;}
        .icheck-inline{margin: 0px !important;} .modal-product{width: 66% !important;}
        .img_medium{    width: 240px; max-width: 100%; height: 240px !important;}
    </style>
    <div id="page_content">
        <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
            <h1>{{ ucfirst($data->user_name) }}</h1>
            {{-- <span class="uk-text-muted uk-text-upper uk-text-small">@if($data->main_category == 1) Sell Your Stuff @elseif($data->main_category == 2) Rent Your Stuff @elseif($data->main_category == 3) Offer Services @else Start Your Shop @endif - {{ ucfirst($data->category_name) }} - {{ ucfirst($data->subcategory_name) }}</span> --}}
        </div>
        <div id="page_content_inner">

            <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
                <div class="uk-width-xLarge-2-10 uk-width-large-3-10">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text">
                                Photos
                            </h3>
                        </div>
                        <div class="md-card-content">
                            @if($data->images=="")
                                <div class="uk-margin-bottom uk-text-center">
                                    <img src="{{ URL::asset('/public/no-image.png') }}" alt="" class="img_medium" />
                                </div>
                            @else
                                @php
                                    $image = json_decode($data->images);
                                @endphp
                                <div class="uk-margin-bottom uk-text-center">
                                    <ul class="uk-slideshow" data-uk-slideshow="{autoplay:true,kenburns:true}">
                                        @foreach($image as $key )
                                            <li><img src="{{ URL::asset('/public/images/'.$key->image) }}" class="img_medium" alt=""></li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="uk-width-xLarge-8-10  uk-width-large-7-10">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text">
                                Ads Details
                            </h3>
                        </div>
                        <div class="md-card-content large-padding">
                            <div class="uk-grid uk-grid-divider uk-grid-medium">
                                <div class="uk-width-large-1-1">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">Ad Title</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle"> <b> @if($data->ad_title!="") {{ ucfirst($data->ad_title) }} @else NA @endif </b> </span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">

                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">Ad Description</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle"> <b> @if($data->ad_description!="") {{ ucfirst($data->ad_description) }} @else NA @endif </b> </span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">

                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">Mobile No</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle"> <b> @if($data->mobile!="") {{ $data->mobile }} @else NA @endif </b> </span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">

                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">Price</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle"> <b> @if($data->price!="") {{ $data->price }} @else NA @endif </b> </span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">

                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">Price Type</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle"> <b> @if($data->price_type!="")  @if($data->price_type=="price") <span class="uk-badge uk-badge-danger"> {{ ucfirst($data->price_type) }} </span> @else <span class="uk-badge uk-badge-primary"> {{ ucfirst($data->price_type) }} </span> @endif  @else NA @endif </b> </span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">

                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">Start Date</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle"> <b> @if($data->timestamp!="") {{ date('d-m-Y',($data->timestamp/1000)) }} @else NA @endif </b> </span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">

                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">Expiry Date</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle"> <b> @if($data->expired_date!="") {{ date('d-m-Y',$data->expired_date) }} @else NA @endif </b> </span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">

                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">Block Status</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle"> <b> @if($data->block_status == 1) <span class="uk-badge uk-badge-danger"> Blocked </span>  @else <span class="uk-badge uk-badge-success"> Unblocked </span> @endif </b> </span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">

                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">Activation Status</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle"> <b> @if($data->activation_status == 1) <span class="uk-badge uk-badge-danger"> Deactivated </span> @else  <span class="uk-badge uk-badge-success"> Activated </span> @endif </b> </span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">

                                    <hr class="uk-grid-divider uk-hidden-large">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text">
                                Other Details
                            </h3>
                        </div>
                        <div class="md-card-content large-padding">
                            <div class="uk-grid uk-grid-divider uk-grid-medium">
                                <div class="uk-width-large-1-1">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">Main Category</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle"> <b> @if($data->main_category == 1) Sell Your Stuff @elseif($data->main_category == 2) Rent Your Stuff @elseif($data->main_category == 3) Offer Services @else Start Your Shop @endif </b></span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    @if($data->category !="")
                                        @php $cat = json_decode($data->category); @endphp
                                        @foreach($cat as $pro)
                                            <div class="uk-grid uk-grid-small">
                                                <div class="uk-width-large-1-3">
                                                    <span class="uk-text-muted uk-text-small">{{ $pro->tital }}</span>
                                                </div>
                                                <div class="uk-width-large-2-3">
                                                    <span class="uk-text-large uk-text-middle">{{ $pro->seletecvalue }}</span>
                                                </div>
                                            </div>
                                            <hr class="uk-grid-divider">
                                        @endforeach
                                    @endif

                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">City</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle"> <b> @if($data->city!="") {{ $data->city }} @else NA @endif </b></span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    <hr class="uk-grid-divider uk-hidden-large">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{-- <div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary" href="{{ route('edit_product',['id'=>encrypt($data->id)]) }}" style="background-color:#516A7C;color:white;">
            <i class="material-icons">&#xE150;</i>
        </a>
    </div> --}}
    {{--  ecommerce_product_edit.html  --}}
@endsection
