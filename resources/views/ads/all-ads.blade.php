@extends('layout.base')
@section('content')
<style>
    .uk-pagination>li.uk-active>a, .uk-pagination>li.uk-active>span{background: #516A7C;}
    .icheck-inline{margin: 0px !important;} .modal-product{width: 66% !important;}
    .product-buttons{background-color:#516A7C;color:white;}
        .data-description{
        width: 30em;
        display: inline-block !important;
        overflow: hidden !important;
        white-space: nowrap !important;
        text-overflow: ellipsis !important;
        }
        .dataTables_wrapper .uk-overflow-container td{
          //  white-space: normal !important;
        overflow: hidden !important;
          white-space: nowrap !important;
          text-overflow: ellipsis !important;
            border-bottom: none !important;
        }
        
</style>
<div id="page_content">
    <div id="page_content_inner">
        <h4 class="heading_a uk-margin-bottom">View Ads</h4>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <a href="javascript:void(0)"  data-uk-modal="{target:'#modal_del'}"  class="md-btn md-btn-small md-btn-wave-light md-btn-icon waves-effect waves-button waves-light product-buttons"  title="Multiple Delete Products">
                            <i class="uk-icon-trash no_margin" style="color:white;"></i>
                        </a>
                        <div class="uk-modal" id="modal_del">
                            <div class="uk-modal-dialog">
                                <div class="uk-modal-header">
                                    <h3 class="uk-modal-title">Are you sure you want to delete selected ads? </h3>
                                </div>
                                <div class="uk-modal-content">
                                    <a href="javascript:void(0)" class="md-btn md-btn-small md-btn-wave-light md-btn-icon waves-effect waves-button waves-light delete_all" title="Multiple Delete Products" style="background-color:#516A7C;color:white;">
                                        Yes
                                    </a>
                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">No</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="uk-overflow-container1">
                    <table id="dt_default" class="uk-table uk-table-striped uk-table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>
                                <span class="icheck-inline">
                                    <input type="checkbox" class="ts_checkbox_all" name="checkbox_demo_inline_mercury" id="checkbox_demo_inline_1" data-md-icheck />
                                </span>
                            </th>
                            <th> User Name </th>
                            <th> Title </th>
                            <th> Description </th>
                            <th> Price </th>
                            <th> Start Date </th>
                            <th> Expiry Date </th>
                            <th> Ads Status </th>
                            <th> Block <br> Unblock </th>
                            <th> Activation <br> Deactivation </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(!$data->isEmpty())
                                @foreach($data as $key)
                                    <tr>
                                        <td>
                                            <span class="icheck-inline">
                                                <input type="checkbox" name="checkbox_demo_inline_mercury" class="sub_chk ts_checkbox" data-id="{{$key->id}}" id="checkbox_demo_inline_1" data-md-icheck />
                                            </span>
                                        </td>
                                        <td> {{ $key->user_name }} </td>
                                        <td> {{ $key->ad_title }} </td>
                                        <td  class="data-description"> {{ $key->ad_description }} </td>
                                        <td> {{ $key->price }} </td>
                                        {{-- <td> {{ date('d-m-Y',$key->timestamp) }} </td> --}}
                                        <td> {{ date('d-m-Y',($key->timestamp/1000)) }} </td>
                                        <td> {{ date('d-m-Y',$key->expired_date) }} </td>
                                        <td> 
                                            @if($key->mobile_verify=="true") 
                                                <span class="uk-badge md-bg-purple-600"> Live </span> 
                                            @else
                                                <span class="uk-badge md-bg-pink-600"> Pending </span>
                                            @endif
                                        </td>
                                        <td>
                                            <input type="checkbox" data-switchery data-switchery-color="#d32f2f" onchange="block_status({{$key->id}},{{$key->block_status}})" @if($key->block_status==0) checked @endif   id="switch_demo_danger"/>
                                        </td>
                                        <td>
                                            <input type="checkbox" data-switchery data-switchery-color="#d32f2f" onchange="activation_status({{$key->id}},{{$key->activation_status}})" @if($key->activation_status==0) checked @endif   id="switch_demo_danger"/>
                                        </td>
                                        <td>
                                            <a href="{{ route('ads_details',['id'=>encrypt($key->id)]) }}" title="View" ><i class="uk-icon-eye uk-icon-small"></i></a>
                                            &nbsp;&nbsp;
                                            
                                            <a href="#" onclick="UIkit.modal.confirm('Are you sure you want to delete ads? ', function(){ 
                                                var url = '{{ route("delete_ads", ":slug") }}';
                                                url = url.replace(':slug', '{{$key->id}}');
                                                window.location.href=url;
                                                });" title="DELETE">
                                                <i class="uk-icon-trash uk-icon-small"></i>
                                            </a>
                                        </td>
                                    </tr>

                                @endforeach
                            @else
                                <tr>
                                    <td colspan="10"> No Record Found</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function block_status(id,status){
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "POST",
            url: "{{ route('ad_block_status') }}",
            data: {id: id,'_token': '{!!csrf_token()!!}'},
            cache: false,

            success :function(data){
                if(status==0){
                    if(data.message=="success"){
                        UIkit.notify({ message: 'Blocked Successfully!!..', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                    }else if(data.message=="warning"){
                        UIkit.notify({ message: 'Something Went Wrong!!..', status: 'warning', timeout: 5000, group: null, pos: 'top-center' });
                    }else{
                        UIkit.notify({ message: 'Ads Not Found!!..', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    location.reload();
                }else{
                    if(data.message=="success"){
                        UIkit.notify({ message: 'Unblocked Successfully', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                    }else if(data.message=="warning"){
                        UIkit.notify({ message: 'Something Went Wrong', status: 'warning', timeout: 5000, group: null, pos: 'top-center' });
                    }else{
                        UIkit.notify({ message: 'Ads Not Found', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    location.reload();
                }
            },
            error: function () 
            {}
        });
    }

    function activation_status(id,status){
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "POST",
            url: "{{ route('ad_activation_status') }}",
            data: {id: id,'_token': '{!!csrf_token()!!}'},
            cache: false,

            success :function(data){
                if(status==0){
                    if(data.message=="success"){
                        UIkit.notify({ message: 'Deacticated Successfully!!..', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                    }else if(data.message=="warning"){
                        UIkit.notify({ message: 'Something Went Wrong!!..', status: 'warning', timeout: 5000, group: null, pos: 'top-center' });
                    }else{
                        UIkit.notify({ message: 'Ads Not Found!!..', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    location.reload();
                }else{
                    if(data.message=="success"){
                        UIkit.notify({ message: 'Acticated Successfully', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                    }else if(data.message=="warning"){
                        UIkit.notify({ message: 'Something Went Wrong', status: 'warning', timeout: 5000, group: null, pos: 'top-center' });
                    }else{
                        UIkit.notify({ message: 'Ads Not Found', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    location.reload();
                }
            },
            error: function () 
            {}
        });
    }
</script>
<script src="{{ URL::asset('theme/assets/js/common.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".sub_chk").prop('checked', false);

        $('.ts_checkbox_all')
            .iCheck({
                checkboxClass: 'icheckbox_md',
                radioClass: 'iradio_md',
                increaseArea: '20%'
            })
            .on('ifChecked',function() {
                $('#dt_default')
                    .find('.ts_checkbox')
                    .prop('checked',true)
                    .iCheck('update')
                    .closest('tr')
                    .addClass('row_highlighted');
            })
            .on('ifUnchecked',function() {
                $('#dt_default')
                    .find('.ts_checkbox')
                    .prop('checked',false)
                    .iCheck('update')
                    .closest('tr')
                    .removeClass('row_highlighted');
            });
        $('.delete_all').on('click', function (e) {
            var allVals = [];
            $(".sub_chk:checked").each(function () {
                allVals.push($(this).attr('data-id'));
            });
            var join_selected_values = allVals;
            console.log(join_selected_values);
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: 'POST',
                url: "{{ route('ads_delete_multiple') }}",
                dataType: "JSON",
                data: {"_token": "{{ csrf_token() }}",id: join_selected_values},
                success: function (data) {
                    $(".sub_chk:checked").each(function () {
                        $(this).parents("tr").remove();
                    });
                    var modal = UIkit.modal("#modal_del");

                    if ( modal.isActive() ) {
                        modal.hide();
                    }

                        UIkit.notify({ message: 'Selected User Deleted Successfully!!..', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                },
                error: function (data) {
                    var modal = UIkit.modal("#modal_del");
                    if ( modal.isActive() ) {
                        modal.hide();
                    }
                    UIkit.notify({ message: 'Something Went Wrong!!..', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                }
            });
  
            $.each(allVals, function (index, value) {
                $('table tr').filter("[data-row-id='" + value + "']").remove();
            });
        });
    });
</script>
@endsection
