@extends('layout.base')
@section('content')
    <style>
        .uk-pagination>li.uk-active>a, .uk-pagination>li.uk-active>span{background: #516A7C;}
        .icheck-inline{margin: 0px !important;} .modal-product{width: 66% !important;}
\        .wallet{color: #516A7C !important;}
        .wallet-color{color:#D72225 !important;}
    </style>
    <div id="page_content">
        <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
            <h1>{{ ucfirst($data->name) }}</h1>
        </div>
        <div id="page_content_inner">

            <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
                <div class="uk-width-xLarge-2-10 uk-width-large-3-10">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text">
                                Photos
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <div class="uk-margin-bottom uk-text-center">
                                @if($data->profile_image!="")
                                    @if (filter_var($data->profile_image, FILTER_VALIDATE_URL)) 
                                        <img src="{{ $data->profile_image }}"  alt="" class="img_medium"/>
                                    @else 
                                        <img src="{{ URL::asset('/public/images/'.$data->profile_image) }}"  alt="" class="img_medium"/>
                                    @endif
                                @else
                                    <img src="{{ URL::asset('public/no-image.png') }}" alt="" class="img_medium" />
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
                <div class="uk-width-xLarge-8-10  uk-width-large-7-10">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text">
                                Basic Details
                            </h3>
                        </div>
                        <div class="md-card-content large-padding">
                            <div class="uk-grid uk-grid-divider uk-grid-medium">
                                <div class="uk-width-large-1-1">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small"> Name </span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle"> {{ ucfirst($data->name) }} </span>
                                        </div>
                                    </div>

                                    <hr class="uk-grid-divider">

                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small"> Email </span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle"> @if($data->email!="")  {{ $data->email }}  @else NA @endif </span>
                                        </div>
                                    </div>

                                    <hr class="uk-grid-divider">

                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small"> Mobile No </span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle"> @if($data->phone_no!="")  {{ $data->phone_no }}  @else NA @endif </span>
                                        </div>
                                    </div>
                                    
                                    <hr class="uk-grid-divider">

                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small"> Total Coins </span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            @if($data->wallet_status==0)
                                                <span class="uk-badge uk-badge-danger"> {{ $data->coin }} </span>
                                            @else
                                                <span class="uk-badge uk-badge-danger md-bg-blue-grey-600"> {{ $data->coin }} </span>
                                            @endif
                                            {{--  <span class="uk-text-large uk-text-middle @if($data->wallet_status== 0) wallet-color @else wallet @endif"> {{ $data->coin }} </span>  --}}
                                        </div>
                                    </div>
                                    
                                    <hr class="uk-grid-divider">

                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small"> Total Credits </span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            @if($data->wallet_status==0)
                                                <span class="uk-badge uk-badge-danger"> {{ $data->credit }} </span>
                                            @else
                                                <span class="uk-badge uk-badge-danger md-bg-blue-grey-600"> {{ $data->credit }} </span>
                                            @endif
                                            {{--  <span class="uk-text-large uk-text-middle @if($data->wallet_status== 0) wallet-color @else wallet @endif"> 
                                                {{ $data->credit }} </span>  --}}
                                        </div>
                                    </div>

                                    <hr class="uk-grid-divider">

                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small"> Block/Unblock (Credits and Coins) </span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <input type="checkbox" data-switchery data-switchery-color="#d32f2f" onchange="wallet_status({{$data->id}},{{$data->wallet_status}})" @if($data->wallet_status==0) checked @endif   id="switch_demo_danger"/>
                                        </div>
                                    </div>
                                    
                                    <hr class="uk-grid-divider">
                                       
                                    <hr class="uk-grid-divider uk-hidden-large">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text">
                                Other Details
                            </h3>
                        </div>
                        <div class="md-card-content large-padding">
                            <div class="uk-grid uk-grid-divider uk-grid-medium">
                                <div class="uk-width-large-1-1">

                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small"> Signup Type </span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle">
                                                @if($data->signup_type!="")  
                                                    @if($data->signup_type=="google")
                                                        <span class="uk-badge uk-badge-danger">{{ strtoupper($data->signup_type) }}</span>
                                                    @elseif($data->signup_type=="facebook")
                                                        <span class="uk-badge uk-badge-primary">{{ strtoupper($data->signup_type) }}</span>
                                                    @else
                                                        <span class="uk-badge uk-badge-success">{{ strtoupper($data->signup_type) }}</span>
                                                    @endif
                                                @else
                                                    NA
                                                @endif
                                            </span>
                                        </div>
                                    </div>

                                    <hr class="uk-grid-divider">

                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small"> Address </span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle"> @if($data->address!="")  {{ $data->address }}  @else NA @endif </span>
                                        </div>
                                    </div>

                                    <hr class="uk-grid-divider">


                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small"> App Type </span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-large uk-text-middle">
                                                @if($data->app_type!="")  
                                                    @if($data->app_type=="google")
                                                        <span class="uk-badge uk-badge-danger">{{ strtoupper($data->app_type) }}</span>
                                                    @elseif($data->app_type=="facebook")
                                                        <span class="uk-badge uk-badge-primary">{{ strtoupper($data->app_type) }}</span>
                                                    @else
                                                        <span class="uk-badge uk-badge-success">{{ strtoupper($data->app_type) }}</span>
                                                    @endif
                                                @else
                                                    NA
                                                @endif
                                            </span>
                                        </div>
                                    </div>

                                    <hr class="uk-grid-divider">
                                    <hr class="uk-grid-divider uk-hidden-large">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--  <div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary" href="" style="background-color:#516A7C;color:white;">
            <i class="material-icons">&#xE150;</i>
        </a>
    </div>  --}}
    {{--  ecommerce_product_edit.html  --}}
    <script>
        function wallet_status(id,status)
        {
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: "POST",
                url: "{{ route('user_wallet_status') }}",
                data: {id: id,'_token': '{!!csrf_token()!!}'},
                cache: false,
    
                success :function(data)
                {
                    if(status==1)
                    {
                        if(data.message=="success")
                        {
                            UIkit.notify({ message: 'Wallet Blocked Successfully!!..', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                        }
                        else if(data.message=="warning")
                        {
                            UIkit.notify({ message: 'Something Went Wrong!!..', status: 'warning', timeout: 5000, group: null, pos: 'top-center' });
                        }
                        else
                        {
                            UIkit.notify({ message: 'User Not Found!!..', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                        }
                        location.reload();
                    }
                    else
                    {
                        if(data.message=="success")
                        {
                            UIkit.notify({ message: 'Wallet Unblocked Successfully', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                        }
                        else if(data.message=="warning")
                        {
                            UIkit.notify({ message: 'Something Went Wrong', status: 'warning', timeout: 5000, group: null, pos: 'top-center' });
                        }
                        else
                        {
                            UIkit.notify({ message: 'User Not Found', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                        }
                        location.reload();
                    }
                },
                error: function () 
                {
                    }
            });
        }
    </script>
@endsection
