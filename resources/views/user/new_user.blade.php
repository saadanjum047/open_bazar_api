@extends('layout.base')
@section('content')
<style>
    .uk-pagination>li.uk-active>a, .uk-pagination>li.uk-active>span{background: #516A7C;}
    .icheck-inline{margin: 0px !important;} .modal-product{width: 66% !important;}
    .product-buttons{background-color:#516A7C;color:white;}
    .wrap-text{display: inline-block; width: 10em; overflow: hidden;white-space: nowrap;text-overflow: ellipsis;}
    .wrap-new{word-wrap: break-word;display: inline-block; width: 6em; overflow: hidden; white-space: normal;}
</style>
<div id="page_content">
    <div id="page_content_inner">
        <h4 class="heading_a uk-margin-bottom">View Users</h4>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        {{-- <a href="javascript:void(0)"  data-uk-modal="{target:'#modal_del'}"  class="md-btn md-btn-small md-btn-wave-light md-btn-icon waves-effect waves-button waves-light product-buttons"  title="Multiple Delete Products">
                            <i class="uk-icon-trash no_margin" style="color:white;"></i>
                        </a> --}}
                        <div class="uk-modal" id="modal_del">
                            <div class="uk-modal-dialog">
                                <div class="uk-modal-header">
                                    <h3 class="uk-modal-title">Are you sure you want to delete selected user? </h3>
                                </div>
                                <div class="uk-modal-content">
                                    <a href="javascript:void(0)" class="md-btn md-btn-small md-btn-wave-light md-btn-icon waves-effect waves-button waves-light delete_all" title="Multiple Delete Products" style="background-color:#516A7C;color:white;">
                                        Yes
                                    </a>
                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">No</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="uk-overflow-container">
                    <table id="dt_default" class="uk-table uk-table-striped uk-table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                             <th>
                                <span class="icheck-inline">
                                    <input type="checkbox" class="ts_checkbox_all" name="checkbox_demo_inline_mercury" id="checkbox_demo_inline_1" data-md-icheck />
                                </span>
                            </th>
                            <th> Name </th>
                            <th> Email </th>
                            <th> Mobile </th>
                            <th> Image </th>
                            <th> App Type </th>
                            <th> Total Coin </th>
                            <th> Total Credit </th>
                            <th> Signup <br> Type </th>
                            <th> Block <br> Unblock </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    
  

    function del(id,status)
    {
        
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "POST",
            url: "{{ route('status_user') }}",
            data: {id: id,'_token': '{!!csrf_token()!!}'},
            cache: false,

            success :function(data)
            {
                if(status==1)
                {
                    if(data.message=="success")
                    {
                        UIkit.notify({ message: 'Blocked Successfully!!..', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    else if(data.message=="warning")
                    {
                        UIkit.notify({ message: 'Something Went Wrong!!..', status: 'warning', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    else
                    {
                        UIkit.notify({ message: 'User Not Found!!..', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    location.reload();
                }
                else
                {
                    if(data.message=="success")
                    {
                        UIkit.notify({ message: 'Unblocked Successfully', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    else if(data.message=="warning")
                    {
                        UIkit.notify({ message: 'Something Went Wrong', status: 'warning', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    else
                    {
                        UIkit.notify({ message: 'User Not Found', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    location.reload();
                }
            },
            error: function () 
            {
                }
        });
    }
</script>
<script src="{{ URL::asset('theme/assets/js/common.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".sub_chk").prop('checked', false);

        $('.ts_checkbox_all')
            .iCheck({
                checkboxClass: 'icheckbox_md',
                radioClass: 'iradio_md',
                increaseArea: '20%'
            })
            .on('ifChecked',function() {
                $('#dt_default')
                    .find('.ts_checkbox')
                    .prop('checked',true)
                    .iCheck('update')
                    .closest('tr')
                    .addClass('row_highlighted');
            })
            .on('ifUnchecked',function() {
                $('#dt_default')
                    .find('.ts_checkbox')
                    .prop('checked',false)
                    .iCheck('update')
                    .closest('tr')
                    .removeClass('row_highlighted');
            });
        $('.delete_all').on('click', function (e) {
            var allVals = [];
            $(".sub_chk:checked").each(function () {
                allVals.push($(this).attr('data-id'));
            });
            var join_selected_values = allVals;
            console.log(join_selected_values);
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: 'POST',
                url: "{{ route('user_delete_multiple') }}",
                dataType: "JSON",
                data: {"_token": "{{ csrf_token() }}",id: join_selected_values},
                success: function (data) {
                    $(".sub_chk:checked").each(function () {
                        $(this).parents("tr").remove();
                    });
                    var modal = UIkit.modal("#modal_del");

                    if ( modal.isActive() ) {
                        modal.hide();
                    }

                        UIkit.notify({ message: 'Selected User Deleted Successfully!!..', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                },
                error: function (data) {
                    var modal = UIkit.modal("#modal_del");
                    if ( modal.isActive() ) {
                        modal.hide();
                    }
                    UIkit.notify({ message: 'Something Went Wrong!!..', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                }
            });
  
            $.each(allVals, function (index, value) {
                $('table tr').filter("[data-row-id='" + value + "']").remove();
            });
        });
    });

    $(document).ready(function () {
        $('#dt_default').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ route('allusers') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
            "columns": [
                { "data": "checkbox" },
                { "data": "name" },
                { "data": "email" },
                { "data": "mobile" },
                { "data": "image" },
                { "data": "apptype" },
                { "data": "totalcoin" },
                { "data": "totalcredit" },
                { "data": "signuptype" },
                { "data": "status" },
                { "data": "action" },
              
            ]	,

            "columnDefs": [
    { "orderable": false, "targets": 0 },
    { "orderable": false, "targets": 4 },
    { "orderable": false, "targets": 6 },
    { "orderable": false, "targets": 7 },
    { "orderable": false, "targets": 9 },
    { "orderable": false, "targets": 10 },
  ],
           


            

        });
    });
</script>
@endsection