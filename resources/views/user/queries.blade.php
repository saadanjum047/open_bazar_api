@extends('layout.base')
@section('content')
<style>
    .uk-pagination>li.uk-active>a, .uk-pagination>li.uk-active>span{background: #516A7C;}
    .icheck-inline{margin: 0px !important;} .modal-product{width: 66% !important;}
    .product-buttons{background-color:#516A7C;color:white;}
    .wrap-text{display: inline-block; width: 12em; overflow: hidden;white-space: nowrap;text-overflow: ellipsis;}
    .wrap-new{word-wrap: break-word;display: inline-block; width: 12em; overflow: hidden;}
</style>
<div id="page_content">
    <div id="page_content_inner">
        <h4 class="heading_a uk-margin-bottom">View Queries</h4>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <a href="javascript:void(0)"  data-uk-modal="{target:'#modal_del'}"  class="md-btn md-btn-small md-btn-wave-light md-btn-icon waves-effect waves-button waves-light product-buttons"  title="Multiple Delete Products">
                            <i class="uk-icon-trash no_margin" style="color:white;"></i>
                        </a>
                        <div class="uk-modal" id="modal_del">
                            <div class="uk-modal-dialog">
                                <div class="uk-modal-header">
                                    <h3 class="uk-modal-title">Are you sure you want to delete selected query? </h3>
                                </div>
                                <div class="uk-modal-content">
                                    <a href="javascript:void(0)" class="md-btn md-btn-small md-btn-wave-light md-btn-icon waves-effect waves-button waves-light delete_all" title="Multiple Delete Products" style="background-color:#516A7C;color:white;">
                                        Yes
                                    </a>
                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">No</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="uk-overflow-container">
                    <table id="dt_default" class="uk-table uk-table-striped uk-table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>
                                <span class="icheck-inline">
                                    <input type="checkbox" class="ts_checkbox_all" name="checkbox_demo_inline_mercury" id="checkbox_demo_inline_1" data-md-icheck />
                                </span>
                            </th>
                            <th> Name </th>
                            <th> Email </th>
                            <th> Mobile </th>
                            <th> Date </th>
                            <th> Subject </th>
                            <th> Message </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(!$data->isEmpty())
                                @foreach($data as $key)
                                    <tr>
                                        <td>
                                            <span class="icheck-inline">
                                                <input type="checkbox" name="checkbox_demo_inline_mercury" class="sub_chk ts_checkbox" data-id="{{$key->id}}" id="checkbox_demo_inline_1" data-md-icheck />
                                            </span>
                                        </td>
                                        <td> {{ $key->name }} </td>
                                        <td> {{ $key->email }} </td>
                                        <td> {{ $key->mobile }} </td>
                                        <td> {{ date('d-m-y',strtotime($key->created_at)) }} </td>
                                        <td> <span class="wrap-text"> @if($key->subject!="") {{ $key->subject }} @else NA @endif </span> </td>
                                        <td> <span class="wrap-text"> @if($key->message!="") {{ $key->message }} @else NA @endif </span> </td>
                                        <td>
                                            <a href="javascript:void(0);" title="View" data-uk-modal="{target:'#modal_default{{$key->id}}'}"><i class="uk-icon-eye uk-icon-small"></i></a>
                                            &nbsp;&nbsp;

                                            {{--  <a href="" title="EDIT"><i class="uk-icon-edit uk-icon-small"></i></a>
                                            &nbsp;&nbsp;  --}}
                                            
                                            <a href="#" onclick="UIkit.modal.confirm('Are you sure you want to delete query? ', function(){ 
                                                var url = '{{ route("delete_query", ":slug") }}';
                                                url = url.replace(':slug', '{{$key->id}}');
                                                window.location.href=url;
                                                });" title="DELETE">
                                                <i class="uk-icon-trash uk-icon-small"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <div class="uk-modal" id="modal_default{{$key->id}}">
                                        <div class="uk-modal-dialog">
                                            <div class="uk-modal-header">
                                                <h3 class="uk-modal-title">Query Details </h3>
                                            </div>
                                            <div class="uk-modal-content">
                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"> Name &nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"><b> {{ $key->name }} </b></label>
                                                    </div>
                                                </div>

                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"> Email &nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"><b>{{ $key->email }}</b></label>
                                                    </div>
                                                </div>

                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"> Mobile No &nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"><b>{{ $key->mobile }}</b></label>
                                                    </div>
                                                </div>

                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"> Date &nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"><b>{{ date('d-m-Y',strtotime($key->created_at)) }}</b></label>
                                                    </div>
                                                </div>

                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"> Subject &nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"><b class="wrap-new">@if($key->subject!="")  {{ $key->subject }}  @else NA @endif</b></label>
                                                    </div>
                                                </div>
                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"> Message &nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"><b class="wrap-new">@if($key->message!="") {{ $key->message }} @else NA @endif</b></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="uk-modal-footer uk-text-right">
                                                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7"> No Record Found</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ URL::asset('theme/assets/js/common.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".sub_chk").prop('checked', false);

        $('.ts_checkbox_all')
            .iCheck({
                checkboxClass: 'icheckbox_md',
                radioClass: 'iradio_md',
                increaseArea: '20%'
            })
            .on('ifChecked',function() {
                $('#dt_default')
                    .find('.ts_checkbox')
                    .prop('checked',true)
                    .iCheck('update')
                    .closest('tr')
                    .addClass('row_highlighted');
            })
            .on('ifUnchecked',function() {
                $('#dt_default')
                    .find('.ts_checkbox')
                    .prop('checked',false)
                    .iCheck('update')
                    .closest('tr')
                    .removeClass('row_highlighted');
            });
        $('.delete_all').on('click', function (e) {
            var allVals = [];
            $(".sub_chk:checked").each(function () {
                allVals.push($(this).attr('data-id'));
            });
            var join_selected_values = allVals;
            console.log(join_selected_values);
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: 'POST',
                url: "{{ route('query_delete_multiple') }}",
                dataType: "JSON",
                data: {"_token": "{{ csrf_token() }}",id: join_selected_values},
                success: function (data) {
                    $(".sub_chk:checked").each(function () {
                        $(this).parents("tr").remove();
                    });
                    var modal = UIkit.modal("#modal_del");

                    if ( modal.isActive() ) {
                        modal.hide();
                    }

                        UIkit.notify({ message: 'Selected Query Deleted Successfully!!..', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                },
                error: function (data) {
                    var modal = UIkit.modal("#modal_del");
                    if ( modal.isActive() ) {
                        modal.hide();
                    }
                    UIkit.notify({ message: 'Something Went Wrong!!..', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                }
            });
  
            $.each(allVals, function (index, value) {
                $('table tr').filter("[data-row-id='" + value + "']").remove();
            });
        });
    });
</script>
@endsection