@extends('layout.base')
@section('content')
<link rel="stylesheet" href="{{ URL::asset('theme/assets/skins/dropify/css/dropify.css') }}">
<div id="page_content">
     <div id="page_content_inner">

        <h3 class="heading_b uk-margin-bottom">Add Category</h3>
        <div class="md-card">
            <div class="md-card-content large-padding">
                <form method="post" action="{{ route('addcategory') }}" id="form_validation" enctype="multipart/form-data" class="uk-form-stacked task-form">
                	@csrf
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <label for="fullname">Category<span class="req">*</span></label>
                            <div class="parsley-row">
                                <select id="select_demo_4" name="main_category" data-md-selectize>
                                    <option value="" >Select Main Category</option>
                                    <option value="1" >Sell Your Stuff</option>
                                    <option value="2" >Rent Your Stuff</option>
                                    <option value="3" >Offer Services</option>
                                    <option value="4" >Start Your Shop</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label for="fullname">Category Name<span class="req">*</span></label>
                            <div class="parsley-row">
                                <input type="text" name="en_name" autocomplete="off" placeholder="Enter category name in English" required class="md-input task" />
                            </div>
                        </div>
                    {{-- </div>
                    <div class="uk-grid" data-uk-grid-margin> --}}
                        <div class="uk-width-medium-1-2">
                            <label for="fullname">Category Name<span class="req">*</span></label>
                            <div class="parsley-row">
                                <input type="text"   name="ar_name" autocomplete="off" placeholder="Enter category name in Dutch" required class="md-input task" />
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label for="fullname">Position<span class="req">*</span></label>
                            <div class="parsley-row">
                                <input type="text"   name="position" autocomplete="off" placeholder="Enter position of category" required class="md-input task" />
                            </div>
                        </div>
                        {{--  dir="rtl"  --}}
                        <div class="uk-width-medium-1-2">
                            <div class="md-card">
                                <div class="md-card-content">
                                    <h3 class="heading_a uk-margin-small-bottom">
                                        Upload Image<span class="req">*</span>
                                    </h3>
                                    <input type="file" id="input-file-a" class="dropify" name="image" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                        <!-- accept=".png, .jpg, .jpeg" -->

                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <button type="submit" class="md-btn" style="background-color:#516A7C;color: white;">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection