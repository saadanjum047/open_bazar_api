@extends('layout.base')
@section('content')
<style>
    .uk-pagination>li.uk-active>a, .uk-pagination>li.uk-active>span{background: #516A7C;}
</style>
<div id="page_content">
     <div id="page_content_inner">
        <h4 class="heading_a uk-margin-bottom">View Category</h4>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <a class="md-btn md-btn-small md-btn-wave-light md-btn-icon waves-effect waves-button waves-light add_button" title="Add Category" style="background-color:#516A7C;color:white;" href="{{ route('add-category') }}">
                            <i class="uk-icon-plus-circle no_margin" style="color:white;"></i>
                        </a>
                    </div>
                </div>
                <br>
                <div class="uk-overflow-container">
                    <table id="dt_default" class="uk-table uk-table-striped uk-table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Main Category</th>
                            <th>Category Name<br>(English)</th>
                            <th>Category Name<br>(Dutch)</th>
                            <th>Position</th>
                            <th>Image</th>
                            <th>Publish&nbsp;/<br>Unpublish</th>
                            <th>Sub-Category</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                         <tbody>
                            @if(!$data->isEmpty())
                                @foreach($data as $key)
                                    <tr>
                                        <td>
                                            @if($key->main_category==1)
                                                <span class="uk-badge">Sell Your Stuff</span>
                                            @elseif($key->main_category==2)
                                                <span class="uk-badge uk-badge-danger">Rent Your Stuff</span>
                                            @elseif($key->main_category==3)
                                                <span class="uk-badge uk-badge-primary">Offer Services</span>
                                            @else
                                                <span class="uk-badge uk-badge-warning">Start Your Shop</span>
                                            @endif
                                        </td>
                                        <td> {{ $key->en_name }} </td>
                                        <td> {{ $key->ar_name }} </td>
                                        <td> {{ $key->position }} </td>
                                        <td>
                                            <img src="{{ URL::asset('/public/images/'.$key->image) }}" height="60" width="70">
                                        </td>
                                        <td>
                                            <input type="checkbox" data-switchery data-switchery-color="#d32f2f" onchange="del({{$key->id}},{{$key->publish}})" @if($key->publish==1) checked @endif   id="switch_demo_danger"/>
                                        </td>
                                        <td>
                                            <a class="md-btn md-btn-small md-btn-wave-light md-btn-icon waves-effect waves-button waves-light add_button" style="background-color:#516A7C;color:white;" href="{{ route('add-subcategory',['id'=>encrypt($key->id)]) }}" title="Add Subcategory">
                                                <i class="uk-icon-plus-circle" style="color:white;"></i>Add
                                            </a>
                                            <br><br>
                                            <a class="md-btn md-btn-small md-btn-wave-light md-btn-icon waves-effect waves-button waves-light add_button" style="background-color:#516A7C;color:white;" href="{{ route('view-subcategory',['id'=>encrypt($key->id)]) }}" title="View Subcategory">
                                                <i class="uk-icon-eye uk-icon-small" style="color:white;"></i>View 
                                            </a>
                                            {{-- <a href="{{ route('view-subcategory',['id'=>encrypt($key->id)]) }}" title="View Subcategory"></a>
                                            &nbsp;&nbsp; --}}
                                        </td>
                                        <td> 
                                            <a href="javascript:void(0);" title="View" data-uk-modal="{target:'#modal_default{{$key->id}}'}"><i class="uk-icon-eye uk-icon-small"></i></a>
                                            &nbsp;&nbsp;

                                            <a href="{{ route('edit-category',['id'=>encrypt($key->id)]) }}" title="EDIT"><i class="uk-icon-edit uk-icon-small"></i></a>
                                            &nbsp;&nbsp;
                                            
                                            <a href="#" onclick="UIkit.modal.confirm('Are you sure you want to delete category? ', function(){ 
                                                    var url = '{{ route("deletecategory", ":slug") }}';

                                                    url = url.replace(':slug', '{{$key->id}}');

                                                    window.location.href=url;
                                                 });" title="DELETE">

                                                 <i class="uk-icon-trash uk-icon-small"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <div class="uk-modal" id="modal_default{{$key->id}}">
                                        <div class="uk-modal-dialog">
                                            <div class="uk-modal-header">
                                                <h3 class="uk-modal-title">Category Details </h3>
                                            </div>
                                            <div class="uk-modal-content">
                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname">Main Category &nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname">
                                                            <b>
                                                                @if($key->main_category==1)
                                                                    <span class="uk-badge">Sell Your Stuff</span>
                                                                @elseif($key->main_category==2)
                                                                    <span class="uk-badge uk-badge-danger">Rent Your Stuff</span>
                                                                @elseif($key->main_category==3)
                                                                    <span class="uk-badge uk-badge-primary">Offer Services</span>
                                                                @else
                                                                    <span class="uk-badge uk-badge-warning">Start Your Shop</span>
                                                                @endif
                                                            </b>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname">Category Name(English)&nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"><b>{{ $key->en_name }}</b></label>
                                                    </div>
                                                </div>

                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname">Category Name(Arabic)&nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"><b>{{ $key->ar_name }}</b></label>
                                                    </div>
                                                </div>

                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname">Position&nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"><b>{{ $key->position }}</b></label>
                                                    </div>
                                                </div>

                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname">Image&nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <img src="{{ URL::asset('/public/images/'.$key->image) }}" height="60" width="70">
                                                    </div>
                                                </div>
                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname">Publish/Unpublish&nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        @if($key->publish==0)
                                                            <span class="uk-badge uk-badge-primary">Unpublished</span>
                                                        @else
                                                            <span class="uk-badge uk-badge-warning">Published</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="uk-modal-footer uk-text-right">
                                                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">No Record Found</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="md-fab-wrapper">
    <a class="md-fab md-fab-primary" href="{{ route('add-category') }}" style="background-color:#516A7C;color:white;">
        <i class="uk-icon-plus"></i>
    </a>
</div>
<script>
    function del(id,status)
    {
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "POST",
            url: "{{ route('status_category') }}",
            data: {id: id,'_token': '{!!csrf_token()!!}'},
            cache: false,

            success :function(data)
            {
                if(status==1)
                {
                    if(data.message=="success")
                    {
                        UIkit.notify({ message: 'Unpublished Successfully!!..', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    else if(data.message=="warning")
                    {
                        UIkit.notify({ message: 'Something Went Wrong!!..', status: 'warning', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    else
                    {
                        UIkit.notify({ message: 'Category Not Found!!..', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    location.reload();
                }
                else
                {
                    if(data.message=="success")
                    {
                        UIkit.notify({ message: 'Published Successfully', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    else if(data.message=="warning")
                    {
                        UIkit.notify({ message: 'Something Went Wrong', status: 'warning', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    else
                    {
                        UIkit.notify({ message: 'Category Not Found', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                    }
                    location.reload();
                }
            },
            error: function () 
            {
                }
        });
    }
</script>
@endsection