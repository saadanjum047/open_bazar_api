@extends('layout.base')
@section('content')
<link rel="stylesheet" href="{{ URL::asset('theme/assets/skins/dropify/css/dropify.css') }}">
<div id="page_content">
     <div id="page_content_inner">

        <h3 class="heading_b uk-margin-bottom">Update Category</h3>
        <div class="md-card">
            <div class="md-card-content large-padding">
                <form method="post" action="{{ route('editcategory') }}" id="form_validation" enctype="multipart/form-data" class="uk-form-stacked task-form">
                	@csrf
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <label for="fullname">Category<span class="req">*</span></label>
                            <div class="parsley-row">
                                <select id="select_demo_4" name="main_category" data-md-selectize>
                                    <option value="" >Select Main Category</option>
                                    <option value="1" @if($data->main_category == "1") selected @endif >Sell Your Stuff</option>
                                    <option value="2" @if($data->main_category == "2") selected @endif >Rent Your Stuff</option>
                                    <option value="3"  @if($data->main_category == "3")  selected @endif >Offer Services</option>
                                    <option value="4" @if($data->main_category == "4") selected @endif >Start Your Shop</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label for="fullname">Category Name<span class="req">*</span></label>
                            <div class="parsley-row">
                                <input type="hidden" name="id" value="{{$data->id}}">
                                <input type="hidden" name="oldimage" value="{{$data->image}}">
                                <input type="text" name="en_name" value="{{ $data->en_name }}" placeholder="Enter category name in english" required class="md-input task" />
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label for="fullname">Category Name<span class="req">*</span></label>
                            <div class="parsley-row">
                                <input type="text"  name="ar_name" value="{{ $data->ar_name }}" placeholder="Enter category name in arabic" required class="md-input task" />
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label for="fullname">Position<span class="req">*</span></label>
                            <div class="parsley-row">
                                <input type="text"  name="position" value="{{ $data->position }}" placeholder="Enter position of the category" required class="md-input task" />
                            </div>
                        </div>
                        {{--  dir="rtl"   --}}
                        <div class="uk-width-medium-1-2">
                            <div class="md-card">
                                <div class="md-card-content">
                                    <h3 class="heading_a uk-margin-small-bottom">
                                        Upload Image<span class="req">*</span>
                                    </h3>
                                    <input type="file" id="input-file-a" class="dropify" name="image" data-default-file="{{ URL::asset('/public/images/'.$data->image) }}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--  <div class="uk-grid" data-uk-grid-margin>
                        
                    </div>  --}}
                        {{--  <!-- accept=".png, .jpg, .jpeg" -->  --}}

                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <button type="submit" class="md-btn" style="background-color:#516A7C;color: white;">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection