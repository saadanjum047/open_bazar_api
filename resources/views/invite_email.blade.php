<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title>OpenBazar</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" />
  <style type="text/css" xml:space="preserve">
    /* CLIENT-SPECIFIC STYLES */
        #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
        .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
        body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
        table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
        img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */
        /* RESET STYLES */
        body{margin:0; padding:0;}
        img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
        table{border-collapse:collapse !important;}
        body{height:100% !important; margin:0; padding:0; width:100% !important;}
        /* iOS BLUE LINKS */
        .appleBody a {color:#68440a; text-decoration: none;}
        .appleFooter a {color:#999999; text-decoration: none;}
        /* MOBILE STYLES */
        @media screen and (max-width: 480px) {
            .wrapper_shrink {
                width:100% !important;
                padding-left: 5px !important;
                padding-right: 5px !important;
            }
            .table_shrink {
                width:95% !important;
            }
            .hero {
                width: 100% !important;
            }
            .left_table, .right_table {
                 width:100% !important;
            }
            .td{
                padding-right: 10px !important;
            }
            .footer{
                padding-left: 0px !important;
                padding-right: 0px !important;
            }
            #footer_logo{
                padding-right: 0px !important;
            }
        }
  </style>
  <meta charset="UTF-8" />
</head>

<body>
  <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff" class="table_shrink" align="center">
    <tr>
      <td rowspan="1" colspan="1">
        <table width="520px" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" align="center" class="table_shrink">
          <tr>
            <td valign="top" rowspan="1" colspan="1">
              <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" class="table_shrink">
                <tr>
                  <td rowspan="1" colspan="1">
                    <div>
                      <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" class="table_shrink">
                        <tr>
                          <td valign="top" align="center" style="padding-top: 30px; padding-bottom: 10px;" rowspan="1" colspan="1">
                            <a href="javascript::void(0);" alias="OpenBazar header logo" shape="rect">
                              <img src="http://openbazar.nl/app-admin/public/Logo.png" height="40" width="180" border="0" alt="OpenBazar" style="display:block; color:#4c9ac9; align:center" align="middle" />
                            </a>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td style="color:#cccccc; padding-top: 10px;" valign="top" rowspan="1" colspan="1">
                    <hr color="#cccccc" size="1" />
                  </td>
                </tr>
                <tr>
                  <td valign="top" align="left" rowspan="1" colspan="1">
                    <div></div>
                  </td>
                </tr>
                {{-- <tr>
                  <td valign="top" align="left" rowspan="1" colspan="1">
                    <div>
                      <tr valign="top">
                        <td align="center" style="padding-top: 20px;" rowspan="1" colspan="1">
                          <img height="93" width="90" border="0" style="display:block; color:#4c9ac9; align:center" align="middle" src="http://139.59.75.219/OpenBazar/public/user.jpeg" />
                        </td>
                      </tr>
                    </div>
                  </td>
                </tr> --}}
                <tr>
                  <td valign="top" align="left" rowspan="1" colspan="1">
                    <div>
                      <tr>
                        <td valign="top" style="padding-top: 40px; font-family:Helvetica neue, Helvetica, Arial, Verdana, sans-serif; color: #516A7C; font-size: 22px; line-height: 32px; text-align:center; font-weight:none;" align="middle" rowspan="1" colspan="1">
                            <span>
                                <strong>{{ $sender }} invites you to try OpenBazar!</strong>{{-- collaborate on --}}
                                <br>
                                <strong>Hello {{ $receiver }}</strong>
                                <br>    
                                <p><i>{{ $sender }} invites you to try OpenBazar. Find out who else form your friends are on OpenBazar.Start your own business by selling and renting your stuff, and earning coins.                                  </i></p>
                            </span>
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" align="center" rowspan="1" colspan="1">
                          <table border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                              <td align="center" style="padding-top: 30px; padding-bottom: 10px" rowspan="1" colspan="1">
                                <a target="_blank" style="font-size: 20px; font-family: Helvetica, Helvetica neue, Arial, Verdana, sans-serif; font-weight: none; color: #ffffff; text-decoration: none; background-color: #c62828 ; border-top: 11px solid #c62828 ; border-bottom: 11px solid #c62828 ; border-left: 20px solid #c62828 ; border-right: 20px solid #c62828 ; border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; display: inline-block;"
                                  class="mobile-button" alias="Button" shape="rect" href="{{ $link }}">Accept invitation!</a>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td valign="top" align="left" rowspan="1" colspan="1">
                    <div></div>
                  </td>
                </tr>
                <tr>
                  <td valign="top" align="left" rowspan="1" colspan="1">
                    <div></div>
                  </td>
                </tr>
                <tr>
                  <td valign="top" align="left" rowspan="1" colspan="1">
                    <div></div>
                  </td>
                </tr>
                <tr>
                  <td valign="top" align="left" rowspan="1" colspan="1">
                    <div></div>
                  </td>
                </tr>
                <tr>
                  <td valign="top" align="left" rowspan="1" colspan="1">
                    <div></div>
                  </td>
                </tr>
                <tr>
                  <td valign="top" align="left" rowspan="1" colspan="1">
                    <div></div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td rowspan="1" colspan="1">
              <table width="520px" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" class="table_shrink" align="center">
                <tr>
                  <td style="color:#cccccc; padding-top: 20px;" valign="top" width="100%" rowspan="1" colspan="1">
                    <hr color="#cccccc" size="1" />
                  </td>
                </tr>
                <tr>
                  <td rowspan="1" colspan="1">
                    <div>
                      <table width="520px" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" class="table_shrink" align="center">
                        <tr>
                          <td valign="top" style="padding-top: 10px; font-family: Helvetica, Helvetica neue, Arial, Verdana, sans-serif; color: #707070; font-size: 12px; line-height: 18px; text-align:left; font-weight:none;" align="left" rowspan="1" colspan="1">
                            <br clear="none" /><span>This message was sent to you by OpenBazar</span>
                            <br clear="none" />
                          </td>
                          <td align="right" style="padding-top:15px; padding-bottom:30px;" rowspan="1" colspan="1">
                            <a href="javascript::void(0);" shape="rect">
                              <img src="http://openbazar.nl/app-admin/public/Logo.png" height="24" width="110" border="0" alt="OpenBazar" style="display:block; color:#3572b0;" />
                            </a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <custom name="opencounter" type="tracking"></custom>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>

</html>