<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->

<head>

    <title>Open Bazar</title>
    @include('layout.stylesheet')
    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
        <link rel="stylesheet" href="assets/css/ie.css" media="all">

    <![endif]-->

    <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::asset('public/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="310x310" href="{{ URL::asset('public/favicon/android-chrome-512x512.png') }}">
    <link rel="icon" type="image/png" sizes="310x310" href="{{ URL::asset('public/favicon/android-chrome-512x512.png') }}">
    <link rel="manifest" href="{{ URL::asset('public/favicon/site.webmanifest') }}">
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    @include('layout.header')
    @include('layout.sidebar')
        @yield('content')
    

@include('layout.script')
    
</body>
</html>