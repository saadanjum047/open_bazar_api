
<!-- additional styles for plugins -->
        <!-- weather icons -->
        <link rel="stylesheet" href="{{ URL::asset('theme/bower_components/weather-icons/css/weather-icons.min.css') }}" media="all">
        <!-- metrics graphics (charts) -->
        <link rel="stylesheet" href="{{ URL::asset('theme/bower_components/metrics-graphics/dist/metricsgraphics.css') }}">
        <!-- chartist -->
        <link rel="stylesheet" href="{{ URL::asset('theme/bower_components/chartist/dist/chartist.min.css') }}">

        <link rel="stylesheet" href="{{ URL::asset('theme/bower_components/fullcalendar/dist/fullcalendar.min.css') }}">
    
    <!-- uikit -->
    <link rel="stylesheet" href="{{ URL::asset('theme/bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="{{ URL::asset('theme/assets/icons/flags/flags.min.css') }}" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="{{ URL::asset('theme/assets/css/style_switcher.min.css') }}" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="{{ URL::asset('theme/assets/css/main.min.css') }}" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="{{ URL::asset('theme/assets/css/themes/themes_combined.min.css') }}" media="all">

    