 <!-- google web fonts -->
    <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>


    <!-- common functions -->
    
    
    <script src="{{ URL::asset('theme/assets/js/common.min.js') }}"></script>
    <script src="{{ URL::asset('theme/assets/js/custom/wizard_steps.min.js') }}"></script>
    

    <!-- uikit functions -->
    <script src="{{ URL::asset('theme/assets/js/uikit_custom.min.js') }}"></script>
    
    <!-- altair common functions/helpers -->
    <script src="{{ URL::asset('theme/assets/js/altair_admin_common.min.js') }}"></script>

    <script src="{{ URL::asset('theme/bower_components/dropify/dist/js/dropify.min.js') }}"></script>

    <!--  form file input functions -->
    <script src="{{ URL::asset('theme/assets/js/pages/forms_file_input.min.js') }}"></script>

    <!--  notifications functions -->
    <script src="{{ URL::asset('theme/assets/js/pages/components_notifications.min.js') }}"></script>


    <!-- page specific plugins -->
       
        <!-- d3 -->
        <script src="{{ URL::asset('theme/bower_components/d3/d3.min.js') }}"></script>
        
        <!-- metrics graphics (charts) -->
        <script src="{{ URL::asset('theme/bower_components/metrics-graphics/dist/metricsgraphics.min.js') }}"></script>
        
        <!-- chartist (charts) -->
        <script src="{{ URL::asset('theme/bower_components/chartist/dist/chartist.min.js') }}"></script>
        
        
        <script src="{{ URL::asset('theme/bower_components/maplace-js/dist/maplace.min.js') }}"></script>
        
        <!-- peity (small charts) -->
        <script src="{{ URL::asset('theme/bower_components/peity/jquery.peity.min.js') }}"></script>
        
        <!-- easy-pie-chart (circular statistics) -->
        <script src="{{ URL::asset('theme/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
        
        <!-- countUp -->
        <script src="{{ URL::asset('theme/bower_components/countUp.js/dist/countUp.min.js') }}"></script>
        
        <!-- handlebars.js -->
        <script src="{{ URL::asset('theme/bower_components/handlebars/handlebars.min.js') }}"></script>
        
        <script src="{{ URL::asset('theme/assets/js/custom/handlebars_helpers.min.js') }}"></script>

        <!-- CLNDR -->
        <script src="{{ URL::asset('theme/bower_components/clndr/clndr.min.js') }}"></script>

        <!--  dashbord functions -->
        <script src="{{ URL::asset('theme/assets/js/pages/dashboard.min.js') }}"></script>

        

        <script src="{{ URL::asset('theme/assets/js/pages/forms_wizard.min.js') }}"></script>


 <script src="{{ URL::asset('theme/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <!-- datatables buttons-->
    <script src="{{ URL::asset('theme/bower_components/datatables-buttons/js/dataTables.buttons.js') }}"></script>
    <script src="{{ URL::asset('theme/assets/js/custom/datatables/buttons.uikit.js') }}"></script>
    <script src="{{ URL::asset('theme/bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('theme/bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ URL::asset('theme/bower_components/pdfmake/build/vfs_fonts.js') }}"></script>
    <script src="{{ URL::asset('theme/bower_components/datatables-buttons/js/buttons.colVis.js') }}"></script>
    <script src="{{ URL::asset('theme/bower_components/datatables-buttons/js/buttons.html5.js') }}"></script>
    <script src="{{ URL::asset('theme/bower_components/datatables-buttons/js/buttons.print.js') }}"></script>

    <!-- datatables custom integration -->
    <script src="{{ URL::asset('theme/assets/js/custom/datatables/datatables.uikit.min.js') }}"></script>

    <!--  datatables functions -->
    <script src="{{ URL::asset('theme/assets/js/pages/plugins_datatables.min.js') }}"></script>
    

        




    
    
    <!-- parsley (validation) -->
    <script>
        // load parsley config (altair_admin_common.js)
        altair_forms.parsley_validation_config();
    </script>

    <script src="{{ URL::asset('theme/bower_components/parsleyjs/dist/parsley.min.js') }}"></script>

    <!--  forms validation functions -->
    <script src="{{ URL::asset('theme/assets/js/pages/forms_validation.min.js') }}"></script>

    <script>
        $(function() {
            if(isHighDensity()) {
                $.getScript( "assets/js/custom/dense.min.js", function(data) {
                    // enable hires images
                    altair_helpers.retina_images();
                });
            }
            if(Modernizr.touch) {
                // fastClick (touch devices)
                FastClick.attach(document.body);
            }
        });

        $window.load(function() {
            // ie fixes
            altair_helpers.ie_fix();
        });
    </script>    



    @if (session('status'))
        <script type="text/javascript">
            UIkit.notify({
                message: '{{ session('status') }}',
                status: 'success',
                timeout: 5000,
                group: null,
                pos: 'top-center',
                onClose: function() {
                    $body.find('.md-fab-wrapper').css('margin-bottom','');
                    if($element.data('callback')) {
                        executeCallback($element.data('callback'));
                    }
                    // clear notify timeout (sometimes callback is fired more than once)
                    clearTimeout(thisNotify.timeout)
                }
            });
            // UIkit.modal.alert( '{{ session('status') }}')
        </script>
        session()->forgot('status');
    @endif


    @if(session('errror'))

        <script type="text/javascript">
            UIkit.notify({
                message: '{{ session('errror') }}',
                status: 'danger',
                timeout: 5000,
                group: null,
                pos: 'top-center',
                onClose: function() {
                    $body.find('.md-fab-wrapper').css('margin-bottom','');
                    if($element.data('callback')) {
                        executeCallback($element.data('callback'));
                    }
                    // clear notify timeout (sometimes callback is fired more than once)
                    clearTimeout(thisNotify.timeout)
                }
            });
            // UIkit.modal.alert( '{{ session('status') }}')
        </script>
        session()->forgot('errror');
    @endif


    @if(session('warning'))

        <script type="text/javascript">
            UIkit.notify({
                message: '{{ session('warning') }}',
                status: 'warning',
                timeout: 5000,
                group: null,
                pos: 'top-center',
                onClose: function() {
                    $body.find('.md-fab-wrapper').css('margin-bottom','');
                    if($element.data('callback')) {
                        executeCallback($element.data('callback'));
                    }
                    // clear notify timeout (sometimes callback is fired more than once)
                    clearTimeout(thisNotify.timeout)
                }
            });
            // UIkit.modal.alert( '{{ session('status') }}')
        </script>
        session()->forgot('warning');
    @endif


    @if(session('info'))

        <script type="text/javascript">
            UIkit.notify({
                message: '{{ session('info') }}',
                status: 'info',
                timeout: 5000,
                group: null,
                pos: 'top-center',
                onClose: function() {
                    $body.find('.md-fab-wrapper').css('margin-bottom','');
                    if($element.data('callback')) {
                        executeCallback($element.data('callback'));
                    }
                    // clear notify timeout (sometimes callback is fired more than once)
                    clearTimeout(thisNotify.timeout)
                }
            });
            // UIkit.modal.alert( '{{ session('status') }}')
        </script>
        session()->forgot('info');
    @endif