<!-- main sidebar -->
<style>
/* Logo Starts*/
    #sidebar_main .sidebar_main_header{background-image:url('Open.jpg')}
    .logo_regular{padding: 7px; margin: 5px; margin-left: 26px;}
/* Logo ends */

/* Selected Red Color */
    #sidebar_main .menu_section>ul>li.current_section>a .menu_title{color:#D72225;}
    #sidebar_main .menu_section>ul>li.current_section>a>.menu_icon .material-icons{color:#D72225;}
    #sidebar_main .menu_section>ul>li ul li.act_item>a {color:#D72225;}
/* Selected */

/* If Not Selected Grey Color */
    #sidebar_main .menu_section>ul>li>a>.menu_icon .material-icons{color:#516A7C;}
    #sidebar_main .menu_section>ul>li>a>.menu_title {color:#516A7C;}
    #sidebar_main .menu_section>ul>li ul li>a {color:#516A7C;}
/* Not Selected */
</style>
<aside id="sidebar_main">
    <div class="sidebar_main_header">
        <!-- <div class="sidebar_logo"> -->
            <a href="{{ route('home') }}" class="sSidebar_hide sidebar_logo_large">
                <img class="logo_regular" src="{{ URL::asset('public/Logo.png') }}" alt="" height="45" width="150"/>
            </a>
        <!-- </div> -->
    </div>
    
    <div class="menu_section">
        <ul>
            <li class="{{ (\Request::route()->getName() == 'home') ? 'submenu_trigger current_section act_section' : '' }}" title="Dashboard">
                <a href="{{ route('home') }}">
                    <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                    <span class="menu_title">Dashboard</span>
                </a>
            </li>
            
            <li title="Category" class="{{ (\Request::route()->getName() == 'add-category' || \Request::route()->getName() == 'view-category') ? 'submenu_trigger current_section act_section' : '' }}">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons"></i></span>
                    <span class="menu_title">Category</span>
                </a>
                <ul>
                    <li class="{{ (\Request::route()->getName() == 'add-category') ? 'act_item' : '' }}" ><a href="{{ route('add-category') }}">Add Category</a></li>
                    <li class="{{ (\Request::route()->getName() == 'view-category') ? 'act_item' : '' }}" ><a href="{{ route('view-category') }}">View Category</a></li>
                </ul>
            
            </li>
            {{--<li title="Sub Category" class="{{ (\Request::route()->getName() == 'add-subcategory' || \Request::route()->getName() == 'view-subcategory') ? 'submenu_trigger current_section act_section' : '' }}">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE1BD;</i></span>
                    <span class="menu_title">Sub Category</span>
                </a>
                <ul>
                    <li class="{{ (\Request::route()->getName() == 'add-subcategory') ? 'act_item' : '' }}" ><a href="{{ route('add-subcategory') }}">Add Sub-Category</a></li>
                    <li class="{{ (\Request::route()->getName() == 'view-subcategory') ? 'act_item' : '' }}" ><a href="{{ route('view-subcategory') }}">View Sub-Category</a></li>
                </ul>
            </li>--}}
            <li title="Product" class="{{ (\Request::route()->getName() == 'add-product' || \Request::route()->getName() == 'view-product') ? 'submenu_trigger current_section act_section' : '' }}" >
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE8F1;</i></span>
                    <span class="menu_title">Product</span>
                </a>
                <ul>
                    <li class="{{ (\Request::route()->getName() == 'add-product') ? 'act_item' : '' }}" ><a href="{{ route('add-product') }}">Add Product</a></li>
                    <li class="{{ (\Request::route()->getName() == 'view-product') ? 'act_item' : '' }}"><a href="{{ route('view-product') }}">View Product</a></li>
                </ul>
            </li>

            <li title="Category" class="{{ (\Request::route()->getName() == 'add-banner' || \Request::route()->getName() == 'view-banner') ? 'submenu_trigger current_section act_section' : '' }}">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons"></i></span>
                    <span class="menu_title">Banner</span>
                </a>
                <ul>
                    <li class="{{ (\Request::route()->getName() == 'add-banner') ? 'act_item' : '' }}" ><a href="{{ route('add-banner') }}">Add Banner</a></li>
                    <li class="{{ (\Request::route()->getName() == 'view-banner') ? 'act_item' : '' }}" ><a href="{{ route('view-banner') }}">View Banner</a></li>
                </ul>

            </li>

            <li title="User Profile" class="{{ (\Request::route()->getName() == 'all_users') ? 'submenu_trigger current_section act_section' : '' }}">
                <a href="{{ route('all_new_users') }}">
                    <span class="menu_icon"><i class="material-icons">&#xE87C;</i></span>
                    <span class="menu_title">Users</span>
                </a>
            </li>

            <li title="Ads" class="{{ (\Request::route()->getName() == 'all_ads') ? 'submenu_trigger current_section act_section' : '' }}">
                <a href="{{ route('all_ads') }}">
                    <span class="menu_icon"><i class="material-icons"></i></span>
                    <span class="menu_title">Ads</span>
                </a>
            </li>

            <li title="Reports" class="{{ (\Request::route()->getName() == 'all_coins') ? 'submenu_trigger current_section act_section' : '' }}">
                <a href="{{ route('coin') }}">
                    <span class="menu_icon"><i class="material-icons"></i></span>
                    <span class="menu_title">Coins</span>
                </a>
            </li>

            <li title="Reports" class="{{ (\Request::route()->getName() == 'all_reports') ? 'submenu_trigger current_section act_section' : '' }}">
                <a href="{{ route('all_reports') }}">
                    <span class="menu_icon"><i class="material-icons"></i></span>
                    <span class="menu_title">Reports</span>
                </a>
            </li>

            <li title="User Queries" class="{{ (\Request::route()->getName() == 'all_queries') ? 'submenu_trigger current_section act_section' : '' }}">
                <a href="{{ route('all_queries') }}">
                    <span class="menu_icon"><i class="material-icons">&#xE554;</i></span>
                    <span class="menu_title">Queries</span>
                </a>
            </li>
            
        </ul>
    </div>
</aside>
<!-- main sidebar end -->
