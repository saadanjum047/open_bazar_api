@extends('layout.base')
@section('content')
<style>
    .uk-pagination>li.uk-active>a, .uk-pagination>li.uk-active>span{background: #516A7C;}
    .icheck-inline{margin: 0px !important;} .modal-product{width: 66% !important;}
    .product-buttons{background-color:#516A7C;color:white;}
    .wrap-text{display: inline-block; width: 12em; overflow: hidden;white-space: nowrap;text-overflow: ellipsis;}
    .wrap-new{word-wrap: break-word;display: inline-block; width: 12em; overflow: hidden;}
</style>
<div id="page_content">
    <div id="page_content_inner">
        <h4 class="heading_a uk-margin-bottom">View Coins</h4>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                {{-- <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <a href="javascript:void(0)"  data-uk-modal="{target:'#modal_del'}"  class="md-btn md-btn-small md-btn-wave-light md-btn-icon waves-effect waves-button waves-light product-buttons"  title="Multiple Delete Products">
                            <i class="uk-icon-trash no_margin" style="color:white;"></i>
                        </a>
                        <div class="uk-modal" id="modal_del">
                            <div class="uk-modal-dialog">
                                <div class="uk-modal-header">
                                    <h3 class="uk-modal-title">Are you sure you want to delete selected coins? </h3>
                                </div>
                                <div class="uk-modal-content">
                                    <a href="javascript:void(0)" class="md-btn md-btn-small md-btn-wave-light md-btn-icon waves-effect waves-button waves-light delete_all" title="Multiple Delete Products" style="background-color:#516A7C;color:white;">
                                        Yes
                                    </a>
                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">No</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br> --}}
                <div class="uk-overflow-container">
                    <table id="dt_default" class="uk-table uk-table-striped uk-table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            {{-- <th>
                                <span class="icheck-inline">
                                    <input type="checkbox" class="ts_checkbox_all" name="checkbox_demo_inline_mercury" id="checkbox_demo_inline_1" data-md-icheck />
                                </span>
                            </th> --}}
                            <th> User Name </th>
                            <th> Title</th>
                            <th> Refered </th>
                            <th> Wallet Type </th>
                            <th> Coins </th>
                            <th> Type </th>
                            <th> Date </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(!$data->isEmpty())
                                @foreach($data as $key)
                                    <tr>
                                        {{-- <td>
                                            <span class="icheck-inline">
                                                <input type="checkbox" name="checkbox_demo_inline_mercury" class="sub_chk ts_checkbox" data-id="{{$key->id}}" id="checkbox_demo_inline_1" data-md-icheck />
                                            </span>
                                        </td> --}}
                                        <td> {{ $key->user_name }} </td>
                                        <td> <span class="wrap-text"> {{ $key->tital }} </span> </td>
                                        <td> <span class="wrap-text"> {{ $key->ad_title }} </span> </td>
                                        <td> 
                                            @if($key->wallet_type==0) 
                                                <span class="uk-badge uk-badge-danger md-bg-blue-grey-600"> Coins </span> 
                                            @else
                                                <span class="uk-badge uk-badge-primary md-bg-brown-600"> Credits </span>
                                            @endif
                                        </td>
                                        <td> {{ $key->coins }} </td>
                                        <td> 
                                            @if($key->type=="like") 
                                                <span class="uk-badge uk-badge-danger md-bg-red-600"> {{ ucfirst($key->type) }} </span> 
                                            @elseif($key->type=="post")
                                                <span class="uk-badge uk-badge-primary md-bg-purple-600"> {{ ucfirst($key->type) }} </span>
                                            @elseif($key->type=="invite")
                                                <span class="uk-badge uk-badge-primary md-bg-cyan-600"> {{ ucfirst($key->type) }} </span>
                                            @elseif($key->type=="credit")
                                                <span class="uk-badge uk-badge-primary md-bg-pink-600"> {{ ucfirst($key->type) }} </span>
                                            @else
                                                <span class="uk-badge uk-badge-primary md-bg-teal-600"> {{ ucfirst($key->type) }} </span>
                                            @endif
                                        </td>
                                        <td> {{ date('d-m-Y',($key->timestamp/1000)) }} </td>
                                        <td>
                                            <a href="javascript:void(0);" title="View" data-uk-modal="{target:'#modal_default{{$key->id}}'}"><i class="uk-icon-eye uk-icon-small"></i></a>
                                            &nbsp;&nbsp;
                                            
                                            {{-- <a href="#" onclick="UIkit.modal.confirm('Are you sure you want to delete ads? ', function(){ 
                                                var url = '{{ route("delete_reports", ":slug") }}';
                                                url = url.replace(':slug', '{{$key->id}}');
                                                window.location.href=url;
                                                });" title="DELETE">
                                                <i class="uk-icon-trash uk-icon-small"></i>
                                            </a> --}}
                                        </td>
                                    </tr>
                                    <div class="uk-modal" id="modal_default{{$key->id}}">
                                        <div class="uk-modal-dialog">
                                            <div class="uk-modal-header">
                                                <h3 class="uk-modal-title">Coin Details </h3>
                                            </div>
                                            <div class="uk-modal-content">
                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname">User Name &nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"><b> {{ $key->user_name }} </b></label>
                                                    </div>
                                                </div>

                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"> Title &nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"><b class="wrap-new">@if($key->tital!="") {{ $key->tital }} @else NA @endif</b></label>
                                                    </div>
                                                </div>

                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"> Refered &nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"><b class="wrap-new">@if($key->ad_title!="")  {{ $key->ad_title }}  @else NA @endif</b></label>
                                                    </div>
                                                </div>

                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"> Wallet Type &nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname">
                                                            <b>
                                                                @if($key->wallet_type==0) 
                                                                    <span class="uk-badge uk-badge-danger md-bg-blue-grey-600"> Coins </span> 
                                                                @else
                                                                    <span class="uk-badge uk-badge-primary md-bg-brown-600"> Credits </span>
                                                                @endif
                                                            </b>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"> Coins &nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"><b>@if($key->coins!="")  {{ $key->coins }}  @else NA @endif</b></label>
                                                    </div>
                                                </div>

                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"> Type No &nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname">
                                                            <b>
                                                                @if($key->type=="like") 
                                                                    <span class="uk-badge uk-badge-danger md-bg-red-600"> {{ ucfirst($key->type) }} </span> 
                                                                @elseif($key->type=="post")
                                                                    <span class="uk-badge uk-badge-primary md-bg-purple-600"> {{ ucfirst($key->type) }} </span>
                                                                @elseif($key->type=="invite")
                                                                    <span class="uk-badge uk-badge-primary md-bg-cyan-600"> {{ ucfirst($key->type) }} </span>
                                                                @elseif($key->type=="credit")
                                                                    <span class="uk-badge uk-badge-primary md-bg-pink-600"> {{ ucfirst($key->type) }} </span>
                                                                @else
                                                                    <span class="uk-badge uk-badge-primary md-bg-teal-600"> {{ ucfirst($key->type) }} </span>
                                                                @endif
                                                            </b>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="uk-grid" data-uk-grid-margin>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"> Date No &nbsp; :</label>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label for="fullname"> <b> {{ date('d-m-Y',($key->timestamp/1000)) }} </b> </label>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="uk-modal-footer uk-text-right">
                                                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8"> No Record Found</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ URL::asset('theme/assets/js/common.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".sub_chk").prop('checked', false);

        $('.ts_checkbox_all')
            .iCheck({
                checkboxClass: 'icheckbox_md',
                radioClass: 'iradio_md',
                increaseArea: '20%'
            })
            .on('ifChecked',function() {
                $('#dt_default')
                    .find('.ts_checkbox')
                    .prop('checked',true)
                    .iCheck('update')
                    .closest('tr')
                    .addClass('row_highlighted');
            })
            .on('ifUnchecked',function() {
                $('#dt_default')
                    .find('.ts_checkbox')
                    .prop('checked',false)
                    .iCheck('update')
                    .closest('tr')
                    .removeClass('row_highlighted');
            });
        $('.delete_all').on('click', function (e) {
            var allVals = [];
            $(".sub_chk:checked").each(function () {
                allVals.push($(this).attr('data-id'));
            });
            var join_selected_values = allVals;
            console.log(join_selected_values);
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: 'POST',
                url: "{{ route('reports_delete_multiple') }}",
                dataType: "JSON",
                data: {"_token": "{{ csrf_token() }}",id: join_selected_values},
                success: function (data) {
                    $(".sub_chk:checked").each(function () {
                        $(this).parents("tr").remove();
                    });
                    var modal = UIkit.modal("#modal_del");

                    if ( modal.isActive() ) {
                        modal.hide();
                    }

                        UIkit.notify({ message: 'Selected Report Deleted Successfully!!..', status: 'success', timeout: 5000, group: null, pos: 'top-center' });
                },
                error: function (data) {
                    var modal = UIkit.modal("#modal_del");
                    if ( modal.isActive() ) {
                        modal.hide();
                    }
                    UIkit.notify({ message: 'Something Went Wrong!!..', status: 'danger', timeout: 5000, group: null, pos: 'top-center' });
                }
            });
  
            $.each(allVals, function (index, value) {
                $('table tr').filter("[data-row-id='" + value + "']").remove();
            });
        });
    });
</script>
@endsection