<html>
    <head>
        <title>OpenBazar</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-offset-1 col-lg-10">
                    <div class="card">
                        {{-- <div class="card-header">
                            <img src="{{ URL::asset('public/Logo.png') }}" alt="" height="45" width="150" style="margin-left: 40%;">
                        </div>
                        <br> --}}
                        <div class="card-body">
                            <h3 class="card-title text-center">Welcome to Open Bazar!</h3>
                            <p class="card-text text-center">Please read this important legal information that sustains your use of the OpenBazar.nl platform and services.</p>
                            <h4 class="text-center">Terms of Use – OpenBazar.nl</h4>
                            <p class="text-center">Last modified: 03 December 2018</p>
                            <p>These Terms of Service provide you with information about the terms upon which we agree to provide, permit and allow you to access and use the Website. Please print a copy of these Terms of Service and refer to it as you use our Systems and Services. You agree to be bound by these Terms of Service if you continue using the Website. If you do not agree to these Terms of Service, you must immediately refrain from using the Website and the Services.</p>
                            <p><b>Your account:</b> Some services in the Open Bazar require you to create your own account with your email and password. This e-mail will be the authorized e-mail. You will be responsible for the confidentiality of the password for your account. Responsible for all activities issued by him. In order to do this you must protect your own password and choose a word that is hard to reach by others. In turn, we advise all users to put their first and last names in the "Username".</p>
                            <p>When you use the Open Bazar you agree to the collection, transfer, storage and use of your personal information on various other sites by the Site.</p>
                            <p>You may also log in and create an account on the Site through third parties services such as Facebook or Google, which authorizes us to access, store and use your personal information as it is an accessible service as described in our Privacy Policy. If your account is compromised or misused, please contact our customer service team immediately.</p>
                            <p><b>Open Bazar Rules:</b> You can post to any of our sections while avoiding the following:</p>
                            <ul>
                                <li>Posting false or misleading ads.</li>
                                <li>Copy, edit, or post content to another person.</li>
                                <li>Any acts of sabotage such as attempts to block the service or impose an unreasonable burden on the infrastructure of the site.</li>
                                <li>Violation of any rights of any other party.</li>
                                <li>Use any prohibited means to access the site database and collect content for any purpose, including bot, spider web and similar methods.</li>
                                <li>Publish any abusive mail, serial messages, or pyramid marketing scheme.</li>
                                <li>Spreading viruses or any other technology that would harm the site or the interests and property of users or anyone.</li>
                                <li>Violate any of the laws or any point of our prohibited content policy.</li>
                                <li>Collect information about other users including your email or other personal information.</li>
                                <li>Override actions used to block or restrict access to the site.</li>
                                <li>Use personal information about other people without their explicit consent.</li>
                            </ul>
                            <p>Any POST containing one or more of the prohibited things below will be deleted. The site reserves the right to permanently delete the user's account, blacklist it, reporting the content and the user information to Law Enforcement or the appropriate Authority. </p>
                            <ul>
                                <li>Tobacco products, narcotic drugs, psychotropic substances, liqueurs, medicines and analgesics, or even the establishment of links, whether direct or indirect, of articles, products or services prohibited by law.</li>
                                <li>Religious materials, including books, antiques, etc., or any information or description of any of these materials, may affect the religious feelings of any person or group.</li>
                                <li>Counterfeit or stolen goods or illegal and authorized services.</li>
                                <li>Prostitution or any other services, including the violation of the provisions of the law immoral.</li>
                                <li>Information and material indicating libel, defamation, threat or even abuse.</li>
                                <li>Human or natural human organs including blood and body fluids as well.</li>
                                <li>Goods, materials and services that infringe on the rights and intellectual property of any third party, or even the privacy of any person.</li>
                                <li>Police and army equipment of official badges, uniforms, coats, weapons, and other materials that prevent circulation.</li>
                                <li>Weapons and related tools (eg, firearms, ammunition, tear gas, rifles and sharp objects).</li>
                                <li>Posting Ads to Open Bazar competition sites or companies.</li>
                                <li>Phantom projects (such as: get rich quick and work from home).</li>
                                <li>Hazardous chemicals and pesticides.</li>
                            </ul>
                            <p><b>Reporting Ad:</b>Please use the report system to inform us of any problems or inappropriate content to work together to keep track of the services provided by the site. In turn, we will terminate our services for those who misuse the site, remove their content, and take legal action to safeguard the rights of other users. If any such action is taken, the site is not responsible for the nature of any inappropriate content that has been published and its subsequent consequences.</p>
                            <p>Report any of the below practices:</p>
                            <ul>
                                <li>Create more than one account.</li>
                                <li>Duplicate Ad serving.</li>
                                <li>Posting Ads with wrong or inappropriate images.	</li>
                                <li>Upload photos with names and phone numbers.</li>
                                <li>Post Ads with unreasonable prices.</li>
                                <li>Spread fraud and fake Ads.</li>
                                <li>Deploy Ads with misleading titles, content, and images.</li>
                                <li>Post Ads with a prepay option or bank transfer.	</li>
                                <li>Post Ads with public content or public content.	</li>
                                <li>Posting items that can be worn without uploading images.</li>
                                <li>Publish Ads in wrong sections.</li>
                                <li>Post Ads with multiple items.</li>
                                <li>Advertising agencies.</li>
                                <li>Post Ads with redirect links to other sites.</li>
                            </ul>
                            <p>Reporting Violations of Intellectual Property Rights Users are prohibited from posting any content that infringes the rights of third parties; this includes, but is not limited to, infringement of intellectual property rights and trademarks (eg, advertising of fictitious material for sale). We have the right to remove any content that violates the terms of our publishing policy and protect the rights of others. If you feel that one of our ads violates your trademark and trademark rights, all you have to do is notify our customer service department, and only those who can submit a notification against the terms and conditions you are likely to contact will be notified                                </p>
                            <p><b>Charges and Services:</b> In general, advertising on the Open Bazar is for free, but we charge a fee for some of the services we provide to users. If you use a service for which we charge a fee, you will be required to review and approve it. Payment will be made in local currency. We may change this mechanism from time to time and we will notify you of any change to our payment policy by posting it on the site. The payment method is temporary when you post new promotions or services. On the other hand, these fees are non-refundable and you must pay them. If you do not, the validity of the service you use will be cancelled.  </p>
                            <p><b>Disclaimers and Limitations of Liability:</b> Open Bazar Services are provided as they exist or are available, and as a User you agree not to hold the Site responsible for any content posted by other users including but not limited to: advertisements or direct messages between users. The site does not guarantee the accuracy of the advertisements or the existing means of communication or the extent of security or even compliance with the laws in them, because most of what is posted on the site by users, in addition to the lack of continuous access to services, Out of our control; including the postponement or delay due to your location or the Internet. TO THE MAXIMUM EXTENT PERMITTED BY LAW, WE DISCLAIM ALL WARRANTIES, CONDITIONS AND CONDITIONS EXPRESS OR IMPLIED, INCLUDING THE QUALITY, MERCHANTABILITY, MERCHANTABILITY, FITNESS AND RELIABILITY OF THE PURPOSE FOR THE PURPOSE. We are also not responsible for any loss, whether by money (including profit), good reputation or any special or indirect damages resulting from your use of the Site, even if we have been informed of or expect such a thing. Some jurisdictions do not allow waiver of warranties or the exclusion of damages, so such disclaimers and exceptions may not apply to you.                            </p>
                            <p><b>Open Bazar Wallet:</b> Is a pre-payment system that allows registered members on the Open Bazar to buy credit to be used to purchase services offered as Featured Ads, Bump Up Ads and upgrading membership by the Open Bazar. The balance of the Open Bazar portfolio cannot be transferred to   other users and it is not replaced by money and the user can not earn interest or financial gain, in addition portfolio balances purchased are final and irrevocable.
                                Any dispute relating to the Open Bazar portfolio must be reported within (24) hours from the time of the user's discovery of a problem. Otherwise, the user has waived the right to file any claim against the Open Bazar site.
                                Each Open Bazar Wallet credit purchased by a registered user has no expiration. However, once a user’s account becomes inactive, or is deactivated, or otherwise terminated, all Open Bazar Wallet credits, if any, shall automatically be forfeited.
                                Open Bazar reserves the right to suspend the sale of portfolio balances or change the cost, as well as the way in which the registered user can buy, store or use the Open Bazar portfolio.
                                Open Bazar reserves the right to confiscate or cancel any portfolio balances purchased directly or indirectly or obtained through fraudulent or illegal methods without further notice to the user.
                                In case Open Bazar Administration finds reasons or evidence to confirm that any User has violated one of the terms of use of the Wallet or other terms of use of the Site, you shall prevent the use of the Open Bazar Portfolio and delete your Ads whether paid or free.
                                </p>
                            <p><b>Open Bazar Coins:</b> is an Affiliate system which allows registered members on the Open Bazar to collect Coins by placing an Ad, inviting friends or like Ads and redeem them to credit in Open Bazar wallet to be used to purchase services offered as Featured Ads, Bump Up Ads and upgrading membership by the Open Bazar. The balance of the Open Bazar coins cannot be transferred to other users and it is not replaced by money and the user can not earn interest or financial gain.
                                Any dispute relating to the Open Bazar coins must be reported within (24) hours from the time of the user's discovery of a problem. Otherwise, the user has waived the right to file any claim against the Open Bazar site.
                                Each Open Bazar Coins collected by a registered user has no expiration. However, once a user’s account becomes inactive, or is deactivated, or otherwise terminated, all Open Bazar Coins, if any, shall automatically be forfeited.
                                Open Bazar reserves the right to suspend coins affiliate system or change the cost, as well as the way in which the registered user can collect, store or use the Open Bazar Coins.
                                Open Bazar reserves the right to confiscate or cancel any Coins balances collected directly or indirectly or obtained through fraudulent or illegal methods without further notice to the user.
                                In case Open Bazar Administration finds reasons or evidence to confirm that any User has violated one of the terms of use of the Coins or other terms of use of the Site, you shall prevent the use of the Open Bazar Portfolio and delete your Ads whether paid or free.
                                </p>
                            <p><b>Rights and Knowledgeable:</b> Open Bazar market has all rights, ownership and interests of its applications, including patent rights, copyrights, secrets and trademarks, and all other proprietary rights that include additions, updates and modifications to its applications. The use of code, software engineering, symbols, or otherwise, may not be modified, translated, or modified from any other application, and the copyright, trademark and intellectual property notices contained within or upon access may not be removed, Any of the open market applications.
                                You acknowledge and agree that any questions, comments, suggestions, ideas, comments or other information about the Site and / or the Open Bazar service you provide to us are not confidential and may be used by us at our discretion.
                                These were our general conditions. If we change the conditions in the future, you will find this on our platform. We make sure that changes are not detrimental to you.
                                </p>
                        </div>
                        <div class="card-footer">
                            <h5>Open Bazar<h5>
                            <p>@All Rights Reserved 2017-2018 OpenBazar.nl</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>


