@extends('layout.base')
@section('content')
<div id="page_content">
    <div id="page_content_inner">
        <!-- statistics (small charts) -->
        <div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin>
            <div>
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_visitors peity_data">5,3,9,6,5,9,7</span></div>
                        <span class="uk-text-muted uk-text-small">Total Users</span>
                        <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>{{ $user }}</noscript></span></h2>
                    </div>
                </div>
            </div>
            <div>
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_sale peity_data">5,3,9,6,5,9,7,3,5,2</span></div>
                        <span class="uk-text-muted uk-text-small">Ads</span>
                        <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>{{ $ads }}</noscript></span></h2>
                    </div>
                </div>
            </div>
            <div>
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">64/100</span></div>
                        <span class="uk-text-muted uk-text-small">Today Ads</span>
                        <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>{{ $today_ads }}</noscript></span></h2>
                    </div>
                </div>
            </div>
            <div>
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">64/100</span></div>
                        <span class="uk-text-muted uk-text-small">Today Joined User</span>
                        <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>{{ $today_user }}</noscript></span></h2>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin>
            <div>
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_sale peity_data">5,3,9,6,5,9,7,3,5,2</span></div>
                        <span class="uk-text-muted uk-text-small">Total IOS User</span>
                        <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>{{ $ios }}</noscript></span></h2>
                    </div>
                </div>
            </div>
            <div>
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_visitors peity_data">5,3,9,6,5,9,7</span></div>
                        <span class="uk-text-muted uk-text-small">Total Android Users</span>
                        <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>{{ $android }}</noscript></span></h2>
                    </div>
                </div>
            </div>
            <div>
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-float-right uk-margin-top uk-margin-small-right"><i class="material-icons" style="font-size: 35px; color:#D72225;">&#xE0BE;</i></div>
                        <span class="uk-text-muted uk-text-small">Today Message Received</span>
                        <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>{{ $today_msg }}</noscript></span></h2>
                    </div>
                </div>
            </div>
        </div>

        <!-- large chart -->
        {{-- <div class="uk-grid">
            <div class="uk-width-1-1">
                <div class="md-card">
                    <div class="md-card-toolbar">
                        <div class="md-card-toolbar-actions">
                            <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
                            <i class="md-icon material-icons">&#xE5D5;</i>
                            <div class="md-card-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                                <i class="md-icon material-icons">&#xE5D4;</i>
                                <div class="uk-dropdown uk-dropdown-small">
                                    <ul class="uk-nav">
                                        <li><a href="#">Action 1</a></li>
                                        <li><a href="#">Action 2</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h3 class="md-card-toolbar-heading-text">
                            Chart
                        </h3>
                    </div>
                    <div class="md-card-content">
                        <div class="mGraph-wrapper">
                            <div id="mGraph_sale" class="mGraph" data-uk-check-display></div>
                        </div>
                        <div class="md-card-fullscreen-content">
                            <div class="uk-overflow-container">
                                <table class="uk-table uk-table-no-border uk-text-nowrap">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Best Seller</th>
                                        <th>Total Sale</th>
                                        <th>Change</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>January 2014</td>
                                        <td>Non suscipit qui fuga.</td>
                                        <td>$3 234 162</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>February 2014</td>
                                        <td>Voluptatem et sint at.</td>
                                        <td>$3 771 083</td>
                                        <td class="uk-text-success">+2.5%</td>
                                    </tr>
                                    <tr>
                                        <td>March 2014</td>
                                        <td>Et magnam mollitia.</td>
                                        <td>$2 429 352</td>
                                        <td class="uk-text-danger">-4.6%</td>
                                    </tr>
                                    <tr>
                                        <td>April 2014</td>
                                        <td>Doloribus fugiat blanditiis quam.</td>
                                        <td>$4 844 169</td>
                                        <td class="uk-text-success">+7%</td>
                                    </tr>
                                    <tr>
                                        <td>May 2014</td>
                                        <td>Qui magnam ipsum dolor.</td>
                                        <td>$5 284 318</td>
                                        <td class="uk-text-success">+3.2%</td>
                                    </tr>
                                    <tr>
                                        <td>June 2014</td>
                                        <td>Reprehenderit voluptas non.</td>
                                        <td>$4 688 183</td>
                                        <td class="uk-text-danger">-6%</td>
                                    </tr>
                                    <tr>
                                        <td>July 2014</td>
                                        <td>Non repellendus maiores.</td>
                                        <td>$4 353 427</td>
                                        <td class="uk-text-success">-5.3%</td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                            <p class="uk-margin-large-top uk-margin-small-bottom heading_list uk-text-success">Some Info:</p>
                            <p class="uk-margin-top-remove">Dolorem quia tempora maxime et occaecati qui aliquid sed eum earum nisi ut cumque earum labore et facere vel quis totam a nulla qui deleniti et nobis accusamus illo numquam repudiandae debitis ut nostrum debitis aliquam qui omnis minima animi fugiat dolorem optio facilis in tempore modi tempora est optio ratione reiciendis qui magni ipsum sed eum aspernatur cumque natus eum iste et autem doloribus rerum ea architecto nulla sequi est est nostrum architecto animi porro quasi voluptates sit iure autem numquam debitis dicta corrupti facere aperiam tempore assumenda et assumenda dolor molestias ut veritatis voluptatem placeat nobis enim expedita ratione.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>  --}}

        <!-- circular charts -->
        {{-- <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-xlarge-1-5 uk-text-center uk-sortable sortable-handler" id="dashboard_sortable_cards" data-uk-sortable data-uk-grid-margin>
            <div>
                <div class="md-card md-card-hover md-card-overlay">
                    <div class="md-card-content">
                        <div class="epc_chart" data-percent="76" data-bar-color="#03a9f4">
                            <span class="epc_chart_icon"><i class="material-icons">&#xE0BE;</i></span>
                        </div>
                    </div>
                    <div class="md-card-overlay-content">
                        <div class="uk-clearfix md-card-overlay-header">
                            <i class="md-icon material-icons md-card-overlay-toggler">&#xE5D4;</i>
                            <h3>
                                User Messages
                            </h3>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias consectetur.
                    </div>
                </div>
            </div>
            <div>
                <div class="md-card md-card-hover md-card-overlay">
                    <div class="md-card-content uk-flex uk-flex-center uk-flex-middle">
                        <span class="peity_conversions_large peity_data">5,3,9,6,5,9,7</span>
                    </div>
                    <div class="md-card-overlay-content">
                        <div class="uk-clearfix md-card-overlay-header">
                            <i class="md-icon material-icons md-card-overlay-toggler">&#xE5D4;</i>
                            <h3>
                                Conversions
                            </h3>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    </div>
                </div>
            </div>
            <div>
                <div class="md-card md-card-hover md-card-overlay md-card-overlay-active">
                    <div class="md-card-content" id="canvas_1">
                        <div class="epc_chart" data-percent="37" data-bar-color="#9c27b0">
                            <span class="epc_chart_icon"><i class="material-icons">&#xE85D;</i></span>
                        </div>
                    </div>
                    <div class="md-card-overlay-content">
                        <div class="uk-clearfix md-card-overlay-header">
                            <i class="md-icon material-icons md-card-overlay-toggler">&#xE5D4;</i>
                            <h3>
                                Tasks List
                            </h3>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <button class="md-btn md-btn-primary">More</button>
                    </div>
                </div>
            </div>
            <div>
                <div class="md-card md-card-hover md-card-overlay">
                    <div class="md-card-content">
                        <div class="epc_chart" data-percent="53" data-bar-color="#009688">
                            <span class="epc_chart_text"><span class="countUpMe">53</span>%</span>
                        </div>
                    </div>
                    <div class="md-card-overlay-content">
                        <div class="uk-clearfix md-card-overlay-header">
                            <i class="md-icon material-icons md-card-overlay-toggler">&#xE5D4;</i>
                            <h3>
                                Orders
                            </h3>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    </div>
                </div>
            </div>
            <div>
                <div class="md-card md-card-hover md-card-overlay">
                    <div class="md-card-content">
                        <div class="epc_chart" data-percent="37" data-bar-color="#607d8b">
                            <span class="epc_chart_icon"><i class="material-icons">&#xE7FE;</i></span>
                        </div>
                    </div>
                    <div class="md-card-overlay-content">
                        <div class="uk-clearfix md-card-overlay-header">
                            <i class="md-icon material-icons md-card-overlay-toggler">&#xE5D4;</i>
                            <h3>
                                User Registrations
                            </h3>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    </div>
                </div>
            </div>
        </div> --}}

        <!-- tasks -->
        <div class="uk-grid" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
            <div class="uk-width-medium-1-2">
                <div class="md-card">
                    <div class="md-card-content">
                        <h3 class="heading_a uk-margin-bottom uk-text-center">Today Ads</h3>
                        <div class="uk-overflow-container">
                            <table class="uk-table uk-table-striped uk-table-hover">
                                <thead>
                                    <tr>
                                        <th class="uk-text-nowrap">User</th>
                                        <th class="uk-text-nowrap">Ad Title</th>
                                        <th class="uk-text-nowrap">Start Date</th>
                                        <th class="uk-text-nowrap uk-text-right">Expiry Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!$data->isEmpty())
                                        @foreach($data as $key)
                                            <tr class="uk-table-middle">
                                                <td class="uk-width-3-10 uk-text-nowrap">{{ $key->user_name }}</td>
                                                <td class="uk-width-2-10 uk-text-nowrap">{{ $key->ad_title }}</td>
                                                <td class="uk-width-3-10"> {{ date('d-m-Y',($key->timestamp/1000)) }} </td>
                                                <td class="uk-width-2-10 uk-text-right uk-text-muted uk-text-small"> {{ date('d-m-Y',$key->expired_date) }} </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4">No Data Found</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            <a href="{{ route('all_ads') }}" titel="View All" style="float:right;"><i>View all</i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-medium-1-2">
                <div class="md-card">
                    <div class="md-card-content">
                        <h3 class="heading_a uk-margin-bottom uk-text-center">Today Users Joined</h3>
                        {{-- <div id="ct-chart" class="chartist"></div> --}}
                        <div class="uk-overflow-container">
                            <table class="uk-table uk-table-striped uk-table-hover">
                                <thead>
                                    <tr>
                                        <th class="uk-text-nowrap">User</th>
                                        <th class="uk-text-nowrap">Email</th>
                                        <th class="uk-text-nowrap">Mobile No</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!$data_user->isEmpty())
                                        @foreach($data_user as $k)
                                            <tr class="uk-table-middle">
                                                <td class="uk-width-3-10 uk-text-nowrap">{{ $k->name }}</td>
                                                <td class="uk-width-2-10 uk-text-nowrap">{{ $k->email }}</td>
                                                <td class="uk-width-3-10"> @if($k->phone_no!="") {{ $k->phone_no }} @else NA @endif </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4">No Data Found</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            <a href="{{ route('all_users') }}" titel="View All" style="float:right;"><i>View all</i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--  <div class="uk-grid" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
            <div class="uk-width-medium-1-1">
                <div class="md-card">
                    <div class="md-card-content">
                        <h3 class="heading_a uk-margin-bottom uk-text-center">All Users</h3>
                        <div class="uk-overflow-container">
                            <table id="dt_default" class="uk-table uk-table-striped uk-table-hover" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th> Name </th>
                                        <th> Email </th>
                                        <th> Mobile </th>
                                        <th> Image </th>
                                        <th> App Type </th>
                                        <th> Total Coin </th>
                                        <th> Total Credit </th>
                                        <th> Signup <br> Type </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!$all->isEmpty())
                                        @foreach($all as $key)
                                            <tr>
                                                <td> <span class="wrap-new"> {{ $key->name }} </span> </td>
                                                <td> <span class="wrap-text"> @if($key->email!="")  {{ $key->email }}  @else NA @endif </span> </td>
                                                <td> @if($key->phone_no!="")  {{ $key->phone_no }}  @else NA @endif </td>
                                                <td>
                                                    @if($key->profile_image!="")
                                                        @if (filter_var($key->profile_image, FILTER_VALIDATE_URL)) 
                                                            <img src="{{ $key->profile_image }}"  height="30" width="50"/>
                                                        @else 
                                                            <img src="{{ URL::asset('/images/'.$key->profile_image) }}"  height="30" width="50"/>
                                                        @endif
                                                    @else
                                                        NA
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($key->app_type=="Android") 
                                                        <span class="uk-badge uk-badge-danger md-bg-blue-grey-600"> Android </span> 
                                                    @else
                                                        <span class="uk-badge uk-badge-primary md-bg-brown-600"> iOS </span>
                                                    @endif
                                                </td>
                                                <td> {{ $key->coin }} </td>
                                                <td> {{ $key->credit }} </td>
                                                <td>
                                                    @if($key->signup_type!="")  
                                                        @if($key->signup_type=="google")
                                                            <span class="uk-badge uk-badge-danger">{{ strtoupper($key->signup_type) }}</span>
                                                        @elseif($key->signup_type=="facebook")
                                                            <span class="uk-badge uk-badge-primary">{{ strtoupper($key->signup_type) }}</span>
                                                        @else
                                                            <span class="uk-badge uk-badge-success">{{ strtoupper($key->signup_type) }}</span>
                                                        @endif
                                                    @else
                                                        NA
                                                    @endif
                                                </td>
                                            </tr>
        
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7"> No Record Found</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            <a href="{{ route('all_users') }}" titel="View All" style="float:right;"><i>View all</i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>  --}}
    </div>
</div>
@endsection
