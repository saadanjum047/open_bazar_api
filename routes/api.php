<?php

use Illuminate\Http\Request;

Route::get('/clear/route',function(){
	$exitCode = \Artisan::call('cache:clear');
	 $exitCode = \Artisan::call('config:cache');
	 return $exitCode;
});
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     // return $request->user();
//     Route::post('update_profile','api\profile_setup@update_profile');
// });
//Route::post('/boost_ads_with_gateway','api1\ApiController@boost_ads_with_gateway');
Route::group(['middleware' => 'auth:api'], function() {
	
	// profile
		Route::post('update_profile','api\profile_setup@update_profile');
		Route::post('update_city','api\profile_setup@update_city');
		Route::post('user_detail','api\profile_setup@user_detail');
	

	// post
		Route::post('insert_ads','api\post_ads@insert_ads');	//Post ad
		Route::post('update_ads','api\post_ads@update_ads'); // Update ad

		Route::post('similar_ads','api\post_ads@similar_ads');
		Route::post('get_adposts1','api\post_ads@get_adposts1');
		
		Route::post('ad_view','api\post_ads@ad_view');
		Route::post('get_adposts1','api\post_ads@get_adposts1');
		Route::post('offer_bid','api\post_ads@offer_bid');
		Route::post('get_offer_bid','api\post_ads@get_offer_bid'); // get post offer bid
		Route::post('delete_offers','api\post_ads@delete_offers'); // delete post offer bid
		Route::post('delete_ads','api\post_ads@delete_ads'); // delete post offer bid
	
	// account
	Route::post('get_total_credits','api\accountcredits@get_total_credits');
		
		Route::post('get_mycoins','api\accountcredits@get_mycoins');
		Route::post('get_myads','api\accountcredits@get_myads');
		Route::post('redeem_coin','api\accountcredits@redeem_coin');
		Route::post('buy_credit','api\accountcredits@buy_credit');
		Route::post('my_followers','api\accountcredits@my_followers');
		Route::post('my_followings','api\accountcredits@my_followings');
		Route::post('other_followings','api\accountcredits@other_followings');
		Route::post('other_followers','api\accountcredits@other_followers');
		
		Route::post('my_account','api\accountcredits@my_account');

		Route::post('fetch_notification','api\post_ads@fetch_notification');
		Route::post('notification_status','api\ApiController@notification_status');
	Route::post('getpost_detail','api\post_ads@getpost_detail');

		Route::post('post_report','api\post_ads@post_report'); 
			
		Route::post('user_details','api\profile_setup@user_details');
		Route::post('follow','api\profile_setup@follow');

		Route::post('update_mobilenumber','api\profile_setup@update_mobilenumber');
		Route::post('recent_view','api\profile_setup@recent_view');
		Route::post('my_favorite','api\profile_setup@my_favorite');
		Route::post('my_offerads','api\profile_setup@my_offerads');
		Route::post('my_followingsads','api\profile_setup@my_followingsads');


	// Contact us api
		Route::post('/contact_us','api\ApiController@contact_us');

	// Count ads category api
		Route::post('/ad_counts','api\ApiController@ad_counts');

	// Search api
		Route::post('/search_ads','api\ApiController@search_ads');
	
	// Invitation email
		Route::post('/invitation_email','api\ApiController@invitation_email');

	// Block user
		Route::post('/block_user','api\ApiController@block_user');

	// List of Blocked Users
		Route::post('/blocked_list','api\ApiController@blocked_list');

	// Boost ads
		Route::post('/boost_ads','api\ApiController@boost_ads');

		Route::post('/get_all_post','api\ApiController@get_all_post');

		Route::post('/temp','api\ApiController@temp');
});

// profile setup api
Route::post('signup','api\profile_setup@signup');
Route::post('emailotp','api\profile_setup@emailotp');
Route::post('login','api\profile_setup@login');
Route::post('reset_password','api\profile_setup@reset_password');
Route::post('sociallogin','api\profile_setup@sociallogin');
Route::post('get_category','api\post_ads@get_category');
Route::post('get_subcategory','api\post_ads@get_subcategory');
Route::post('get_productcat','api\post_ads@get_productcat'); // get sub cat's products
Route::post('uploadimage','api\post_ads@uploadimage');
Route::post('follow_users','api\post_ads@follow_users'); 
Route::post('update_mobileverification','api\post_ads@update_mobileverification'); 
Route::post('myprofile','api\accountcredits@myprofile');
Route::post('verify_social_account','api\profile_setup@verify_social_account');

// Logout api
Route::post('logout','api\ApiController@logout');
Route::post('sendpush_message','api\ApiController@sendpush_message');
Route::post('send_push','api\ApiController@send_push');



Route::group(['prefix' => 'v1'], function () {
	Route::group(['middleware' => 'auth:api'], function() {
	
                 Route::post('buy_credit','api1\accountcredits@buy_credit');

		// profile
			Route::post('update_profile','api1\profile_setup@update_profile');
			Route::post('update_city','api1\profile_setup@update_city');
			Route::post('user_detail','api1\profile_setup@user_detail');
			;

			Route::post('update_country','api\v1\profile_setup@update_country');
		// post
			Route::post('insert_ads','api1\post_ads@insert_ads');	//Post ad
			Route::post('update_ads','api1\post_ads@update_ads'); // Update ad
	
			Route::post('similar_ads','api1\post_ads@similar_ads');
			Route::post('get_adposts1','api1\post_ads@get_adposts1');
			
			Route::post('ad_view','api1\post_ads@ad_view');
			Route::post('get_adposts1','api1\post_ads@get_adposts1');
			Route::post('offer_bid','api1\post_ads@offer_bid');
			Route::post('get_offer_bid','api1\post_ads@get_offer_bid'); // get post offer bid
			Route::post('delete_offers','api1\post_ads@delete_offers'); // delete post offer bid
			Route::post('delete_ads','api1\post_ads@delete_ads'); // delete post offer bid
		
		// account
		Route::post('get_total_credits','api1\accountcredits@get_total_credits');
			
			Route::post('get_mycoins','api1\accountcredits@get_mycoins');
			Route::post('get_myads','api1\accountcredits@get_myads');
			Route::post('redeem_coin','api1\accountcredits@redeem_coin');
			Route::post('my_followers','api1\accountcredits@my_followers');
			Route::post('my_followings','api1\accountcredits@my_followings');
			Route::post('other_followings','api1\accountcredits@other_followings');
			Route::post('other_followers','api1\accountcredits@other_followers');
			
			Route::post('my_account','api1\accountcredits@my_account');
	
			Route::post('fetch_notification','api1\post_ads@fetch_notification');
			Route::post('notification_status','api1\ApiController@notification_status');
		Route::post('getpost_detail','api1\post_ads@getpost_detail');
	
			Route::post('post_report','api1\post_ads@post_report'); 
				
			Route::post('user_details','api1\profile_setup@user_details');
			Route::post('follow','api1\profile_setup@follow');
	
			Route::post('update_mobilenumber','api1\profile_setup@update_mobilenumber');
			Route::post('recent_view','api1\profile_setup@recent_view');
			Route::post('my_favorite','api1\profile_setup@my_favorite');
			Route::post('my_offerads','api1\profile_setup@my_offerads');
			Route::post('my_followingsads','api1\profile_setup@my_followingsads');
	
	

			Route::post('upgrade_account','api1\accountcredits@upgrade_account');

				
		// Contact us api
			Route::post('/contact_us','api1\ApiController@contact_us');
	
		// Count ads category api
			Route::post('/ad_counts','api1\ApiController@ad_counts');
	
		// Search api
			Route::post('/search_ads','api1\ApiController@search_ads');
		
		// Invitation email
			Route::post('/invitation_email','api1\ApiController@invitation_email');
	
		// Block user
			Route::post('/block_user','api1\ApiController@block_user');
	
		// List of Blocked Users
			Route::post('/blocked_list','api1\ApiController@blocked_list');
	
		// Boost ads
			Route::post('/boost_ads','api1\ApiController@boost_ads');

			Route::post('/boost_ads_with_gateway','api1\ApiController@boost_ads_with_gateway');
	
			Route::post('/get_all_post','api1\ApiController@get_all_post');
	
			Route::post('/temp','api1\ApiController@temp');
	});
	
	// profile setup api

	Route::post('home_data','api1\ApiController@home_data');
	
	Route::post('signup','api1\profile_setup@signup');
	Route::any('emailotp','api1\profile_setup@emailotp');
	Route::post('login','api1\profile_setup@login');
	Route::post('reset_password','api1\profile_setup@reset_password');
	Route::post('sociallogin','api1\profile_setup@sociallogin');

	Route::post('get_category','api1\post_ads@get_category');
	Route::post('get_subcategory','api1\post_ads@get_subcategory');
	Route::post('get_productcat','api1\post_ads@get_productcat'); // get sub cat's products
	Route::post('dummy','api1\DummyController@get_productcat');

             Route::post('getallbanner','api\v1\profile_setup@getallbanner');

	Route::post('uploadimage','api1\post_ads@uploadimage');
	Route::post('follow_users','api1\post_ads@follow_users'); 
	Route::post('update_mobileverification','api1\post_ads@update_mobileverification'); 
	Route::post('myprofile','api1\accountcredits@myprofile');
	Route::post('verify_social_account','api1\profile_setup@verify_social_account');

	Route::post('get_productcat_new','api1\post_ads@get_productcat_new');
	
	// Logout api
	Route::post('logout','api1\ApiController@logout');
	Route::post('sendpush_message','api1\ApiController@sendpush_message');
	Route::post('send_push','api1\ApiController@send_push');
	Route::post('chat_push','api1\post_ads@chat_push');
	
  });




