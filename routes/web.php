<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/terms-of-use', function () {

    return view('terms_of_use');
});
Route::get('/privacy-policy', function () {

    return view('privacy_policy');
});
Route::get('/', array('as' => 'login', 'uses' => 'LoginController@login'));
// Route::get('/terms-of-use',array('as'=>'terms_of_use','uses'=>'HomeController@terms_of_use'));
// Route::get('/privacy_policy',array('as'=>'privacy_policy','uses'=>'HomeController@privacy_policy'));


Auth::routes();

Route::middleware('auth')->group(function () {
    Route::group(['middleware' => ['role:admin']], function () {
        Route::get('/home', 'HomeController@index')->name('home');

        // Route::get('/my-profile',array('as'=>'my_profile','uses'=>'HomeController@my_profile'));
        // Category
        Route::get('add-category',array('as'=>'add-category','uses'=>'HomeController@add_category'));
        Route::post('add-category',array('as'=>'addcategory','uses'=>'HomeController@_addcategory'));
        Route::get('view-category',array('as'=>'view-category','uses'=>'HomeController@view_category'));
        Route::get('edit-category/{id}',array('as'=>'edit-category','uses'=>'HomeController@edit_category'));
        Route::post('edit-category',array('as'=>'editcategory','uses'=>'HomeController@_editcategory'));
        Route::get('delete-category/{id}',array('as'=>'deletecategory','uses'=>'HomeController@delete_category'));
        Route::post('status_category',array('as'=>'status_category','uses'=>'HomeController@status_category'));

        // Sub Category
        Route::get('add-subcategory/{id}',array('as'=>'add-subcategory','uses'=>'HomeController@add_subcategory'));
        Route::post('add-subategory',array('as'=>'addsubcategory','uses'=>'HomeController@_addsubcategory'));
        Route::get('view-subcategory/{id}',array('as'=>'view-subcategory','uses'=>'HomeController@view_subcategory'));
        Route::get('delete-subcategory/{id}',array('as'=>'delete_subcategory','uses'=>'HomeController@delete_subcategory'));
        Route::get('edit-subcategory/{id}',array('as'=>'edit-subcategory','uses'=>'HomeController@edit_subcategory'));
        Route::post('edit-subcategory',array('as'=>'editsubcategory','uses'=>'HomeController@_editsubcategory'));
        Route::post('status_subcategory',array('as'=>'status_subcategory','uses'=>'HomeController@status_subcategory'));

        // Product
        Route::get('add-product',array('as'=>'add-product','uses'=>'ProductController@add_product'));
        Route::post('get-category',array('as'=>'get_category','uses'=>'ProductController@get_category'));
        Route::post('get-subcategory',array('as'=>'get_subcategory','uses'=>'ProductController@get_subcategory'));
        Route::post('add-product',array('as'=>'addproduct','uses'=>'ProductController@addproduct'));
        Route::get('view-products',array('as'=>'view-product','uses'=>'ProductController@view_product'));
        Route::get('view-products-details/{id}',array('as'=>'view_product_details','uses'=>'ProductController@view_product_details'));
        Route::get('delete-product/{id}',array('as'=>'delete_product','uses'=>'ProductController@delete_product'));
        Route::post('status_product',array('as'=>'status_product','uses'=>'ProductController@status_product'));
        Route::post('product_delete',array('as'=>'product_delete_multiple','uses'=>'ProductController@product_delete_multiple'));
        Route::get('edit-product/{id}',array('as'=>'edit_product','uses'=>'ProductController@edit_product'));
        Route::post('edit-product/',array('as'=>'_editproduct','uses'=>'ProductController@_editproduct'));
        Route::get('clone-product/{id}',array('as'=>'clone_product','uses'=>'ProductController@clone_product'));

        // User
        Route::get('all-users',array('as'=>'all_users','uses'=>'UserController@all_users'));
        Route::get('all-new-users',array('as'=>'all_new_users','uses'=>'UserController@all_new_users'));
        Route::post('allusers',array('as'=>'allusers','uses'=>'UserController@allUsers'));
        Route::get('user-details/{id}',array('as'=>'user_details','uses'=>'UserController@user_details'));
        Route::get('delete-user/{id}',array('as'=>'delete_user','uses'=>'UserController@delete_user'));
        Route::post('user-delete',array('as'=>'user_delete_multiple','uses'=>'UserController@user_delete_multiple'));
        Route::post('status-user',array('as'=>'status_user','uses'=>'UserController@status_user'));
        Route::post('user-wallet-status',array('as'=>'user_wallet_status','uses'=>'UserController@user_wallet_status'));

        // Ads
        Route::get('all-ads',array('as'=>'all_ads','uses'=>'AdsController@all_ads'));
        Route::get('ads-details/{id}',array('as'=>'ads_details','uses'=>'AdsController@ads_details'));
        Route::post('block-status',array('as'=>'ad_block_status','uses'=>'AdsController@ad_block_status'));
        Route::post('activation-status',array('as'=>'ad_activation_status','uses'=>'AdsController@ad_activation_status'));
        Route::get('delete-ads/{id}',array('as'=>'delete_ads','uses'=>'AdsController@delete_ads'));
        Route::post('ads-delete',array('as'=>'ads_delete_multiple','uses'=>'AdsController@ads_delete_multiple'));

        // Coins
        Route::get('all-coins',array('as'=>'all_coins','uses'=>'AdsController@all_coins'));
        Route::POST('allcoins',array('as'=>'allcoins','uses'=>'AdsController@allcoins'));
        Route::get('coin',array('as'=>'coin','uses'=>'AdsController@coin'));

        //Post Reports
        Route::get('all-reports',array('as'=>'all_reports','uses'=>'AdsController@all_reports'));
        Route::get('delete-reports/{id}',array('as'=>'delete_reports','uses'=>'AdsController@delete_reports'));
        Route::post('reports-delete',array('as'=>'reports_delete_multiple','uses'=>'AdsController@reports_delete_multiple'));

        // Contact us 
        Route::get('queries',array('as'=>'all_queries','uses'=>'UserController@all_queries'));
        Route::get('delete-query/{id}',array('as'=>'delete_query','uses'=>'UserController@delete_query'));
        Route::post('query-delete',array('as'=>'query_delete_multiple','uses'=>'UserController@query_delete_multiple'));


        Route::get('view-banner',array('as'=>'view-banner','uses'=>'BannerController@allbanners'));
        Route::any('add-banner',array('as'=>'add-banner','uses'=>'BannerController@add_banner'));
        Route::get('edit-banner/{id}',array('as'=>'edit-banner','uses'=>'BannerController@edit_banner'));
        Route::post('edit-banner/',array('as'=>'_editbanner','uses'=>'BannerController@_editbanner'));
        Route::get('delete-banner/{id}',array('as'=>'deletebanner','uses'=>'BannerController@deletebanner'));
    });
});


