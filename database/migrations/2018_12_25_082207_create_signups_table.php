<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signups', function (Blueprint $table) {
            $table->increments('id');
           $table->string('name')->nullable();
			$table->string('email')->nullable();
     		 $table->string('address')->nullable();
     		 $table->string('phone_no')->nullable();
      		$table->string('profile_image')->nullable();
			$table->string('password')->nullable();
			$table->string('player_id')->nullable();
			$table->string('social_token')->nullable();
			$table->string('signup_type')->nullable();
			$table->string('app_type')->nullable();
			 $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signups');
    }
}
