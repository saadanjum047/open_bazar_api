<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
           $table->string('user_id')->nullable();
          $table->string('ad_title')->nullable();
          $table->string('ad_description')->nullable();
          $table->string('mobile')->nullable();
          $table->string('price')->nullable();
          $table->string('images')->nullable();
          $table->string('category')->nullable();
          $table->string('city')->nullable();
          $table->string('favorite_count')->nullable();
          $table->string('views_count')->nullable();
          $table->string('like_count')->nullable();
          $table->string('timestamp')->nullable();
          $table->string('activation_status')->nullable();
          $table->string('block_status')->nullable();
          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
