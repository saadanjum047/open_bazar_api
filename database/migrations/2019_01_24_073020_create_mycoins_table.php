<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMycoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mycoins', function (Blueprint $table) {
            $table->increments('id');
             $table->string('user_id')->nullable();
            $table->string('refer_id')->nullable();
            $table->string('coins')->nullable();
            $table->string('timestamp')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mycoins');
    }
}
