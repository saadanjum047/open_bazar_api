<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Banner extends Model
{    
    protected $table = 'banners';
    protected $primaryKey = 'bannerid';
    protected $fillable = ['banner_path', 'banner_isactive', 'banner_sortid'];

    public function storeRecord($banner_path, $banner_isactive, $banner_sortid)
    {
        return $this->create([
            'banner_path' => $banner_path,
            'banner_isactive' => $banner_isactive,
            'banner_sortid' => $banner_sortid
        ]);
    }

    public function updateRecord($id,$banner_path, $banner_isactive, $banner_sortid)
    {
        return $this->where('bannerid', $id)->update([
            'banner_path' => $banner_path,
            'banner_isactive' => $banner_isactive,
            'banner_sortid' => $banner_sortid
        ]);
    }
    public function getall() {
    
        return $banners = $this->where('banner_isactive',1)->get();
    }
}
