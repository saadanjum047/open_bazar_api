<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // public function handle($request, Closure $next)
    // {
    //     return $next($request);
    // }

    public function handle($request, Closure $next,...$params)
    {
        if (Auth::check())
        {
            $r=Auth::user()->role;
		 
            if(in_array($r, $params))
            {
                return $next($request);
            }
            else 
            {
                return redirect('login');
            }
        } 
        else 
        {
            return redirect('login');     
        }   		     
   }
}
