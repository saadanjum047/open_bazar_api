<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Auth;
use App\Category;
use App\SubCategory;
use App\Product;
use App\User;
use App\mycoins;
use App\ContactUs;

class UserController extends Controller
{
    // Start User Function

        // User List
            public function all_users()
            {
                $data = User::where('role','!=','admin')->where('delete_status',1)->orderBy('id','DESC')->get();
                foreach($data as $key)
                {
                    $coin = mycoins::where('user_id',$key->id)->get();
                    $sum = 0;
                    $credit = 0;
                    foreach($coin as $c){
                        if($c->wallet_type==0){
                            if($c->type == 'redeem'){
                                $sum = $c->coins - $sum;
                            }else{
                                $sum = $c->coins+ $sum;
                            }
                        }else{
                            $credit = $c->coins + $credit;
                        }
                        
                    }
                    $key->coin = $sum;
                    $key->credit = $credit;
                }                
                return View::make('user.all-users',compact('data'));
            }
        // End Function

        // User Details Function
            public function user_details($id)
            {
                $ids = decrypt($id);
                $data = User::where('id',$ids)->where('delete_status',1)->first();
                if($data)
                {
                    $coin = mycoins::where('user_id',$data->id)->get();
                    $sum = 0;
                    $credit = 0;
                    foreach($coin as $c){
                        if($c->wallet_type==0){
                            if($c->type == 'redeem'){
                                $sum = $c->coins - $sum;
                            }else{
                                $sum = $c->coins+ $sum;
                            }
                        }else{
                            $credit = $c->coins + $credit;
                        }
                        
                    }
                    $data->coin = $sum;
                    $data->credit = $credit;

                    return View::make('user.user-details',compact('data'));
                }else{
                    return back()->with('errror','Something went wrong!!..');
                }
            }
        // End Function

        // Delete User Function 
            public function delete_user($id)
            {
                $check = User::where('id',$id)->where('delete_status',1)->first();
                if($check)
                {
                    $check->delete_status = 0;
                    $save = $check->save();
                    if($save)
                    {
                        return redirect()->route('all_users')->with('status','User deleted successfully!!..');
                    }else{
                        return back()->with('errror','Something went wrong!!..');
                    }
                }else{
                    return back()->with('errror','Something went wrong!!..');
                }
            }
        // End Function

        // Multiple User Delete Function
            public function user_delete_multiple(Request $req)
            {
                $ids = $req->id;
                $Check = User::whereIn('id', $ids)->where('delete_status',1)->get();
                foreach ($Check as $p) {
                    if ($p->delete_status == 1) {
                        $p->delete_status = 0;
                        $p->save();
                    }
                }
        
                return Response($ids);
            }
        // End Function

        // User Block status function
            public function status_user(Request $req)
            {
                $data = User::where('id',$req->id)->where('delete_status',1)->first();
                if($data)
                {
                    if($data->block_status==0)
                    {
                        $data->block_status = 1;
                        $save = $data->save();
                    }
                    else{
                        $data->block_status = 0;
                        $save = $data->save();
                    }
                    
                    if($save)
                    {
                        return response()->json(['message'=>'success','status'=>'true']);
                    }
                    else{
                        return response()->json(['message'=>'warning','status'=>'false']);
                    }
                }
                else{
                    return response()->json(['message'=>'errror','status'=>'false']);
                }
            }
        // ENd function

        // Block user wallet function
            public function user_wallet_status(Request $req)
            {
                $data = User::where('id',$req->id)->where('delete_status',1)->first();
                if($data)
                {
                    if($data->wallet_status==0)
                    {
                        $data->wallet_status = 1;
                        $save = $data->save();
                    }
                    else{
                        $data->wallet_status = 0;
                        $save = $data->save();
                    }
                    
                    if($save)
                    {
                        return response()->json(['message'=>'success','status'=>'true']);
                    }
                    else{
                        return response()->json(['message'=>'warning','status'=>'false']);
                    }
                }
                else{
                    return response()->json(['message'=>'errror','status'=>'false']);
                }
            }
        // End Function

    // End User Function

    // Start Contact Us Functions
        // View All queries function
            public function all_queries()
            {
                $data = ContactUs::where('status',1)->orderBy('id','desc')->get();   
                return View::make('user.queries',compact('data'));
            }
        // End Function

        // Delete Query Function
            public function delete_query($id)
            {
                $check = ContactUs::where('id',$id)->where('status',1)->first();
                if($check)
                {
                    $check->status = 0;
                    $save = $check->save();
                    if($save)
                    {
                        return redirect()->route('all_queries')->with('status','Query deleted successfully!!..');
                    }else{
                        return back()->with('errror','Something went wrong!!..');
                    }
                }else{
                    return back()->with('errror','Something went wrong!!..');
                }
            }
        // End Function

        // Delete Multiple Queries Function
            public function query_delete_multiple(Request $req)
            {
                $ids = $req->id;
                $Check = ContactUs::whereIn('id', $ids)->where('status',1)->get();
                foreach ($Check as $p) {
                    if ($p->status == 1) {
                        $p->status = 0;
                        $p->save();
                    }
                }
        
                return Response($ids);
            }
        // End Function
    // End FUnctions

    public function all_new_users()
    {
        return View::make('user.new_user');
        
    }

    public function allUsers(Request $request)
    {
        
        $columns = array( 
                            0 =>'id', 
                            1 =>'name',
                            2=> 'email',
                            3=> 'phone_no',
                            5=> 'app_type',
                            8=> 'signup_type',
                        );
  
        $totalData = User::count();
            
        $totalFiltered = $totalData; 
        $or=$request->input('order.0.column');
        if($or!=''){
            $order = $columns[$request->input('order.0.column')];
        }
        else{
            $order="id";
        }
        $limit = $request->input('length');
        $start = $request->input('start');
       
        $dir = $request->input('order.0.dir');
        
        if(empty($request->input('search.value')))
        {            
            $data = User::where('role','!=','admin')->where('delete_status',1)->offset($start)->limit($limit)->orderBy($order,$dir)->get();
                        
        }
        else {
            $search = $request->input('search.value'); 

            $data =  User::where('id','LIKE',"%{$search}%")
                            ->orWhere('name', 'LIKE',"%{$search}%")
                            ->orWhere('email', 'LIKE',"%{$search}%")
                            ->orWhere('phone_no', 'LIKE',"%{$search}%")
                            ->orWhere('app_type', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            
                            ->get();

            $totalFiltered =User::where('id','LIKE',"%{$search}%")
                            ->orWhere('name', 'LIKE',"%{$search}%")
                            ->orWhere('email', 'LIKE',"%{$search}%")
                            ->orWhere('phone_no', 'LIKE',"%{$search}%")
                            ->orWhere('app_type', 'LIKE',"%{$search}%")
                             ->count();
        }

        foreach($data as $key)
        {
            $coin = mycoins::where('user_id',$key->id)->get();
            $sum = 0;
            $credit = 0;
            foreach($coin as $c){
                if($c->wallet_type==0){
                    if($c->type == 'redeem'){
                        $sum = $c->coins - $sum;
                    }else{
                        $sum = $c->coins+ $sum;
                    }
                }else{
                    $credit = $c->coins + $credit;
                }
                
            }
            $key->coin = $sum;
            $key->credit = $credit;
        }           




        $data_user = array();
        if(!empty($data))
        {
            foreach ($data as $post)
            {

                $checkbox='<span class="icheck-inline">
                <div class="icheckbox_md hover"><input type="checkbox" class="ts_checkbox_all" name="checkbox_demo_inline_mercury" id="checkbox_demo_inline_1" data-md-icheck="" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
            </span>';

                if($post->phone_no==''){
                    $phone="N/A";
                }
                else{
                    $phone=$post->phone_no;
                }

                if($post->email==''){
                    $email="N/A";
                }
                else{
                    $email=$post->email;
                }
                if($post->profile_image!=""){
                    if (filter_var($post->profile_image, FILTER_VALIDATE_URL)) {
                    $image="<img src='$post->profile_image'  height='30' width='50'/>";
                    }
                    else{ 
                        $url = url('/uploads/images/'.$post->profile_image);
                        $image="<img src='{$url}'  height='30' width='50'/>";
                    }
                }
                else{
                    $image="N/A";
                }

                if($post->app_type=="Android") {
                $apptype="<span class='uk-badge uk-badge-danger md-bg-blue-grey-600'> Android </span>"; 
                }
                else{
                $apptype="<span class='uk-badge uk-badge-primary md-bg-brown-600'> iOS </span>";
                }

                if($key->signup_type!="")  {

                    if($post->signup_type=="google"){
                       $signuptype="<span class='uk-badge uk-badge-danger'>". strtoupper($post->signup_type) ."</span>";
                    }    
                    elseif($post->signup_type=="facebook"){
                        $signuptype="<span class='uk-badge uk-badge-primary'>".strtoupper($post->signup_type)."</span>";
                    }
                    else{
                        $signuptype="<span class='uk-badge uk-badge-success'>".strtoupper($post->signup_type)."</span>";
                    }
                  
                }
                else{
                 $signuptype="NA";
                }

                $postid=$post->id;
                $post_status=$post->block_status;
                $route_url=route("user_details",["id"=>encrypt($post->id)]);

                if($post->block_status==1){
                    $status='<input type="checkbox" data-switchery data-switchery-color="#d32f2f"  checked    id="switch_demo_danger" style="display:none;"/>
                    <span onclick="del('.$postid.','.$post_status.')" class="switchery switchery-default" style="background-color: rgba(211, 47, 47, 0.5); border-color: rgba(211, 47, 47, 0.5); box-shadow: rgba(211, 47, 47, 0.5) 0px 0px 0px 7px inset; transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s, background-color 1.2s ease 0s;"><small style="left: 18px; background-color: rgb(211, 47, 47); transition: background-color 0.4s ease 0s, left 0.2s ease 0s;"></small></span>';
                }
                else{
                    $status='<input type="checkbox" data-switchery data-switchery-color="#d32f2f"   id="switch_demo_danger" style="display:none;"/>
                    <span onclick="del('.$postid.','.$post_status.')" class="switchery switchery-default" style="box-shadow: rgba(0, 0, 0, 0.26) 0px 0px 0px 0px inset; border-color: rgba(0, 0, 0, 0.26); background-color: rgba(0, 0, 0, 0.26); transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s;"><small style="left: 0px; background-color: rgb(250, 250, 250); transition: background-color 0.4s ease 0s, left 0.2s ease 0s;"></small></span>';
                }
               
                $action='<a href="'.$route_url.'" title="View" ><i class="uk-icon-eye uk-icon-small"></i></a>';
                $nestedData['checkbox'] = $checkbox;
                $nestedData['id'] = $post->id;
                $nestedData['name'] = $post->name;
                $nestedData['email'] = $email;
                $nestedData['mobile'] = $phone;
                $nestedData['image'] = $image;
                $nestedData['apptype'] = $apptype;
                $nestedData['totalcoin'] = $post->coin;
                $nestedData['totalcredit'] = $post->credit;
                $nestedData['signuptype'] = $signuptype;
                $nestedData['status'] = $status;
                $nestedData['action'] = $action;
               
                $data_user[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data_user   
                    );
            
        echo json_encode($json_data); 
        
    }
}
