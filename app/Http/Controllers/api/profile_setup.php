<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\signups;
use App\notification_id;
use App\User;
use App\Ads;
use App\FollowUser;
use App\mycoins;
use Illuminate\Support\Facades\Auth; 
use Mail;
use Hash;
use DB;
use App\adboost;

class profile_setup extends Controller
{
    


//--------------socail signup------------------facebook,google---
  public function sociallogin(Request $r)
 {
    if($r->signup_type == "facebook")
    {
      if($r->email == '')
      {
        $check_email=User::where('social_token',$r->social_token)->where('delete_status',1)->first();    //check count   facebook toke or email if exist in Db;
      }
      else
      {
        $check_email=User::where('social_token',$r->social_token)->where('delete_status',1)->orwhere('email',$r->email)->first();    //check count   facebook toke or email if exist in Db;
      }

      if($this->check_count($check_email)>0)  //if already exist just update type and login
      {
        if($check_email->block_status==1)
        {
          if ($r->email == '') 
          {
            $update=User::where('email',$r->email)->update(['signup_type'=>'facebook','app_type'=>$r->app_type]);
          
            // add push id
            $this->addpushid($check_email->id,$r->player_id,$r->device_id);
          
            // token
            $user = Auth::loginUsingId($check_email->id);
            $user = Auth::User();
            $passport= $user->createToken('personal')->accessToken;
            $data=$check_email;
            $data->token=$passport;
            
            // social details
            $social_data=signups::where('user_id',$check_email->id)->update(['verify_facebook'=>'true','facebook_details'=>$r->social_details]);

            $social_account=signups::where('user_id',$check_email->id)->select('verify_google','verify_facebook')->first();
          
            $data->google_verification=$social_account->verify_google;
            
            $data->facebook_verification=$social_account->verify_facebook;
            
              return response()->json(['data'=>$data,'message'=>'Successfully added','status'=>'success','login'=>false]);

          }
          else
          {
            $update=User::where('email',$r->email)->where('delete_status',1)->update(['signup_type'=>'facebook','app_type'=>$r->app_type]);
          
            $this->addpushid($check_email->id,$r->player_id,$r->device_id);
            $user = Auth::loginUsingId($check_email->id);
            $user = Auth::User();
            $passport= $user->createToken('personal')->accessToken;
            $data=$check_email;
            $data->token=$passport;
            
            $social_data=signups::where('user_id',$check_email->id)->update(['verify_facebook'=>'true','facebook_details'=>$r->social_details]);

            $social_account=signups::where('user_id',$check_email->id)->select('verify_google','verify_facebook')->first();
            
            $data->google_verification=$social_account->verify_google;
            
            $data->facebook_verification=$social_account->verify_facebook;

            return response()->json(['data'=>$data,'message'=>'Successfully added','status'=>'success','login'=>false]);

          }
        }else{
          return response()->json(['message'=>'You are blocked by admin. Please contact admin.','status'=>'fail','login'=>'false']); 
        }
      }
      else
      {

          //  insert details of user
        $insert=new User;
        $insert->name=$r->name;
        $insert->email=$r->email;
        $insert->profile_image=$r->profile_image;
        $insert->phone_no=$r->phone_no;     
        $insert->app_type=$r->app_type;
        $insert->social_token=$r->social_token;
        $insert->signup_type="facebook";
        $insert->save();
      
        $this->addpushid($insert->id,$r->player_id,$r->device_id);
        
        $social_data=new signups;
        $social_data->user_id=$insert->id;
        $social_data->verify_facebook='true';
        $social_data->facebook_details=$r->social_details;
        $social_data->reffer_id=$r->reffer_id;
        $social_data->save();

        $this->add_coin('You are rewarded for inviting your friend','5',$insert->id,$r->reffer_id,'reffer','0');
    

        $user = Auth::loginUsingId($insert->id);
        $user = Auth::User();
        $passport= $user->createToken('personal')->accessToken;
    
        $insert->token=$passport;
            

        $insert->google_verification='false';
        
        $insert->facebook_verification='true';

    
        return response()->json(['data'=>$insert,'message'=>'Successfully added','status'=>'success','login'=>true]);
      }
    }
    else
    {
      if($r->email == '')
      {
        return response()->json(['message'=>'Missing email','status'=>'fail']);
      }
      else {
        $check_email=User::where('email',$r->email)->where('delete_status',1)->first();    //check count   facebook toke or email if exist in Db;
      }


      if($this->check_count($check_email) > 0)  //if already exist just update type and login
      {
        if($check_email->block_status==1){
          $update=User::where('email',$r->email)->where('delete_status',1)->update(['signup_type'=>'google','app_type'=>$r->app_type]);
          // $notification_count=notification::where('user_id',$check_email->id)->where('view_status','false')->count();
          $this->addpushid($check_email->id,$r->player_id,$r->device_id);
       
          $data=User::where('email',$r->email)->first();
          
          $user = Auth::loginUsingId($data->id);
          $user = Auth::User();
          $passport= $user->createToken('personal')->accessToken;
          $data=$check_email;
          $data->token=$passport;
  
          $social_data=signups::where('user_id',$data->id)->update(['verify_google'=>'true','google_details'=>$r->social_details]);
  
          $social_account=signups::where('user_id',$data->id)->select('verify_google','verify_facebook')->first();
          
          if($this->check_count($social_account) > 0) 
          {
            $data->google_verification=$social_account->verify_google;
          
            $data->facebook_verification=$social_account->verify_facebook;
          }
          else
          {
            $data->google_verification='true';
          
            $data->facebook_verification='false';
  
            $social_data=new signups;
            $social_data->user_id=$data->id;
            $social_data->verify_google='true';
            $social_data->google_details=$data->social_details;
            $social_data->save();
          }
          return response()->json(['data'=>$data,'message'=>'Successfully added','status'=>'success','login'=>false]);
        }
        else
        {
          return response()->json(['message'=>'You are blocked by admin. Please contact admin.','status'=>'fail','login'=>'false']);
        }
      }
      else
      {
        $insert=new User;
        $insert->name=$r->name;
        $insert->email=$r->email;
        $insert->profile_image=$r->profile_image;
        $insert->app_type=$r->app_type;
        $insert->phone_no=$r->phone_no;
        $insert->signup_type="google";
        $insert->save();
      
        $this->addpushid($insert->id,$r->player_id,$r->device_id);
      
        $social_data=new signups;
        $social_data->user_id=$insert->id;
        $social_data->verify_google='true';
        $social_data->google_details=$r->social_details;
        $social_data->reffer_id=$r->reffer_id;
        $social_data->save();
        $this->add_coin('You are rewarded for inviting your friend','5',$insert->id,$r->reffer_id,'reffer','0');
    
        $user = Auth::loginUsingId($insert->id);
          $user = Auth::User();
          $passport= $user->createToken('personal')->accessToken;
    
          $insert->token=$passport;
        
        $insert->google_verification='true';
          
          $insert->facebook_verification='false';
            return response()->json(['data'=>$insert,'message'=>'Successfully added','status'=>'success','login'=>true]);
      }
    }
 }

//-------sign up-------------------------
 public function signup(Request $r)
  {
      $check_email=User::where('email',$r->email)->where('delete_status',1)->count();    //check count;
     
    if($check_email > 0)
    {
       return response()->json(['message'=>'Email already Exists','status'=>'fail']);
    }
    else
    {

      $insert=new User;
      $insert->name=$r->name;
      $insert->email=$r->email;
      $insert->phone_no=$r->phone_no;
      $insert->password=bcrypt($r->password);
      $insert->app_type=$r->app_type;
      $insert->signup_type=$r->signup_type;
      $insert->save();
    
    $social_data=new signups;
    $social_data->reffer_id=$r->reffer_id;
    $social_data->user_id=$insert->id;
    $social_data->save();

      $data=User::where('email',$r->email)->where('delete_status',1)->first();
      if(!empty($r->reffer_id)){
        $this->add_coin('You are rewarded for inviting your friend','5',$insert->id,$r->reffer_id,'reffer','0');
    
      }
    
    $this->addpushid($data->id,$r->player_id,$r->device_id);
    $user = Auth::loginUsingId($data->id);
        $user = Auth::User();
        $passport= $user->createToken('personal')->accessToken;
      
        return response()->json(['data'=>$data,'message'=>'Successfully added','status'=>'success','token'=>$passport]);
      }
  }


//sendotp function
  public function emailotp(Request $req){
    $check=User::where('email',$req->email)->where('delete_status',1)->count();

     $type=$req->type;
      if ($type=='signup') {
       if ($check>0) {
         return response()->json(['message'=>'User already exist','status'=>'fail']);
     
        }
        else {

      $OTP=rand(10000,99999);
      $data = array('otp'=>$OTP);
      $email=$req->email;
      Mail::send('send_forgot', $data, function($message) use ($email)
      {
      $message->from('openbazarnl@gmail.com', "OpenBazar");
      $message->subject("OpenBazar");
      $message->to($email,'User');
      } );
      return response()->json(['otp'=>$OTP,'message'=>'Send Otp in mail Box','status'=>'success']);

    }
      }
      else  
       if ($check>0) {
      $OTP=rand(10000,99999);
      $data = array('otp'=>$OTP);
      $email=$req->email;
      Mail::send('send_forgot', $data, function($message) use ($email)
      {
      $message->from('openbazarnl@gmail.com', "OpenBazar");
      $message->subject("OpenBazar");
      $message->to($email,'User');
      } );
      return response()->json(['otp'=>$OTP,'message'=>'Send Otp in mail Box','status'=>'success']);

    }else {

      return response()->json(['message'=>'User not found','status'=>'fail']);
    }
  
  }



//----------------------login ---------------------

  public function login(Request $r)
 {
    $email_check=User::where('email',$r->email)->where('delete_status',1)->first();
    if($email_check!==null)
    {
      if($email_check->block_status == 1)
      {
        $pass=$email_check->password;
        $password=$r->password;
        if (Hash::check($password,$pass))
        {
          //---update player id---
      
          $update = $email_check;
          $update->app_type = $r->app_type;
          $save=$update->save();
        
          //-----end player id---

          $data=$update;
          $this->addpushid($data->id,$r->player_id,$r->device_id);
          $user = Auth::loginUsingId($data->id);
          $user = Auth::User();
          $passport= $user->createToken('personal')->accessToken;
          $data->token=$passport;

          $social_account=signups::where('user_id',$data->id)->select('verify_google','verify_facebook')->first();
          if($this->check_count($social_account)>0) {
            $data->google_verification=$social_account->verify_google;
            
            $data->facebook_verification=$social_account->verify_facebook;
          }
          return response()->json(['data'=>$data,'message'=>'Successfully logged','status'=>'success']);
        }
        else
        {
          return response()->json(['message'=>'invalid password','status'=>'fail']);
        }
      }
      else
      {
        return response()->json(['message'=>'You are blocked by admin. Please contact admin.','status'=>'fail','login'=>'false']);
      }
    }
    else
    {
      return response()->json(['message'=>'invalid email','status'=>'fail']);
    }
 }

// Add push  id
  public function addpushid($id,$player_id,$device_id){

    $push_id=notification_id::where('device_id',$device_id)->first();

      if ($push_id==null) {
      $insert=new notification_id;
          $insert->user_id=$id;
          $insert->push_id=$player_id;
           $insert->device_id=$device_id;
          $insert->save();
        }
        else{
       $update = $push_id;
          $update->user_id = $id;
          $update->push_id = $player_id;
          $save=$update->save();
        }

  }



//----------------reset password-------------------
  public function reset_password(Request $r)
  {
    $user=User::where('email',$r->email)->where('delete_status',1)->first();
    if($user!==null)
    {
      $update=$user;
        $update->password=bcrypt($r->password);
      $save=$update->save();

      if($save)
      {
        return response()->json(['data'=>$update,'message'=>'Successfully','status'=>'success']);
      }
      else {
         return response()->json(['message'=>'Password Updation Failed','status'=>'fail']);
      }

    }
    else {
       return response()->json(['message'=>'Invalid Email Address','status'=>'fail']);
    }

  }

 


// update profile of user

  public function update_profile(Request $r){
      $files = $r->file('profile_image');
     if($files!='')
       {
           $imageName = date("YmdHis").'.'.$r->profile_image->getClientOriginalExtension();
           $success = $r->profile_image->move(public_path('images'), $imageName);
           $imageurl=$imageName;

       }

    if($files!='')
    {
     $id=$r->id;
     $update = User::find($id);
     $update->name = $r->name;
      $update->profile_image = $imageurl;
     $save=$update->save();

     $social_update = signups::where('user_id',$id)->first();
      $social_update->verify_google = $r->verify_google;
      $social_update->verify_facebook = $r->verify_facebook;
       $save=$social_update->save();
  
    }
    else {
      $id=$r->id;
      $update = User::find($id);
      $update->name = $r->name;
      $save=$update->save();

        $social_update = signups::where('user_id',$id)->first();
      $social_update->verify_google = $r->verify_google;
      $social_update->verify_facebook = $r->verify_facebook;
       $save=$social_update->save();
       }

    if($save)
    {
        $user_detail=User::where('id',$r->id)->where('delete_status',1)->first();
          $social_account=signups::where('user_id',$r->id)->select('verify_google','verify_facebook')->first();
    
    $user_detail->google_verification=$social_account->verify_google;
    
    $user_detail->facebook_verification=$social_account->verify_facebook;
    
       return response()->json(['data'=>$user_detail,'message'=>'Successfully Updated','status'=>'success']);
    }
    else {
      return response()->json(['message'=>'Updation failed','status'=>'fail']);
    }

  }


// user_details
    public function user_detail(Request $r){
    
      $user_detail=User::where('id',$r->id)->where('delete_status',1)->first();
      
      $social_account=signups::where('user_id',$r->id)->select('verify_google','verify_facebook')->first();
      
      $user_detail->google_verification=$social_account->verify_google;
      
      $user_detail->facebook_verification=$social_account->verify_facebook;
      
         return response()->json(['data'=>$user_detail,'message'=>'Successfully Updated','status'=>'success']);
  
    }

// user_details
    public function user_details(Request $r){
    

     $check=User::where('id',$r->id)->where('delete_status',1)->count();

     if ($check>0) {
              
             

             $requests = DB::table('users')
              ->leftJoin('signups', 'signups.user_id', '=', 'users.id')
              ->select('users.id','users.name','users.phone_no','users.profile_image','users.verify_phone','users.created_at',
                      'signups.verify_google','signups.verify_facebook')
              ->where('users.id',$r->id)->where('delete_status',1)->first();

              // follower and following count
              $ads_count = Ads::where('user_id',$r->id)->where('mobile_verify','true')->where('activation_status',0)->where('block_status',0)
                  ->where('delete_status',1)->count();
              $requests->ads_count = $ads_count;

                $followers=0;
                $following=0;
                $follow_user='false';
                $check=FollowUser::where('user_id',$r->id)->orwhere('following_id',$r->id)->get();
               if ($this->check_count($check)>0)     
                {

                    $followers=0;
                    $following=0;
                     $follow_user='false';
                    foreach($check as $d)      
                    {                       
                        if($d->user_id == $r->id)
                        {                           
                            
                            $following++;
                        }
                        else 
                        {                            
                            $followers++; 
                        }

                        if(($r->id == $d->following_id) && ($r->user_id == $d->user_id) )
                        {
                            $follow_user='true';
                        }
                      }
                        
                }

              $requests->followers=$followers;
               $requests->following=$following;
               $requests->follow_user=$follow_user;
               
               $ads = Ads::where('user_id',$r->id)->where('block_status','0')->where('activation_status','0')->where('delete_status',1)->where('mobile_verify','true')->count();
                $requests->ads_count = $ads;

                $data=$this->get_ads($requests,$r->index,$r->user_id);

        return response()->json(['user_detail'=>$requests,'data'=>$data,'message'=>'Successfully Updated','status'=>'success']);
  
     }else{

        return response()->json(['message'=>'No user exist','status'=>'fail']);
  
     }
       
   
    }
  
    



// Update user city

  public function update_city(Request $r){
    
    $user_detail=User::where('id',$r->id)->where('delete_status',1)->first();
    
    
    $user_detail->address=$r->city;
    $user_detail->save();
    
         return response()->json(['data'=>$user_detail,'message'=>'Successfully Updated','status'=>'success']);
  
    }  



      public function get_ads($r,$index,$user_id){
    $limit = 10;
    if (isset($index)) {
      $offset = $index * $limit;

    } else {
      $offset = 0;
    }

    $date = date('Y-m-d');
    

    $data=Ads::where('user_id',$r->id)->where('block_status','0')->where('activation_status','0')->where('delete_status',1)->where('mobile_verify','true')
                    ->offset($offset)->limit($limit)->orderBy('id', 'desc')->get();
    foreach($data as $d)
    {
      $boost = adboost::where('post_id',$d->id)->where('status',0)->whereDate('expiry_date','>',$date)->first();
      if($boost)
      {
        $d->days = $boost->days;
        $d->boost_type = $boost->boost_type;
      }
      else {
        $d->days = "";
        $d->boost_type = "";
      }

    //      // follow user -------------
    //       $follow_count = FollowUser::where('user_id',$user_id)->where('following_id',$d->user_id)->count();

    //         $follow_user='false';
    //           if ($follow_count>0) {
    //             $follow_user='true';
    //           }
    //         $d->follow_user = $follow_user;

    // // // follow userr end----------

     
      if($r)
      {
        $d->user_name = $r->name;
        $d->user_image = $r->profile_image;
      }
      $d->favorite_count=0;
      $d->views_count=0;
      $d->like_count=0;
      $d->fav_stattus='No';
      $d->like_stattus='No';

      $like_datas=DB::table('adcounts')->where('post_id',$d->id)->get();
        $favorite_count=0;
        $views_count=0;
        $like_count=0;
        $fav_stattus='No';
        $like_stattus='No';
      foreach ($like_datas as $like_data) 
      {
        if ($like_data->favorite==1) 
        {
          $favorite_count++;
        }
        if ($like_data->views==1) 
        {
          $views_count++;
        }
        if ($like_data->likes==1) 
        {
          $like_count++;
        }

        if ($like_data->user_id==$user_id) 
        {
          if ($like_data->favorite==1) 
          {           
            $fav_stattus='Yes';
          }
          if ($like_data->likes==1) 
          {           
            $like_stattus='Yes';
          }
          
        }

        $d->favorite_count=$favorite_count;
        $d->views_count=$views_count;
        $d->like_count=$like_count;
        $d->fav_stattus=$fav_stattus;
        $d->like_stattus=$like_stattus;

    
      }

    }

    
    if ($this->check_count($data)>0) 
    {
          return $data;
      }  
      else
      {
        return $data;
      }   
  }


// Follow
  public function follow(Request $r){
    
    if (!empty($r->user_id) && !empty($r->following_id)) {
      
      $check =FollowUser::where('user_id',$r->user_id)->where('following_id',$r->following_id)->count();
        if ($check>0) {
           $check =FollowUser::where('user_id',$r->user_id)->where('following_id',$r->following_id)->delete();
     
        return response()->json(['data'=>$check ,'message'=>'Unfollow Successfully','status'=>'success']);
            }
        else{
    
      $follow_data=new FollowUser;
      $follow_data->user_id=$r->user_id;
      $follow_data->following_id=$r->following_id;
      $follow_data->save();

       return response()->json(['data'=>$follow_data ,'message'=>'Follow Successfully','status'=>'success']);
    
        }
      
      }
    else{
   return response()->json(['message'=>'Param Missing','status'=>'fail']);
    }
  }

// Verify Social Account
  public function verify_social_account(Request $r){

      if (!empty($r->user_id) &&  !empty($r->type) &&  !empty($r->verify)) {
       
      $check=signups::where('user_id',$r->user_id)->first();

        if ($this->check_count($check)>0) {
          if ($r->type=='google') {
            $check->verify_google=$r->verify;
            if ($r->verify=='true') {
               $check->google_details=$r->details;
              }
             $check->update();
          }else if ($r->type=='fb') {
             $check->verify_facebook=$r->verify;
               if ($r->verify=='true') {
             $check->facebook_details=$r->details;
           }
             $check->update();
          }

             return response()->json(['data'=>$check ,'message'=>'Successfully Updated','status'=>'success']);
    
        }else {
          if ($r->type=='google') {
              $insert =new signups();
                $insert->user_id=$r->user_id;
              $insert->verify_google=$r->verify;
              $insert->google_details=$r->details;
             $insert->save();
          }else if ($r->type=='fb') {
            $insert =new signups();
              $insert->user_id=$r->user_id;
             $insert->verify_facebook=$r->verify;
             $insert->facebook_details=$r->details;
             $insert->save();
          }
          $data=signups::find($insert->id);
      return response()->json(['data'=>$data ,'message'=>'Successfully Added','status'=>'success']);
    
        } 
      }
      else{
       return response()->json(['message'=>'Param Missing','status'=>'fail']);
      }
  }

// Update mobile Number
  public function update_mobilenumber(Request $r){

      if (!empty($r->user_id) && !empty($r->phone_no)) {
      
      $check=User::find($r->user_id);
      $check->phone_no=$r->phone_no;
      $check->verify_phone='true';
      $check->update();
      return response()->json(['data'=>$check ,'message'=>'Successfully Updated','status'=>'success']);
      }
      else{
       return response()->json(['message'=>'Param Missing','status'=>'fail']);
      }
  }

// Recent View
  public function recent_view(Request $r){


     $limit = 20;
    // if (isset($r->index)) {
      $offset = $r->index ;

    // } else {
    //   $offset = 0;
    // }


     $data=DB::table('adcounts')->where('user_id',$r->user_id)->where('views','1')->select('post_id')
                              ->orderBy('updated_at', 'desc')->get();
      
      foreach ($data as  $v) {
        $id[] =$v->post_id;
      }
      
        $postdata=$this->getadposts($r->user_id,$id);
        $postdata= collect($postdata);
        $postdata=$postdata->forPage($offset,$limit)->values();
        // dd($postdata);
        return response()->json(['data'=>$postdata ,'message'=>'Successfully Updated','status'=>'success']);
      
  }

// My favorite
    public function my_favorite(Request $r){

     $limit = 20;
      if (isset($r->index)) {
        $offset = $r->index * $limit;

      } else {
        $offset = 0;
      }
      
     $data=DB::table('adcounts')->where('user_id',$r->user_id)->where('favorite','1')->select('post_id')
                              ->offset($offset)->limit($limit)->orderBy('updated_at', 'desc')->get();
      
     
     if (sizeof($data)>0) {
     
      foreach ($data as  $v) {
        $id[] =$v->post_id;
      }
        $postdata=$this->getadposts($r->user_id,$id);
        return response()->json(['data'=>$postdata ,'message'=>'Successfully Updated','status'=>'success']);
       
     }else{
             return response()->json(['message'=>'No data','status'=>'false']);
  
     }
    }

// My offer ads
  public function my_offerads(Request $r){

      $limit = 20;
      if (isset($index)) {
        $offset = $index * $limit;

      } else {
        $offset = 0;
      }


     $data=DB::table('bids')->where('user_id',$r->user_id)->where('bid_type','0')->select('post_id')
     ->offset($offset)->limit($limit)->orderBy('id', 'desc')->get()->groupBy('post_id');
      
      foreach ($data as $key => $v) {
        $id[] =$key;
      }
        $postdata=$this->getadposts($r->user_id,$id);
        return response()->json(['data'=>$postdata ,'message'=>'Successfully Updated','status'=>'success']);
      
  }

// My following ads
  public function my_followingsads(Request $r){

      $limit = 10;
      if (isset($r->index)) {
        $offset = $r->index * $limit;

      } else {
        $offset = 0;
      }

      // $follow = FollowUser::where('following_id',$r->user_id)->pluck('user_id')->toArray();
      // $data = Ads::whereIn('user_id',$follow)->where('mobile_verify','true')->where('activation_status',0)->where('block_status',0)
      // ->where('delete_status',1)->get();
      // $data->following_adcount = $ad_count;


      $data=DB::table('follow_users')->join('ads','ads.user_id','=','follow_users.user_id')
            ->select('follow_users.*','ads.*')
            ->where('follow_users.following_id',$r->user_id)
            ->where('block_status','0')->where('activation_status','0')->where('delete_status','1')->where('mobile_verify','true')
            ->offset($offset)->limit($limit)->orderBy('ads.id', 'desc')->get();
    
      foreach($data as $d)
      {
      $user=  DB::table('users')
                ->leftJoin('signups', 'signups.user_id', '=', 'users.id')
                ->select('users.id','users.name','users.profile_image','users.created_at',
                        'signups.ads_count')
                ->where('users.id',$d->user_id)->where('delete_status',1)->first();


    // follow user -------------
        $follow_count = FollowUser::where('user_id',$r->user_id)->where('following_id',$d->user_id)->count();

          $follow_user='false';
            if ($follow_count>0) {
              $follow_user='true';
            }
          $d->follow_user = $follow_user;

    // // follow userr end----------


        if($user)
        {
          $d->user_name = $user->name;
          $d->user_image = $user->profile_image;
      
          $d->user_regiterdate = $user->created_at;
          $d->ads_count = $user->ads_count;

        }
        $d->favorite_count=0;
        $d->views_count=0;
        $d->like_count=0;
        $d->fav_stattus='No';
        $d->like_stattus='No';

        $like_datas=DB::table('adcounts')->where('post_id',$d->id)->get();
          $favorite_count=0;
          $views_count=0;
          $like_count=0;
          $fav_stattus='No';
          $like_stattus='No';
          
        foreach ($like_datas as $like_data) 
        {
          if ($like_data->favorite==1) 
          {
            $favorite_count++;
          }
          if ($like_data->views==1) 
          {
            $views_count++;
          }
          if ($like_data->likes==1) 
          {
            $like_count++;
          }

          if ($like_data->user_id==$r->user_id) 
          {
            if ($like_data->favorite==1) 
            {           
              $fav_stattus='Yes';
            }
            if ($like_data->likes==1) 
            {           
              $like_stattus='Yes';
            }
            
          }

          $d->favorite_count=$favorite_count;
          $d->views_count=$views_count;
          $d->like_count=$like_count;
          $d->fav_stattus=$fav_stattus;
          $d->like_stattus=$like_stattus;

        }
        
      }

      
    return response()->json(['data'=>$data ,'message'=>'Successfully Updated','status'=>'success']);
  
        
  }

}

