<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use App\Category;
use App\Ads;
use App\bids;
use App\signups;
use App\adcounts;
use App\mycoins;
use DB;
use Mail;
use App\User;
use App\SubCategory;
use App\Product;
use App\PostReport;
use App\FollowUser;
use App\Notifications;
use App\ContactUs;
use App\Block;
use App\notification_id;
use App\adboost;

class ApiController extends Controller
{
    public function get_all_post(Request $r)
    {
        $date = date('Y-m-d');

        $limit = 10;
        $offset = $r->index ;
        $check = User::where('id',$r->user_id)->where('delete_status',1)->first();
        if($check)
        {
            if($check->block_status==1)
            {
                $blocked_to = Block::where('user_id',$r->user_id)->pluck('block_id');
                $blocked_by = Block::where('block_id',$r->block_id)->pluck('user_id');

                if (!empty($r->location) && !empty($r->cat_id)) {
                    $user=	DB::table('ads')->leftJoin('adboosts', 'ads.id', '=', 'adboosts.post_id')
                        ->select('ads.*','adboosts.boost_type','adboosts.days')
                        ->where('ads.block_status',0)->where('ads.mobile_verify','true')->where('ads.country_id',$r->country_id)
                        ->where('ads.activation_status',0)->where('ads.delete_status',1)
                        ->whereNotIn('ads.user_id',$blocked_by)->whereNotIn('ads.user_id',$blocked_to)
                        ->whereDate('adboosts.expiry_date','>',$date)->where('adboosts.status',0)
                        ->where('ads.main_category',$r->type)->where('ads.category_id',$r->cat_id)->where('ads.city',$r->location)
                        ->orderBy('adboosts.boost_type','asc')
                        ->orderBy('adboosts.created_at','asc')->get(); 
                                //->where('adboosts.user_id',$r->user_id)    
                }elseif (!empty($r->cat_id)) {
                    $user=	DB::table('ads')->leftJoin('adboosts', 'ads.id', '=', 'adboosts.post_id')
                        ->select('ads.*','adboosts.boost_type','adboosts.days')
                        ->where('ads.block_status',0)->where('ads.mobile_verify','true')
                        ->where('ads.activation_status',0)->where('ads.delete_status',1)->where('ads.country_id',$r->country_id)
                        ->whereNotIn('ads.user_id',$blocked_by)->whereNotIn('ads.user_id',$blocked_to)
                        ->whereDate('adboosts.expiry_date','>',$date)->where('adboosts.status',0)
                        ->where('ads.main_category',$r->type)->where('ads.category_id',$r->cat_id)
                        ->orderBy('adboosts.boost_type','asc')
                        ->orderBy('adboosts.created_at','asc')->get();
                }elseif (!empty($r->location)) {
                    $user=	DB::table('ads')->leftJoin('adboosts', 'ads.id', '=', 'adboosts.post_id')
                        ->select('ads.*','adboosts.boost_type','adboosts.days')
                        ->where('ads.block_status',0)->where('ads.mobile_verify','true')
                        ->where('ads.activation_status',0)->where('ads.delete_status',1)->where('ads.country_id',$r->country_id)
                        ->whereNotIn('ads.user_id',$blocked_by)->whereNotIn('ads.user_id',$blocked_to)
                        ->whereDate('adboosts.expiry_date','>',$date)->where('adboosts.status',0)
                        ->where('ads.main_category',$r->type)->where('ads.city',$r->location)
                        ->orderBy('adboosts.boost_type','asc')
                        ->orderBy('adboosts.created_at','asc')->get();
                }else if($r->type =="all"){
                    $user=	DB::table('ads')->leftJoin('adboosts', 'ads.id', '=', 'adboosts.post_id')
                        ->select('ads.*','adboosts.boost_type','adboosts.days')
                        ->where('ads.block_status',0)->where('ads.mobile_verify','true')->where('ads.country_id',$r->country_id)
                        ->where('ads.activation_status',0)->where('ads.delete_status',1)
                        ->whereNotIn('ads.user_id',$blocked_by)->whereNotIn('ads.user_id',$blocked_to)
                        ->whereDate('adboosts.expiry_date','>',$date)->where('adboosts.status',0)
                        ->orderBy('adboosts.boost_type','asc')
                        ->orderBy('adboosts.created_at','asc')->get();
                }else{
                    $user=	DB::table('ads')->leftJoin('adboosts', 'ads.id', '=', 'adboosts.post_id')
                        ->select('ads.*','adboosts.boost_type','adboosts.days')
                        ->where('ads.block_status',0)->where('ads.mobile_verify','true')->where('ads.country_id',$r->country_id)
                        ->where('ads.activation_status',0)->where('ads.delete_status',1)
                        ->whereNotIn('ads.user_id',$blocked_by)->whereNotIn('ads.user_id',$blocked_to)
                        ->whereDate('adboosts.expiry_date','>',$date)->where('adboosts.status',0)
                        ->orderBy('adboosts.boost_type','asc')->where('ads.main_category',$r->type)
                        ->orderBy('adboosts.created_at','asc')->get();
                }


                $boost = adboost::where('status',1)->whereDate('expiry_date','>',$date)->pluck('post_id')->toArray();

                if (!empty($r->location) && !empty($r->cat_id)) {
                    $data1=DB::table('ads')->whereNotIn('user_id',$blocked_by)->whereNotIn('post_id',$boost)->whereNotIn('user_id',$blocked_to)->where('block_status','0')->where('activation_status','0')->where('delete_status',1)->where('mobile_verify','true')->where('ads.country_id',$r->country_id)
                            ->where('main_category',$r->type)->where('category_id',$r->cat_id)->where('city',$r->location)
                            ->orderBy('id', 'desc')->get();
                
                }elseif (!empty($r->cat_id)) {
                    $data1=DB::table('ads')->whereNotIn('user_id',$blocked_by)->whereNotIn('post_id',$boost)->whereNotIn('user_id',$blocked_to)->where('block_status','0')->where('activation_status','0')->where('delete_status',1)->where('mobile_verify','true')->where('ads.country_id',$r->country_id)
                            ->where('main_category',$r->type)->where('category_id',$r->cat_id)
                            ->orderBy('id', 'desc')->get();
                
                }elseif (!empty($r->location)) {
                    $data1=DB::table('ads')->whereNotIn('user_id',$blocked_by)->whereNotIn('post_id',$boost)->whereNotIn('user_id',$blocked_to)->where('block_status','0')->where('activation_status','0')->where('delete_status',1)->where('mobile_verify','true')->where('ads.country_id',$r->country_id)
                            ->where('main_category',$r->type)->where('city',$r->location)
                        ->orderBy('id', 'desc')->get();
                }else if($r->type =="all"){
                    $data1=DB::table('ads')->whereNotIn('user_id',$blocked_by)->whereNotIn('post_id',$boost)->whereNotIn('user_id',$blocked_to)->where('block_status','0')->where('activation_status','0')->where('mobile_verify','true')->where('delete_status',1)->where('ads.country_id',$r->country_id)
                            ->orderBy('id', 'desc')->get();
                }else{
                    $data1=DB::table('ads')->whereNotIn('user_id',$blocked_by)->whereNotIn('post_id',$boost)->whereNotIn('user_id',$blocked_to)->where('block_status','0')->where('activation_status','0')->where('main_category',$r->type)->where('delete_status',1)->where('mobile_verify','true')->where('ads.country_id',$r->country_id)
                            ->orderBy('id', 'desc')->get();
                }

                $da = $user->merge($data1)->unique('id'); 
                // $da= $da->unique('id')->pluck('id')->toArray();
                // dd($da);
                $da = $da->forPage($offset,$limit)->values();



                foreach($da as $d){
                    $user_adcount=Ads::where('user_id',$d->user_id)->where('mobile_verify','true')->where('activation_status',0)->where('block_status',0)->where('delete_status',1)->count();
                    $d->ads_count = $user_adcount;
                    $user=	DB::table('users')->leftJoin('signups', 'signups.user_id', '=', 'users.id')
                                ->select('users.id','users.name','users.profile_image','users.created_at')
                                ->where('users.id',$d->user_id)->where('delete_status',1)->first();

                    // follow user -------------
                        $follow_count = FollowUser::where('user_id',$r->user_id)->where('following_id',$d->user_id)->count();
                        $follow_user='false';
                        
                        if ($follow_count>0) {
                            $follow_user='true';
                        }
                        $d->follow_user = $follow_user;

                    // follow userr end----------
                        if($user){
                            $d->user_name = $user->name;
                            $d->user_image = $user->profile_image;
                            $d->user_regiterdate = date('Y-m-d H:i:s', strtotime($user->created_at));
                        }
                        $d->favorite_count=0;
                        $d->views_count=0;
                        $d->like_count=0;
                        $d->fav_stattus='No';
                        $d->like_stattus='No';

                        $like_datas=DB::table('adcounts')->where('post_id',$d->id)->get();
                        $favorite_count=0;
                        $views_count=0;
                        $like_count=0;
                        $fav_stattus='No';
                        $like_stattus='No';
                            
                        foreach ($like_datas as $like_data){
                            if ($like_data->favorite==1){
                                $favorite_count++;
                            }
                            if ($like_data->views==1) {
                                $views_count++;
                            }
                            if ($like_data->likes==1) {
                                $like_count++;
                            }

                            if ($like_data->user_id==$r->user_id) {
                                if ($like_data->favorite==1){						
                                    $fav_stattus='Yes';
                                }
                                if ($like_data->likes==1) {						
                                    $like_stattus='Yes';
                                }
                            }

                            $d->favorite_count=$favorite_count;
                            $d->views_count=$views_count;
                            $d->like_count=$like_count;
                            $d->fav_stattus=$fav_stattus;
                            $d->like_stattus=$like_stattus;

                        }
                            
                }

                // get blocked notifications


                $deleted_ads=array();
                        $user_adcount=Ads::where('user_id',$r->user_id)->get();
                        if ($this->check_count($user_adcount)>0) 
                        {
                            foreach ($user_adcount as $adcount_value) 
                            {
                                

                                if ($adcount_value->delete_status==0) 
                                {
                                    $deleted_ads[]=$adcount_value->id;
                                }
                            }
                        }
                $blocked=User::where('delete_status',0)->pluck('id');
              $noty = Notifications::where('user_id',$r->user_id)->whereNotIn('callby',$blocked)
                        ->whereNotIn('refer_id',$deleted_ads)->where('status',0)->count();  
                if ($this->check_count($da)>0) {
                    return response()->json(['data'=>$da ,'noty'=>$noty,'message'=>'Successfully','status'=>'success']);
                }  else{
                    return response()->json(['message'=>'No data found','status'=>'fail']);
                }	
        
            }
            else
            {
                return response()->json(['message'=>'You are blocked by admin. Please contact admin.','status'=>'fail']);
            }
        }
        else
        {
            return response()->json(['message'=>'Invalid User','status'=>'false']);
        }       
    }

    public function home_data(Request $r)
    {
        $date = date('Y-m-d');

        $limit = 10;
        $offset = $r->index ;
        $check = User::where('delete_status',1)->first();
        // where('id',$r->user_id)->
        if($check)
        {
            if($check->block_status==1)
            {
                // $blocked_to = Block::where('user_id',$r->user_id)->pluck('block_id');
                // $blocked_by = Block::where('block_id',$r->block_id)->pluck('user_id');

                if (!empty($r->location) && !empty($r->cat_id)) {
                    $user=	DB::table('ads')->leftJoin('adboosts', 'ads.id', '=', 'adboosts.post_id')
                        ->select('ads.*','adboosts.boost_type','adboosts.days')
                        ->where('ads.block_status',0)->where('ads.mobile_verify','true')
                        ->where('ads.activation_status',0)->where('ads.delete_status',1)
                        // ->whereNotIn('ads.user_id',$blocked_by)->whereNotIn('ads.user_id',$blocked_to)
                        ->whereDate('adboosts.expiry_date','>',$date)->where('adboosts.status',0)
                        ->where('ads.main_category',$r->type)->where('ads.category_id',$r->cat_id)->where('ads.city',$r->location)
                        ->orderBy('adboosts.boost_type','asc')
                        ->orderBy('adboosts.created_at','asc')->get(); 
                                //->where('adboosts.user_id',$r->user_id)    
                }elseif (!empty($r->cat_id)) {
                    $user=	DB::table('ads')->leftJoin('adboosts', 'ads.id', '=', 'adboosts.post_id')
                        ->select('ads.*','adboosts.boost_type','adboosts.days')
                        ->where('ads.block_status',0)->where('ads.mobile_verify','true')
                        ->where('ads.activation_status',0)->where('ads.delete_status',1)
                        // ->whereNotIn('ads.user_id',$blocked_by)->whereNotIn('ads.user_id',$blocked_to)
                        ->whereDate('adboosts.expiry_date','>',$date)->where('adboosts.status',0)
                        ->where('ads.main_category',$r->type)->where('ads.category_id',$r->cat_id)
                        ->orderBy('adboosts.boost_type','asc')
                        ->orderBy('adboosts.created_at','asc')->get();
                }elseif (!empty($r->location)) {
                    $user=	DB::table('ads')->leftJoin('adboosts', 'ads.id', '=', 'adboosts.post_id')
                        ->select('ads.*','adboosts.boost_type','adboosts.days')
                        ->where('ads.block_status',0)->where('ads.mobile_verify','true')
                        ->where('ads.activation_status',0)->where('ads.delete_status',1)
                        // ->whereNotIn('ads.user_id',$blocked_by)->whereNotIn('ads.user_id',$blocked_to)
                        ->whereDate('adboosts.expiry_date','>',$date)->where('adboosts.status',0)
                        ->where('ads.main_category',$r->type)->where('ads.city',$r->location)
                        ->orderBy('adboosts.boost_type','asc')
                        ->orderBy('adboosts.created_at','asc')->get();
                }else if($r->type =="all"){
                    $user=	DB::table('ads')->leftJoin('adboosts', 'ads.id', '=', 'adboosts.post_id')
                        ->select('ads.*','adboosts.boost_type','adboosts.days')
                        ->where('ads.block_status',0)->where('ads.mobile_verify','true')
                        ->where('ads.activation_status',0)->where('ads.delete_status',1)
                        // ->whereNotIn('ads.user_id',$blocked_by)->whereNotIn('ads.user_id',$blocked_to)
                        ->whereDate('adboosts.expiry_date','>',$date)->where('adboosts.status',0)
                        ->orderBy('adboosts.boost_type','asc')
                        ->orderBy('adboosts.created_at','asc')->get();
                }else{
                    $user=	DB::table('ads')->leftJoin('adboosts', 'ads.id', '=', 'adboosts.post_id')
                        ->select('ads.*','adboosts.boost_type','adboosts.days')
                        ->where('ads.block_status',0)->where('ads.mobile_verify','true')
                        ->where('ads.activation_status',0)->where('ads.delete_status',1)
                        // ->whereNotIn('ads.user_id',$blocked_by)->whereNotIn('ads.user_id',$blocked_to)
                        ->whereDate('adboosts.expiry_date','>',$date)->where('adboosts.status',0)
                        ->orderBy('adboosts.boost_type','asc')->where('ads.main_category',$r->type)
                        ->orderBy('adboosts.created_at','asc')->get();
                }


                $boost = adboost::where('status',1)->whereDate('expiry_date','>',$date)->pluck('post_id')->toArray();

                if (!empty($r->location) && !empty($r->cat_id)) {
                    $data1=DB::table('ads')->whereNotIn('post_id',$boost)->where('block_status','0')->where('activation_status','0')->where('delete_status',1)->where('mobile_verify','true')				
                            ->where('main_category',$r->type)->where('category_id',$r->cat_id)->where('city',$r->location)
                            ->orderBy('id', 'desc')->get();
                            // ->whereNotIn('user_id',$blocked_by)->whereNotIn('user_id',$blocked_to)
                
                }elseif (!empty($r->cat_id)) {
                    $data1=DB::table('ads')->whereNotIn('post_id',$boost)->where('block_status','0')->where('activation_status','0')->where('delete_status',1)->where('mobile_verify','true')				
                            ->where('main_category',$r->type)->where('category_id',$r->cat_id)
                            ->orderBy('id', 'desc')->get();
                            // ->whereNotIn('user_id',$blocked_by)->whereNotIn('user_id',$blocked_to)
                
                }elseif (!empty($r->location)) {
                    $data1=DB::table('ads')->whereNotIn('post_id',$boost)->where('block_status','0')->where('activation_status','0')->where('delete_status',1)->where('mobile_verify','true')				
                            ->where('main_category',$r->type)->where('city',$r->location)
                        ->orderBy('id', 'desc')->get();
                        // ->whereNotIn('user_id',$blocked_by)->whereNotIn('user_id',$blocked_to)
                }else if($r->type =="all"){
                    $data1=DB::table('ads')->whereNotIn('post_id',$boost)->where('block_status','0')->where('activation_status','0')->where('mobile_verify','true')->where('delete_status',1)					
                            ->orderBy('id', 'desc')->get();
                            //->whereNotIn('user_id',$blocked_by)->whereNotIn('user_id',$blocked_to)
                }else{
                    $data1=DB::table('ads')->whereNotIn('post_id',$boost)->where('block_status','0')->where('activation_status','0')->where('main_category',$r->type)->where('delete_status',1)->where('mobile_verify','true')					
                            ->orderBy('id', 'desc')->get();
                            // ->whereNotIn('user_id',$blocked_by)->whereNotIn('user_id',$blocked_to)
                }

                $da = $user->merge($data1)->unique('id'); 
                // $da= $da->unique('id')->pluck('id')->toArray();
                // dd($da);
                $da = $da->forPage($offset,$limit)->values();



                foreach($da as $d){
                    $user_adcount=Ads::where('user_id',$d->user_id)->where('mobile_verify','true')->where('activation_status',0)->where('block_status',0)->where('delete_status',1)->count();
                    $d->ads_count = $user_adcount;
                    $user=	DB::table('users')->leftJoin('signups', 'signups.user_id', '=', 'users.id')
                                ->select('users.id','users.name','users.profile_image','users.created_at')
                                ->where('users.id',$d->user_id)->where('delete_status',1)->first();

                    // follow user -------------
                        $follow_count = FollowUser::where('following_id',$d->user_id)->count();
                        // where('user_id',$r->user_id)->
                        $follow_user='false';
                        
                        if ($follow_count>0) {
                            $follow_user='true';
                        }
                        $d->follow_user = $follow_user;

                    // follow userr end----------
                        if($user){
                            $d->user_name = $user->name;
                            $d->user_image = $user->profile_image;
                            $d->user_regiterdate = date('Y-m-d H:i:s', strtotime($user->created_at));
                        }
                        $d->favorite_count=0;
                        $d->views_count=0;
                        $d->like_count=0;
                        $d->fav_stattus='No';
                        $d->like_stattus='No';

                        $like_datas=DB::table('adcounts')->where('post_id',$d->id)->get();
                        $favorite_count=0;
                        $views_count=0;
                        $like_count=0;
                        $fav_stattus='No';
                        $like_stattus='No';
                            
                        foreach ($like_datas as $like_data){
                            if ($like_data->favorite==1){
                                $favorite_count++;
                            }
                            if ($like_data->views==1) {
                                $views_count++;
                            }
                            if ($like_data->likes==1) {
                                $like_count++;
                            }

                            // if ($like_data->user_id==$r->user_id) {
                                if ($like_data->favorite==1){						
                                    $fav_stattus='Yes';
                                }
                                if ($like_data->likes==1) {						
                                    $like_stattus='Yes';
                                }
                            // }

                            $d->favorite_count=$favorite_count;
                            $d->views_count=$views_count;
                            $d->like_count=$like_count;
                            $d->fav_stattus=$fav_stattus;
                            $d->like_stattus=$like_stattus;

                        }
                            
                }

                // get blocked notifications


                $deleted_ads=array();
                        $user_adcount=Ads::get();
                        // where('user_id',$r->user_id)
                        if ($this->check_count($user_adcount)>0) 
                        {
                            foreach ($user_adcount as $adcount_value) 
                            {
                                

                                if ($adcount_value->delete_status==0) 
                                {
                                    $deleted_ads[]=$adcount_value->id;
                                }
                            }
                        }
                $blocked=User::where('delete_status',0)->pluck('id');
              $noty = Notifications::whereNotIn('callby',$blocked)
                        ->whereNotIn('refer_id',$deleted_ads)->where('status',0)->count();  
                        // where('user_id',$r->user_id)->
                if ($this->check_count($da)>0) {
                    return response()->json(['data'=>$da ,'noty'=>$noty,'message'=>'Successfully','status'=>'success']);
                }  else{
                    return response()->json(['message'=>'No data found','status'=>'fail']);
                }	
        
            }
            else
            {
                return response()->json(['message'=>'You are blocked by admin. Please contact admin.','status'=>'fail']);
            }
        }
        else
        {
            return response()->json(['message'=>'Invalid User','status'=>'false']);
        }       
    }

    // Boost ads 
        public function boost_ads(Request $req)
        {
            if(!empty($req->userid) && !empty($req->ad_id) && !empty($req->credit) && !empty($req->days))
            {
                $check = mycoins::where('user_id',$req->userid)->where('wallet_type',1)->sum('coins');
                $credit = $req->credit;
                if($check < $credit)
                {
                    return response()->json(['message'=>'You Dont have enough credit to boost ad!!..','status'=>'false']);
                }
                else
                {
                    $check = adboost::where('user_id',$req->userid)->where('post_id',$req->ad_id)->where('status',0)->first();
                    if($check)
                    {
                        return response()->json(['message'=>'Ad is already boosted!!..','status'=>'false']);
                    }
                    else
                    {
                        $date = date('Y-m-d');

                        $insert = new adboost;
                        $insert->user_id = $req->userid;
                        $insert->post_id = $req->ad_id;
                        $insert->boost_type = $req->type;
                        $insert->date = $date;
                        $insert->days = $req->days;
                        $insert->expiry_date = date('Y-m-d',strtotime($date .' + '.$req->days.' days')); 
                        $insert->credits = $req->credit;
                        $save = $insert->save();

                        if($save)
                        {
                        	$milliseconds = round(microtime(true) * 1000);
					
                            $add = new mycoins;
                            $add->user_id = $req->userid;
                            $add->tital = "Credits redeeem to boost ad";
                            $add->refer_id = $req->ad_id;
                            $add->coins = -$req->credit;
                            $add->type = 'redeem';
                            $add->wallet_type = 1;
                            $add->timestamp = $milliseconds;
                            
                            $add->save();
                            return response()->json(['message'=>'success','status'=>'true','data'=>$insert]);
                        }
                        else
                        {
                            return response()->json(['message'=>'Something Went Wrong!!..','status'=>'false']);
                        }
                    }
                }
            } 
            else
            {
                return response()->json(['message'=>'Param Missing!!..','status'=>'false']);
            }
        }
    // End Function

    // Seen Unseen count of notification
        public function notification_status(Request $req){
            if($req->type=='all'){
                $check = Notifications::where('user_id',$req->noty_id)->update(['status' => 1]);
                if($check){
                    return response()->json(['message'=>'success','status'=>'true']);
                }else{
                    return response()->json(['message'=>'error','status'=>'false']);
                }
            }else{
                if(!empty($req->noty_id)){
                    $check = Notifications::where('id',$req->noty_id)->first();
                    if($check){
                        $check->status = 1;
                        $save = $check->save();
                        if($save){
                            return response()->json(['message'=>'success','status'=>'true','data'=>$check]);
                        }else{
                            return response()->json(['message'=>'Something went wrong','status'=>'false']);
                        }
                    }else{
                        return response()->json(['message'=>'Notification id does not exist','status'=>'fa;se']);
                    }
                }else{
                    return response()->json(['message'=>'Param Missing','status'=>'false']);
                }
            }
        }
    // End function

    // List of blocked users by another user functions
        public function blocked_list(Request $req)
        {
            if(!empty($req->user_id)){
                $blocked = Block::where('user_id',$req->user_id)->pluck('block_id');
                if(!$blocked->isEmpty()){
                    $data = User::whereIn('id',$blocked)->select('id','name','profile_image')->orderBy('id','desc')->get();
                    return response()->json(['message'=>'Success','data'=>$data,'status'=>'true']);
                }else{
                    return response()->json(['message'=>'No Blocked User Found!!..','status'=>'false']);
                }
            }else{
                return response()->json(['message'=>'Param Missing!!..','status'=>'false']);
            }
        }
    // End Function

    // Block user Function
        public function block_user(Request $req)
        {
            if(!empty($req->user_id) && !empty($req->block_id) && !empty($req->type)){
                if($req->type=="block"){
                    $check = Block::where('user_id',$req->user_id)->where('block_id',$req->block_id)->first();
                    if($check){
                        return response()->json(['message'=>'User Already Blocked!!..','status'=>'false']);
                    }else{
                        $insert = new Block;
                        $insert->user_id = $req->user_id;
                        $insert->block_id = $req->block_id;
                        $save =$insert->save();
                        if($save){
                            return response()->json(['message'=>'Successfully Blocked User!!..','status'=>'true']);
                        }else{
                            return response()->json(['message'=>'Something Went Wrong!!..','status'=>'false']);
                        }
                    }
                }else{
                    $check = Block::where('user_id',$req->user_id)->where('block_id',$req->block_id)->delete();
                    if($check){
                        return response()->json(['message'=>'Successfully Unblocked User!!..','status'=>'true']);
                    }else{
                        return response()->json(['message'=>'Something Went Wrong!!..','status'=>'false']);        
                    }
                }
            }else{
                return response()->json(['message'=>'Param Missing!!..','status'=>'false']);
            }
        }
    // End function

    // Invitation Email Function
        public function invitation_email(Request $req)
        {
            if(!empty($req->email) && !empty($req->sender) && !empty($req->receiver) && !empty($req->link)){

                $data = array('sender'=>$req->sender,'receiver'=>$req->receiver,'link'=>$req->link);
                $email = $req->email;

                Mail::send('invite_email', $data, function($message) use ($email)
                {
                    $message->from('info@openbazar.nl', "Open Bazar");
                    $message->subject("Open Bazar");
                    $message->to($email,'User');
                } );

                if (Mail::failures()) {
                    // return response showing failed emails
                    return response()->json(['message'=>'something went wrong','status'=>'false']);
                }else{
                    return response()->json(['message'=>'success','status'=>'true']);
                }

            }else{
                return response()->json(['message'=>'param missing','status'=>'false']);
            }
        }
    // End Function

    // Count ads Category Function
        public function ad_counts(Request $req)
        {

            if(!empty($req->main_category)){

                $check = Category::where('main_category',$req->main_category)->where('publish',1)->where('delete_status',1)
                ->orderBy('position')->get();

                foreach($check as $key){
                    $ads = Ads::where('category_id',$key->id)->where('mobile_verify','true')->where('activation_status',0)->where('block_status',0)->where('delete_status',1)->count();
                    $key->ad_count = $ads;
                }

                return response()->json(['message'=>'success','status'=>'true','data'=>$check]);

            }else{

                return response()->json(['message'=>'parma missiog','status'=>'fail']);

            }
        }
    // End FUnction
    
    // Api Contact Us Function  
        public function contact_us(Request $req)
        {

            if(!empty($req->name) && !empty($req->email) && !empty($req->mobile)){

                $check = ContactUs::where('name',$req->name)->where('email',$req->email)->where('mobile',$req->mobile)->where('subject',$req->subject)->where('message',$req->message)->where('status',1)->first();

                if($check){

                    return response()->json(['message'=>'Query Exist. We will Contact you soon!!..','status'=>'false']);

                }else{

                    $insert = new ContactUs;
                    $insert->name = $req->name;
                    $insert->email = $req->email;
                    $insert->mobile = $req->mobile;
                    $insert->subject = $req->subject;
                    $insert->message = $req->message;

                    $save = $insert->save();
                    if($save){

                        return response()->json(['message'=>'Query submitted successfully!!..','status'=>'true']);

                    }else{

                        return response()->json(['message'=>'Something went wrong','status'=>'false']);
                    }                    

                }

            }else{

                return response()->json(['message'=>'param missing','status'=>'false']);

            }
        }
    // End Function

    // Search api Functions
        public function search_ads(Request $r)
        {
            if ($r->has('country_id')) {
            
                $check1 =Ads::where('activation_status',0)->where('mobile_verify','true')->where('block_status',0)->where('country_id',$r->country_id)->where('delete_status',1)->get();

            }
            else{
                $check1 =Ads::where('activation_status',0)->where('mobile_verify','true')->where('block_status',0)->where('delete_status',1)->get();
                }
                $arr =  $arr1 = array();
                foreach($check1 as $key){
                    $array = json_decode($key->category);
                    foreach($array as $v=>$val)
                    {
                        // echo $val->tital;
                        if($val->value == $r->search){
                            $arr[] = $key->id;
                        }
                        
                        if($val->value == $r->condition){
                            $arr1[] = $key->id;
                        }
                    }
                }
                $limit = 20;
                if (isset($r->index)) {
                    $offset = $r->index * $limit;
                } else {
                    $offset = 0;
                }
            
                $check = Ads::where('activation_status',0)
                        ->where('mobile_verify','true')
                        ->where('delete_status',1)
                        ->where('block_status',0);
                    
                if(!empty($r->category)){
                    $check = $check->where('main_category',$r->category);
                }
            
                if(!empty($r->country_id)){

                $check = $check->where('country_id',$r->country_id);
                }
                if(!empty($r->price_type)){
                    if($r->price_type=="free"){
                        $check = $check->where('price',0)->where('price_type','free');
                    }else if($r->price_type=="exchange"){
                        $check = $check->where('price',0)->where('price_type','exchange');
                    }else if($r->price_type == "price"){
                        if(!empty($r->price_min) && !empty($r->price_max)){
                            $check = $check->where('price','>',$r->price_min)->where('price','<',$r->price_max);
                        }
                    }else{
                        $check = $check->where('price_type',$r->price_type);
                    }
                }
                if(!empty($r->city)){
                    $check = $check->where('city',$r->city);
                }
                if(!empty($r->cat_id)){
                    $check = $check->where('category_id',$r->cat_id);
                }
                if(!empty($r->condition)){
                    $check = $check->whereIn('id',$arr1);
                }
                if(!empty($r->sort)){
                    if($r->sort == "new"){
                        $check = $check->orderBY('id','desc');
                    }else if($r->sort == "old"){
                        $check = $check->orderBY('id','asc');
                    }else if($r->sort == "low"){
                        $check = $check->orderBY('price','asc');
                    }else{
                        $check = $check->orderBY('price','desc');
                    }
                }
                if(!isset($r->category) || !isset($r->price_type) || !isset($r->city) || !isset($r->condition) ){
                    $search = $r->search;
                    if(!empty($search)){
                        $check = $check->where(function ($query) use ($search,$arr) {
                            $query->whereIn('id',$arr)->orWhere('ad_title','like','%'. $search .'%')
                            ->orWhere('ad_description','like','%'. $search .'%');
                        });
                    }                    
                }        
            // }
                

                
                $data = $check->get();
//                 print_r($check->toSql());
                $date = date('Y-m-d');
                
                        foreach($data as $d)
                        {

                            $boost = adboost::where('post_id',$d->id)->where('status',0)->whereDate('expiry_date','>',$date)->first();
                            if($boost)
                            {
                                $d->days = $boost->days;
                                $d->boost_type = $boost->boost_type;
                            }
                            else {
                                $d->days = "";
                                $d->boost_type = "";
                            }
                                $user=	DB::table('users')
                                            ->leftJoin('signups', 'signups.user_id', '=', 'users.id')
                                            ->select('users.id','users.name','users.profile_image','users.created_at')
                                            ->where('users.id',$d->user_id)->where('delete_status',1)->first();
                                $user_adcount=Ads::where('user_id',$r->user_id)->where('activation_status',0)->where('block_status',0)->where('delete_status',1)->count();
                                $d->ads_count = $user_adcount;
        
        
                                // follow user -------------
                                            $follow_count = FollowUser::where('user_id',$r->user_id)->where('following_id',$d->user_id)->count();
        
                                                $follow_user='false';
                                                    if ($follow_count>0) {
                                                        $follow_user='true';
                                                    }
                                                $d->follow_user = $follow_user;
        
                                // // follow userr end----------
        
        
                                        if($user)
                                        {
                                            $d->user_name = $user->name;
                                            $d->user_image = $user->profile_image;
                                    
                                            $d->user_regiterdate =date('Y-m-d H:i:s', strtotime($user->created_at));
        
                                        }
                                        $d->favorite_count=0;
                                        $d->views_count=0;
                                        $d->like_count=0;
                                        $d->fav_stattus='No';
                                        $d->like_stattus='No';
        
                                        $like_datas=DB::table('adcounts')->where('post_id',$d->id)->get();
                                            $favorite_count=0;
                                            $views_count=0;
                                            $like_count=0;
                                            $fav_stattus='No';
                                            $like_stattus='No';
                                            
                                        foreach ($like_datas as $like_data) 
                                        {
                                            if ($like_data->favorite==1) 
                                            {
                                                $favorite_count++;
                                            }
                                            if ($like_data->views==1) 
                                            {
                                                $views_count++;
                                            }
                                            if ($like_data->likes==1) 
                                            {
                                                $like_count++;
                                            }
        
                                            if ($like_data->user_id==$r->user_id) 
                                            {
                                                if ($like_data->favorite==1) 
                                                {						
                                                    $fav_stattus='Yes';
                                                }
                                                if ($like_data->likes==1) 
                                                {						
                                                    $like_stattus='Yes';
                                                }
                                                
                                            }
        
                                            $d->favorite_count=$favorite_count;
                                            $d->views_count=$views_count;
                                            $d->like_count=$like_count;
                                            $d->fav_stattus=$fav_stattus;
                                            $d->like_stattus=$like_stattus;
        
                                        }
                            
                        }
                
                return response()->json(['message'=>'Success','status'=>'true','data'=>$data]);
        }
    // End Functions

    // Logout api Function
        public function logout(Request $req)
        {
            if(!empty($req->device_id) )
            {
                $check = notification_id::where('device_id',$req->device_id)->first();
                  if($check)
                {
                    //$check->push_id = "";
                    $delete = $check->delete();

                    if($delete)
                    {
                        return response()->json(['message'=>'success','status'=>'true']);
                    }
                    else{
                        return response()->json(['message'=>'Something went wrong!!..','status'=>'fail']);
                    }

                }else{
                    return response()->json(['message'=>'Invalid User!!..','status'=>'fail']);
                }
            }else{
                return response()->json(['message'=>'param missing','status'=>'fail']);
            }
        }
    // End Functions



       // send push to admin app
    public function sendpush_message(Request $r){
            //push notification
                $push=Player_id::where('user_id',$r->id)->select('player_id')->get();


                
                if(count($push)>0)
                {
                  // $player_id=$push->player_id;


 foreach($push as $d)
        {
            if($d->player_id!='')
            {
                $tokens[] = $d->player_id;
            }
    
        }

                  $message=$r->msg;
                  $data=$r->json;
                  $heading=$r->heading;
                  
                $data=  $this->sendPushmsg($tokens,$message,$data,$heading);
                        return response()->json(['status'=>'success','push'=>$data]);
    
                }
       }



       public function send_push(Request $r){


        
                $tokens[] = '67ac9229-028d-4535-abe4-3a04db2a0dd0';
          $content='test';
            $data="test";
        $massage="test1";

         $content = array(

          "en" => $massage

          );

       // print_r($cat_data);
       $fields = array(
          'app_id' => 'b31c1980-f8db-47fe-bb76-b29236584dd8',
          'include_player_ids' =>$tokens,
           'contents' => $content,
           'data' =>array("data"=>$data),
           'android_channel_id'=> 'fe089a69-0f0e-4579-8727-97c066465201'
          );

         $fields = json_encode($fields);
           //print("\nJSON sent:\n");
         //print($fields);


        $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                      'Authorization: Basic YTU5ZTk3MTktZDE4Mi00NjVhLThiYmItNTU3MzUwZjgwOTc3'));

         curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
         curl_setopt($ch, CURLOPT_HEADER, FALSE);
         curl_setopt($ch, CURLOPT_POST, TRUE);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

         $response = curl_exec($ch);
         curl_close($ch);

         return $response;
         

       }
}
