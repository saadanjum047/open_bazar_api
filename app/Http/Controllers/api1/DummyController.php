<?php

namespace App\Http\Controllers\api1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use App\Category;
use App\Ads;
use App\bids;
use App\signups;
use App\adcounts;
use App\mycoins;
use DB;
use App\User;
use App\SubCategory;
use App\Product;
use App\PostReport;
use App\FollowUser;
use App\Notifications;
use App\Block;
use ImageOptimizer;

class DummyController extends Controller
{
    public function get_productcat(Request $r)
	{
		$user_detail=Product::where('publish','1')->where('subcategory_id',$r->id)->where('delete_status',0)
		->get();
		
		if ($this->check_count($user_detail)) 
		{
			foreach ($user_detail as $value) 
			{
				$pro = json_decode($value->product,true);
				
				$key1 = array();
				foreach($pro as $count=>$key)
				{
					
					
					if($count !='label' && $count !='image')
					{
						$array = array();
						$mc = 0;
						foreach($key as $c=>$i)
						{
							$array[$mc."".$c] = $i;
							$mc++;
						}
						$key1[$count]= $array;
					}else{
						$key1[$count] = $key; 
					}
				}
				$value->product = $key1;
				// $value->product1 = $pro;
			}
			
			return response()->json(['data'=>$user_detail,'message'=>'Successfully fatched','status'=>'success']);
		}
		else
		{
			return response()->json(['message'=>'No products exist','status'=>'fail']);
		}
	}
}
