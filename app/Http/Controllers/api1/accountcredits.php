<?php

namespace App\Http\Controllers\api1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\mycoins;
use App\Ads;
use App\User;
use App\FollowUser;
use DB;
use App\Notifications;
use App\adboost;
use App\adcounts;

class accountcredits extends Controller
{
  // get my bid
	public function get_mycoins(Request $r)
	{
		if (!empty($r->user_id) ) 
		{
			$use = User::where('id',$r->user_id)->first();
			if($use)
			{
				$wallet_status = $use->wallet_status;
				if($use->wallet_status==1)
				{
					$limit = 10;
					if (isset($r->index)) {
						$offset = $r->index * $limit;
					} else {
						$offset = 0;
					}				
					$data=mycoins::where('user_id',$r->user_id)->where('wallet_type',$r->type)->offset($offset)->limit($limit)->orderBy('id', 'desc')->get();
					$sum=mycoins::where('user_id',$r->user_id)->where('wallet_type',$r->type)->sum('coins');
			
					if ($this->check_count($data)>0) 
					{
						return response()->json(['data'=>$data ,'mycoins'=>$sum ,'wallet_status'=>$wallet_status,'message'=>'Successfully fetch','status'=>'success']);
					}
					else
					{
						return response()->json(['message'=>'Not data','status'=>'fail']);
					}
				}
				else
				{
					return response()->json(['message'=>'Wallet blocked','wallet_status'=>$wallet_status,'status'=>'fail']);
				}
			}
			else
			{
				return response()->json(['message'=>'User Not Found','status'=>'fail']);
			}	
		}
		else
		{
			return response()->json(['message'=>'Param Missing','status'=>'fail']);
		}
	}

	// get my ads
	public function get_myads(Request $r)
	{ 
		$date = date('Y-m-d');
		$check_sum = adboost::where('user_id',$r->user_id)->whereDate('expiry_date','>',$date)->where('status',0)->orderBy('created_at','asc')->pluck('post_id')->toArray();

		$limit = 10;
		if (isset($r->index)) {
			$offset = $r->index * $limit;

		} else {
			$offset = 0;
		}

		$data=Ads::where('user_id',$r->user_id)->where('block_status','0')->where('delete_status',1)->where('activation_status','0')->offset($offset)->limit($limit)->orderBy('id', 'desc')->get();
		foreach($data as $d)
		{
			if(in_array($d->id,$check_sum))
			{
				$boost = adboost::where('post_id',$d->id)->whereDate('expiry_date','>',$date)->where('status',0)->first();
				if($boost)
				{
					$d->boost_type = $boost->boost_type;
					$d->boost_days = $boost->days;
				}
			}

			$user = User::where('id',$d->user_id)->first();
			if($user)
			{
				$d->user_name = $user->name;
				$d->user_image = $user->profile_image;
			}

			$fav_stattus='No';
			$like_stattus='No';
			$favorite_count=0;
			$views_count=0;
			$like_count=0;

   $like_datas=DB::table('adcounts')->where('post_id',$d->id)->get();
                      $favorite_count=0;
                      $views_count=0;
                      $like_count=0;
                      $fav_stattus='No';
                      $like_stattus='No';
                      
                    foreach ($like_datas as $like_data) 
                    {
                      if ($like_data->favorite==1) 
                      {
                        $favorite_count++;
                      }
                      if ($like_data->views==1) 
                      {
                        $views_count++;
                      }
                      if ($like_data->likes==1) 
                      {
                        $like_count++;
                      }
              
                      if ($like_data->user_id==$r->user_id) 
                      {
                        if ($like_data->favorite==1) 
                        {           
                          $fav_stattus='Yes';
                        }
                        if ($like_data->likes==1) 
                        {           
                          $like_stattus='Yes';
                        }
                        
                      }
              
                      $d->favorite_count=$favorite_count;
                      $d->views_count=$views_count;
                      $d->like_count=$like_count;
                      $d->fav_stattus=$fav_stattus;
                      $d->like_stattus=$like_stattus;
              
                    }


			// $like_datas=DB::table('adcounts')->where('post_id',$d->id)->select('user_id','favorite','likes','views', DB::raw('SUM(favorite) as favorite_count'), DB::raw('SUM(views) as views_count'), DB::raw('SUM(likes) as like_count'))->groupBy('id')->get();
			// foreach ($like_datas as $like_data) 
			// {
			// 	if ($like_data->user_id==$r->user_id) 
			// 	{
			// 		if ($like_data->favorite==1) 
			// 		{						
			// 			$fav_stattus='Yes';
			// 		}
			// 		if ($like_data->likes==1) 
			// 		{						
			// 			$like_stattus='Yes';
			// 		}
			// 	}

			// 	$d->favorite_count=$like_data->favorite_count;
			// 	$d->views_count=$like_data->views_count;
			// 	$d->like_count=$like_data->like_count;
			// 	$d->fav_stattus=$fav_stattus;
			// 	$d->like_stattus=$like_stattus;
			// }
		}
		
		if ($this->check_count($data)>0) 
		{
			return response()->json(['data'=>$data ,'message'=>'Successfully','status'=>'success']);
		}  
		else
		{
			return response()->json(['message'=>'No data found','status'=>'fail']);
		}		
	}


	// My Profile
	public function myprofile(Request $r)
	{
		if(!empty($r->user_id))
		{
			$appuser = User::where('delete_status',1)->where('id',$r->user_id)->first();
			if($appuser)
			{
				if($appuser->block_status==1)
				{
					$user_data=array();
					$user_data['user_id'] = $r->user_id;
					$user_data['verify_google'] = $appuser->verify_google;
					$user_data['verify_facebook'] = $appuser->verify_facebook;
					$user_data['verify_phone'] = $appuser->verify_phone;
					if(!empty($appuser->plan)) {
						$user_data['plan'] = $appuser->plan;
					} else {
						$user_data['plan'] = 'Free Member';	
					}
					$likes_count=0;
					$fav_count=0;
					$view_count=0;
					$data=adcounts::where('user_id',$r->user_id)->get();								
					if($data)
					{
						foreach ($data as $value) 
						{
							$get_add=Ads::where('id',$value->post_id)->where('mobile_verify','true')->where('activation_status',0)->where('block_status',0)
								->where('delete_status',1)->first();
							if ($this->check_count($get_add)>0) 
							{
								$fav_count=$fav_count+$value->favorite;
								$likes_count=$likes_count+$value->likes;
								$view_count=$view_count+$value->views;									
							}
						}
					}

					$user_data['fav_count'] = $fav_count;
					$user_data['view_count'] = $view_count;
					$user_data['likes_count'] = $likes_count;
					
					$bid=DB::table('bids')->where('user_id',$r->user_id)->where('bid_type',0)->distinct('post_id')->get();
					$bidcount=0;
					if ($bid) 
					{
						foreach ($bid as $bid_value) 
						{
							$Ads_status=Ads::where('id',$bid_value->post_id)->where('mobile_verify','true')->where('delete_status','1')->count();
							if($Ads_status>0)
							{
								$bidcount++;
							}
						}
					}

					$ad_count=0;
					$deleted_ads=array();
					$user_adcount=Ads::where('user_id',$r->user_id)->get();
					if ($this->check_count($user_adcount)>0) 
					{
						foreach ($user_adcount as $adcount_value) 
						{
							if ($adcount_value->activation_status==0 && $adcount_value->block_status==0 && $adcount_value->delete_status==1) 
							{
								$ad_count++;
							}

							if ($adcount_value->delete_status==0) 
							{
								$deleted_ads[]=$adcount_value->id;
							}
						}
					}
					
					$user_data['user_adcount'] = $ad_count;
					$user_data['bidcount']=$bidcount;
// where('following_id',$r->user_id)
					$follow = FollowUser::where('user_id',$r->user_id)->pluck('following_id')->toArray();
					$ad_count = Ads::whereIn('user_id',$follow)->where('mobile_verify','true')->where('activation_status',0)->where('block_status',0)
					->where('delete_status',1)->count();
					$user_data['following_adcount'] = $ad_count;
					
					$followers=0;
					$following=0;
					$check=FollowUser::where('user_id',$r->user_id)->orwhere('following_id',$r->user_id)->get();
					if ($this->check_count($check)>0) 
					{
						$followers=0;
						$following=0;
						foreach($check as $d) {                       
							if($d->user_id == $r->user_id){                           
								$following++;
							}else {                            
								$followers++; 
							}
						}
					}
	
					$user_data['followers']=$followers;
					$user_data['following']=$following;
					
					// get blocked notifications
					$blocked=User::where('delete_status',0)->pluck('id');
					$noty = Notifications::where('user_id',$r->user_id)->whereNotIn('callby',$blocked)->whereNotIn('refer_id',$deleted_ads)->where('status',0)->count();
	
					return response()->json(['data'=>$user_data ,'noty'=>$noty,'message'=>'Successfully','status'=>'success']);
				}
				else
				{
					return response()->json(['message'=>'You are blocked by admin. Please contact admin.','status'=>'fail']);
				}
			}
			else
			{
				return response()->json(['message'=>'invalid user','status'=>'false']);
			}
		}
		else
		{
			return response()->json(['message'=>'param missing','status'=>'false']);
		}
	}

	// Redeem Coin
	public function redeem_coin(Request $r)
	{
		if (!empty($r->user_id) && !empty($r->coin)) 
		{
			$check=mycoins::where('user_id',$r->user_id)->where('wallet_type','0')->sum('coins');
			if ($check>=$r->coin) 
			{
				$cr=$r->coin*100;
				$this->add_coinv1('Redeem '.$r->coin.' Coin to Credits',$cr,'0',$r->user_id,'credit','1');
				$this->add_coinv1('Redeem Coin to Credits','-'.$r->coin,'0',$r->user_id,'redeem','0');
			
				return response()->json(['message'=>'Successfully added','status'=>'success']);
			}
			else
			{
				return response()->json(['message'=>'No suficent balance','status'=>'fail']);
			}
		}
		else 
		{
			return response()->json(['message'=>'Param Missing','status'=>'fail']);
		}
	}

	// My Followers
	public function my_followers(Request $r)
	{
		$limit = 10;
		if (isset($r->index)) {
			$offset = $r->index * $limit;

		} else {
			$offset = 0;
		}

		$data=DB::table('follow_users')->join('users','users.id','=','follow_users.user_id')
				->select('follow_users.following_id','follow_users.user_id','users.name',
					'users.profile_image','users.id as signupid')
				->where('follow_users.following_id',$r->user_id)
				->offset($offset)->limit($limit)->orderBy('follow_users.id', 'desc')->get();
			
		foreach ($data as  $value) 
		{
			$ads_count=Ads::where('user_id',$value->user_id)->where('mobile_verify','true')->where('activation_status',0)->where('block_status',0)->where('delete_status',1)->count();
			$value->ads_count = $ads_count; 
			$count=FollowUser::where('user_id',$value->following_id)->where('following_id',$value->user_id)->count();

			if ($count>0) 
			{
				$value->followstatus='YES';
			}
			else
			{
				$value->followstatus='NO';
			}
		}

		return response()->json(['data'=>$data,'message'=>'Successfully added','status'=>'success']);
	}

	// My Following
	public function my_followings(Request $r)
	{
		$limit = 10;
		if (isset($r->index)) {
			$offset = $r->index * $limit;

		} else {
			$offset = 0;
		}

		$data=DB::table('follow_users')->join('users','users.id','=','follow_users.following_id')
			->select('follow_users.following_id','follow_users.user_id','users.name',
					'users.profile_image','users.id as signupid')
				->where('follow_users.user_id',$r->user_id)
				->offset($offset)->limit($limit)->orderBy('follow_users.id', 'desc')->get();
		foreach($data as $value)
		{
			$ads_count=Ads::where('user_id',$value->signupid)->where('mobile_verify','true')->where('activation_status',0)->where('block_status',0)->where('delete_status',1)->count();
			$value->ads_count = $ads_count; 
		}

		return response()->json(['data'=>$data,'message'=>'Successfully added','status'=>'success']);
	}


// other Followers
	public function other_followings(Request $r)
	{
		$limit = 10;
		if (isset($r->index)) {
			$offset = $r->index * $limit;

		} else {
			$offset = 0;
		}

		$data=DB::table('follow_users')->join('users','users.id','=','follow_users.following_id')
			->select('follow_users.following_id','follow_users.user_id','users.name',
					'users.profile_image','users.id as signupid')
				->where('follow_users.user_id',$r->user_id)
				->offset($offset)->limit($limit)->orderBy('follow_users.id', 'desc')->get();
		
		foreach ($data as  $value) 
		{
			$count=FollowUser::where('user_id',$r->other_id)->where('following_id',$value->following_id)->count();
			$ads_count=Ads::where('user_id',$value->following_id)->where('mobile_verify','true')->where('activation_status',0)->where('block_status',0)->where('delete_status',1)->count();
			$value->ads_count = $ads_count;

			if ($count>0) 
			{
				$value->followstatus='YES';
			}
			else
			{
				$value->followstatus='NO';
			}
		}

		return response()->json(['data'=>$data,'message'=>'Successfully added','status'=>'success']);
	}


// other Followers
	public function other_followers(Request $r)
	{
		$limit = 10;
		if (isset($r->index)) {
			$offset = $r->index * $limit;

		} else {
			$offset = 0;
		}

		$data=DB::table('follow_users')->join('users','users.id','=','follow_users.user_id')
				->select('follow_users.following_id','follow_users.user_id','users.name',
					'users.profile_image','users.id as signupid')
				->where('follow_users.following_id',$r->user_id)
				->offset($offset)->limit($limit)->orderBy('follow_users.id', 'desc')->get();
		
		foreach ($data as  $value) 
		{
			$count=FollowUser::where('user_id',$r->other_id)->where('following_id',$value->user_id)->count();
			$ads_count=Ads::where('user_id',$value->user_id)->where('mobile_verify','true')->where('activation_status',0)->where('block_status',0)->where('delete_status',1)->count();
			$value->ads_count = $ads_count;
			if ($count>0) 
			{
				$value->followstatus='YES';
			}
			else
			{
				$value->followstatus='NO';
			}
		}

		return response()->json(['data'=>$data,'message'=>'Successfully added','status'=>'success']);
	}


	// My Account
	public function my_account(Request $r)
	{
		$all_ads=Ads::where('user_id',$r->user_id)->where('mobile_verify','true')->get();
		$ads_count=0;
		$delete_count=0;
		if ($all_ads) 
		{
			foreach ($all_ads as $ads) 
			{
				$ads_count++;
				if ($ads->delete_status==0) 
				{
					$delete_count++;
				}
			}
		}

		$data=User::where('id',$r->user_id)->select('total_free_ads','total_paid_ads')->first();

		$total_adss=($data->total_free_ads+$data->total_paid_ads)-$ads_count;
		$data->ads_count=$ads_count;
		$data->total_ads=$total_adss;
		$data->delete_count=$delete_count;
			
		if ($this->check_count($data)>0) 
		{
			return response()->json(['data'=>$data,'message'=>'Successfully added','status'=>'success']);
		}
		else 
		{
			return response()->json(['message'=>'No Data','status'=>'fail']);
		}
	}


	public function get_total_credits(Request $r)
	{
		$sum=mycoins::where('user_id',$r->userid)->where('wallet_type','1')->sum('coins');
		return response()->json(['Credits'=>$sum,'message'=>'Successfully added','status'=>'success']);

	}

	public function upgrade_account(Request $r)
	{
		$milliseconds = round(microtime(true) * 1000);
		$record = new mycoins;
		$record->coins = $r->coins;
		$record->user_id = $r->user_id;
		$record->wallet_type = 1;
//		if(empty($r->tital)) {
//			$record->tital = 'You have upgrade an account';
//		} else {
			$record->tital = 'Credits added through paypal';
//		}
		$record->refer_id = $r->refer_id;
		$record->type = "credit";
		$record->timestamp=$milliseconds;
		$record->save();
        
        $milliseconds = round(microtime(true) * 1000);
        $record = new mycoins;
        $record->coins = -$r->coins;
        $record->user_id = $r->user_id;
        $record->wallet_type = 1;
//        if(empty($r->tital)) {
            $record->tital = 'You have upgrade an account';
//        } else {
//            $record->tital = 'Credit purchased through paypal';
//        }
        $record->refer_id = $r->refer_id;
        $record->type = "upgrade";
        $record->timestamp=$milliseconds;
        $record->save();
        
		if($record->id > 0) {
			$use = User::where('id',$r->user_id)->update(['total_paid_ads' => DB::raw('total_paid_ads+'.$r->total_paid_ads),'plan' => $r->plan ]);
			return response()->json(['message'=>'Successfully added','status'=>true]);
		} else {
			return response()->json(['message'=>'Unable to save','status'=>false]);
		}
	}
	public function buy_credit(Request $r)
	{
		$milliseconds = round(microtime(true) * 1000);
		$record = new mycoins;
		$record->coins = $r->coins;
		$record->user_id = $r->user_id;
		$record->wallet_type = 1;
		if(empty($r->tital)) { 
			$record->tital = 'Credits added through paypal ';
		} else {
			$record->tital = 'Credits added through paypal';
		}
		$record->refer_id = $r->refer_id;
		$record->type = "credit";
		$record->timestamp=$milliseconds;
		$record->save();
		if($record->id > 0) {
			return response()->json(['message'=>'Successfully added','status'=>true]);
		} else {
			return response()->json(['message'=>'Unable to save','status'=>false]);
		}
	}

}
