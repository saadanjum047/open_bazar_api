<?php

namespace App\Http\Controllers\api1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use App\Category;
use App\Ads;
use App\bids;
use App\signups;
use App\adcounts;
use App\mycoins;
use DB;
use App\User;
use App\SubCategory;
use App\Product;
use App\PostReport;
use App\FollowUser;
use App\Notifications;
use App\Block;
use ImageOptimizer;

class post_ads extends Controller
{
	// get category
	public function get_category(Request $r)
	{
		$user_detail = Category::where('main_category', $r->maincategory_id)->where('delete_status', 1)->where('publish', '1')
			->orderBy('position')->get();
		if ($this->check_count($user_detail)) {
			return response()->json(['data' => $user_detail, 'message' => 'Successfully fatched', 'status' => 'success']);
		} else {
			return response()->json(['message' => 'No category exist', 'status' => 'fail']);
		}
	}

	// get subcategory
	public function get_subcategory(Request $r)
	{
		$user_detail = SubCategory::where('publish', '1')->where('category_id', $r->id)->where('delete_status', 1)->orderBy('position')->get();
		if ($this->check_count($user_detail)) {
			return response()->json(['data' => $user_detail, 'message' => 'Successfully fatched', 'status' => 'success']);
		} else {
			return response()->json(['message' => 'No subcategory exist', 'status' => 'fail']);
		}
	}


	// get products of subcategory
	public function get_productcat(Request $r)
	{
		$user_detail = Product::where('publish', '1')->where('subcategory_id', $r->id)->where('delete_status', 0)
			->get();

		if ($this->check_count($user_detail)) {
			foreach ($user_detail as $value) {
				$value->product = json_decode($value->product, true);
			}
			return response()->json(['data' => $user_detail, 'message' => 'Successfully fatched', 'status' => 'success']);
		} else {
			return response()->json(['message' => 'No products exist', 'status' => 'fail']);
		}
	}

	public function get_productcat_new(Request $r)
	{
		$user_detail = Product::where('publish', '1')->where('subcategory_id', $r->id)->where('delete_status', 0)
			->get();

		if ($this->check_count($user_detail)) {
			foreach ($user_detail as $value) {
				$value->product = $value->product;
			}
			return response()->json(['data' => $user_detail, 'message' => 'Successfully fatched', 'status' => 'success']);
		} else {
			return response()->json(['message' => 'No products exist', 'status' => 'fail']);
		}
	}


	// Upload Images
	public function uploadimage(Request $r)
	{
		$files = $r->file('image');
		if ($files != '') {
			$rand = rand(1, 1000000);
			$imageName = $rand . date("ydmish") . '.' . $r->image->getClientOriginalExtension();
			$success = $r->image->move(public_path('images'), $imageName);

			$image_path = public_path('images') . '/' . $imageName;
			$this->compress_image($image_path);

			return response()->json(['image' => $imageName, 'message' => 'Image Uploaded', 'status' => 'success']);
		} else {
			return response()->json(['message' => 'Param Missing', 'status' => 'fail']);
		}
	}


	//follow users
	public function follow_users(Request $r)
	{
		if (!empty($r->user_id) && !empty($r->sender_id)) { } else {
			return response()->json(['message' => 'Params Missing', 'status' => 'fail']);
		}
	}

	//Post Reports
	public function post_report(Request $r)
	{
		if (!empty($r->user_id) && !empty($r->post_id)  && $r->type != "" && !empty($r->reason)) {
			$milliseconds = round(microtime(true) * 1000);
			$insert = new PostReport;
			$insert->user_id = $r->user_id;
			$insert->post_id = $r->post_id;
			$insert->description = $r->description;
			$insert->timestamp = $milliseconds;

			if ($r->type == 0) {
				$insert->type = "user";
			} else {
				$insert->type = "post";
			}

			$insert->reason = $r->reason;
			$save = $insert->save();

			if ($save) {
				return response()->json(['message' => 'Data Inserted', 'status' => 'success', 'data' => $insert]);
			} else {
				return response()->json(['message' => 'Something went wrong', 'status' => 'fail']);
			}
		} else {
			return response()->json(['message' => 'Params Missing', 'status' => 'fail']);
		}
	}


	// Post Ads
	public function insert_ads(Request $r)
	{
		$user = User::where('id', $r->user_id)->select('total_free_ads', 'total_paid_ads')->first();
		$ads_count = Ads::where('user_id', $r->user_id)->where('mobile_verify', 'true')->where('activation_status', 0)->where('block_status', 0)->where('delete_status', 1)->count();
		$total_ads = ($user->total_free_ads + $user->total_paid_ads) - $ads_count;

		if ($total_ads < 1) {
			return response()->json(['message' => 'Total Ad Limit Is Over', 'status' => 'false']);
		} else {
			$mobile_verify = 'false';

			$insert = new Ads;
			$insert->user_id = $r->user_id;
			$insert->ad_title = $r->ad_title;
			$insert->ad_description = $r->ad_description;
			if ($r->country_id != null) {
				$insert->country_id = $r->country_id;
			}

			$insert->mobile = $r->mobile;
			$insert->price = $r->price;
			$insert->price_type = $r->price_type;
			$insert->category_id = $r->category_id;
			$mobile_verify = $r->mobile_verify;
			$insert->expired_date = date(strtotime("+60 days"));  //expirey date 

			if ($r->images != null) {
				$img = json_decode($r->images, true);
				$insert->images = json_encode(array_values(array_filter($img)));
			}

			if ($mobile_verify == 'true') {
				$insert->mobile_verify = $mobile_verify;
			} else {
				if ($ads_count > 0) {
					$mobile_verify = 'true';
					$insert->mobile_verify = $mobile_verify;
				} else {
					$mobile_verify = 'false';
					$insert->mobile_verify = $mobile_verify;
				}
			}

			$insert->category = $r->category;
			$insert->city = $r->city;
			$insert->timestamp = $r->timestamp;
			$insert->main_category = $r->main_category;

			// add shop name
			if ($insert->main_category == '4') {
				$insert->shop_name = $r->shop_name;
			}
			$insert->save();

			$addcoins = new mycoins;
			$addcoins->user_id = $r->user_id;
			$addcoins->tital = 'You have posted an Ad';
			$addcoins->type = 'post';
			$addcoins->refer_id = $insert->id;
			$addcoins->coins = '3';
			$addcoins->timestamp = $r->timestamp;
			$addcoins->save();

			if ($this->check_count($insert) > 0) {
				// Send push notification
				$this->add_posted_push($insert->user_id, $user->name . ' added ' . $insert->ad_title . ' ad to his list', $insert);

				if ($mobile_verify == 'true') {
					return response()->json(['data' => $insert, 'message' => 'Ad Posted', 'status' => 'success']);
				} else {
					return response()->json(['data' => $insert, 'message' => 'Ad posted but number no verify', 'status' => 'success']);
				}
			} else {
				return response()->json(['message' => 'Not inserted', 'status' => 'fail']);
			}
		}
	}




	//Update mobileverification
	public function update_mobileverification(Request $r)
	{
		$milliseconds = round(microtime(true) * 1000);

		// update ads
		$data = Ads::where('id', $r->id)->where('delete_status', 1)->first();
		$data->mobile_verify = 'true';
		$data->update();

		// add coin
		$addcoins = new mycoins;
		$addcoins->user_id = $data->user_id;
		$addcoins->tital = 'You have posted an Ad';
		$addcoins->type = 'post';
		$addcoins->refer_id = $data->id;
		$addcoins->coins = '3';
		$addcoins->timestamp = $milliseconds;
		$addcoins->save();

		return response()->json(['data' => $data, 'message' => 'Verifyed', 'status' => 'success']);
	}


	// Delete Ads
	public function delete_ads(Request $r)
	{
		if (!empty($r->id) &&  !empty($r->user_id)) {
			$data = Ads::where('id', $r->id)->first();
			if ($data) {
				$data->delete_status = 0;
				$data->save();
				// $this->delete_coin($r->id,$r->user_id,'post');
				return response()->json(['status' => 'success']);
			} else {
				return response()->json(['status' => 'fail']);
			}
		} else {
			return response()->json(['message' => 'Param Missing', 'status' => 'fail']);
		}
	}


	// Update Post Ads
	public function update_ads(Request $r)
	{
		$insert = Ads::where('id', $r->id)->where('delete_status', 1)->first();
		$insert->ad_title = $r->ad_title;
		$insert->ad_description = $r->ad_description;
		$insert->mobile = $r->mobile;
		$insert->price = $r->price;
		if ($r->country_id != null) {
			$insert->country_id = $r->country_id;
		}
		$insert->price_type = $r->price_type;
		$insert->category = $r->category;

		if ($r->images != null) {
			$insert->images = $r->images;
		}

		$insert->category = $r->category;
		$insert->hide_mobileno = $r->hide_mobileno;
		$insert->city = $r->city;

		if ($insert->main_category == 4) {
			$insert->shop_name = $r->shop_name;
		}
		$insert->update();

		if ($this->check_count($insert) > 0) {
			return response()->json(['data' => $insert, 'message' => 'updated', 'status' => 'success']);
		} else {
			return response()->json(['message' => 'Not inserted', 'status' => 'fail']);
		}
	}



	// Get adposts
	public function get_adposts1(Request $r)
	{
		$check = User::where('id', $r->user_id)->where('delete_status', 1)->first();
		if ($check) {
			if ($check->block_status == 1) {
				$limit = 10;
				if (isset($r->index)) {
					$offset = $r->index * $limit;
				} else {
					$offset = 0;
				}

				$black_list = Block::where('user_id', $r->user_id)->orWhere('block_id', $r->block_id)->get();
				$blocked_to = array();
				$blocked_by = array();
				foreach ($black_list as $black_user) {
					if ($black_user->user_id == $r->user_id) {
						$blocked_to[] = $black_user->block_id;
					} else {
						$blocked_by[] = $black_user->user_id;
					}
				}

				$data = Ads::whereNotIn('user_id', $blocked_by)->whereNotIn('user_id', $blocked_to)->where('block_status', '0')->where('activation_status', '0')->where('delete_status', 1)->where('mobile_verify', 'true');

				if (!empty($r->location) && !empty($r->cat_id)) {
					$data->where('main_category', $r->type)->where('category_id', $r->cat_id)->where('city', $r->location);
				} elseif (!empty($r->cat_id)) {
					$data->where('main_category', $r->type)->where('category_id', $r->cat_id);
				} elseif (!empty($r->location)) {
					$data->where('main_category', $r->type)->where('city', $r->location);
				} else if ($r->type != "all") {
					$data->where('main_category', $r->type);
				}
				$data = $data->offset($offset)->limit($limit)->orderBy('id', 'desc')->get();


				foreach ($data as $d) {
					// follow user -------------
					$follow_count = FollowUser::where('user_id', $r->user_id)->where('following_id', $d->user_id)->count();
					$follow_user = 'false';
					if ($follow_count > 0) {
						$follow_user = 'true';
					}
					$d->follow_user = $follow_user;

					// follow userr end----------
					if ($check) {
						$d->user_name = $check->name;
						$d->user_image = $check->profile_image;
						$d->user_regiterdate = $check->created_at;
						$d->ads_count = Ads::where('user_id', $r->user_id)->where('mobile_verify', 'true')->where('activation_status', 0)->where('block_status', 0)
							->where('delete_status', 1)->count();
					}
					$d->fav_stattus = 'No';
					$d->like_stattus = 'No';

					$like_datas = DB::table('adcounts')->where('post_id', $d->id)->select('user_id', 'favorite', 'likes', 'views', DB::raw('SUM(favorite) as favorite_count'), DB::raw('SUM(views) as views_count'), DB::raw('SUM(likes) as like_count'))->groupBy('id')->get();
					$favorite_count = 0;
					$views_count = 0;
					$like_count = 0;
					$fav_stattus = 'No';
					$like_stattus = 'No';

					foreach ($like_datas as $like_data) {
						if ($like_data->user_id == $r->user_id) {
							if ($like_data->favorite == 1) {
								$fav_stattus = 'Yes';
							}
							if ($like_data->likes == 1) {
								$like_stattus = 'Yes';
							}
						}

						$d->favorite_count = $like_data->favorite_count;
						$d->views_count = $like_data->views_count;
						$d->like_count = $like_data->like_count;
						$d->fav_stattus = $fav_stattus;
						$d->like_stattus = $like_stattus;
					}
				}

				$noty = Notifications::where('user_id', $r->user_id)->where('status', 0)->count();

				if ($this->check_count($data) > 0) {
					return response()->json(['data' => $data, 'noty' => $noty, 'message' => 'Successfully', 'status' => 'success']);
				} else {
					return response()->json(['message' => 'No data found', 'status' => 'fail']);
				}
			} else {
				return response()->json(['message' => 'You are blocked by admin.Please contact to admin.', 'status' => 'false']);
			}
		} else {
			return response()->json(['message' => 'invalid user', 'status' => 'false']);
		}
	}


	// get ad post details
	public function getpost_detail(Request $r)
	{
		$data = Ads::where('id', $r->id)->where('block_status', '0')->where('activation_status', '0')->where('delete_status', 1)->where('mobile_verify', 'true')->get();
		foreach ($data as $d) {
			$user =	User::where('id', $d->user_id)->where('delete_status', 1)->first();


			// follow user -------------
			$follow_count = FollowUser::where('user_id', $r->user_id)->where('following_id', $d->user_id)->count();
			$follow_user = 'false';
			if ($follow_count > 0) {
				$follow_user = 'true';
			}
			$d->follow_user = $follow_user;


			if ($user) {
				$d->user_name = $user->name;
				$d->user_image = $user->profile_image;

				$d->user_regiterdate = date('Y-m-d H:i:s', strtotime($user->created_at));
				$d->ads_count = Ads::where('user_id', $d->user_id)->where('mobile_verify', 'true')->where('activation_status', 0)->where('block_status', 0)
					->where('delete_status', 1)->count();
			}


			$like_datas = DB::table('adcounts')->where('post_id', $d->id)->get();
			$favorite_count = 0;
			$views_count = 0;
			$like_count = 0;
			$fav_stattus = 'No';
			$like_stattus = 'No';

			foreach ($like_datas as $like_data) {
				if ($like_data->favorite == 1) {
					$favorite_count++;
				}
				if ($like_data->views == 1) {
					$views_count++;
				}
				if ($like_data->likes == 1) {
					$like_count++;
				}

				if ($like_data->user_id == $r->user_id) {
					if ($like_data->favorite == 1) {
						$fav_stattus = 'Yes';
					}
					if ($like_data->likes == 1) {
						$like_stattus = 'Yes';
					}
				}

				$d->favorite_count = $favorite_count;
				$d->views_count = $views_count;
				$d->like_count = $like_count;
				$d->fav_stattus = $fav_stattus;
				$d->like_stattus = $like_stattus;
			}

			// $like_datas=DB::table('adcounts')->where('post_id',$d->id)->select('user_id','favorite','likes','views', DB::raw('SUM(favorite) as favorite_count'), DB::raw('SUM(views) as views_count'), DB::raw('SUM(likes) as like_count'))->groupBy('id')->get();
			// $fav_stattus='No';
			// $like_stattus='No';

			// foreach ($like_datas as $like_data) 
			// {
			// 	if ($like_data->user_id==$r->user_id) 
			// 	{
			// 		if ($like_data->favorite==1) 
			// 		{						
			// 			$fav_stattus='Yes';
			// 		}
			// 		if ($like_data->likes==1) 
			// 		{						
			// 			$like_stattus='Yes';
			// 		}
			// 	}

			// 	$d->favorite_count=$like_data->favorite_count;
			// 	$d->views_count=$like_data->views_count;
			// 	$d->like_count=$like_data->like_count;
			// 	$d->fav_stattus=$fav_stattus;
			// 	$d->like_stattus=$like_stattus;
			// }
		}

		if ($this->check_count($data) > 0) {
			return response()->json(['data' => $data, 'message' => 'Successfully', 'status' => 'success']);
		} else {
			return response()->json(['message' => 'No data found', 'status' => 'fail']);
		}
	}


	// Ad View
	public function ad_view(Request $r)
	{
		switch ($r->type) {
			case 'view':
				$data = adcounts::where('post_id', $r->post_id)->where('user_id', $r->user_id)->first();
				if ($this->check_count($data) > 0) {
					if ($data->views != '1') {
						$data->views = '1';
						$data->update();
						return response()->json(['data' => $data, 'status' => 'success']);
					} else {
						return response()->json(['data' => $data, 'status' => 'success']);
					}
				} else {
					$data = new adcounts;
					$data->post_id = $r->post_id;
					$data->user_id = $r->user_id;
					$data->views = '1';
					$data->save();

					return response()->json(['data' => $data, 'status' => 'fail']);
				}
				break;

			case 'favorite':
				$data = adcounts::where('post_id', $r->post_id)->where('user_id', $r->user_id)->first();
				if ($this->check_count($data) > 0) {
					if ($data->favorite == '1') {
						$data->favorite = '0';
						$data->update();
						return response()->json(['data' => $data, 'status' => 'success']);
					} else {
						$data->favorite = '1';
						$data->update();

						// Send push notification
						$adsdetail = Ads::where('id', $data->post_id)->where('delete_status', 1)->first();
						$user_detail = User::where('id', $data->user_id)->where('delete_status', 1)->first();

						//add notification  
						$this->add_notification(
							$adsdetail->user_id,
							$data->post_id,
							$data->user_id,
							$user_detail->name . ' added ' . $adsdetail->ad_title . ' this ad to his favourites',
							'4'
						);

						$this->sendPush($adsdetail->user_id, $user_detail->name . ' added ' . $adsdetail->ad_title . ' ad to his favourites', $adsdetail);

						return response()->json(['data' => $data, 'status' => 'success']);
					}
				} else {
					$data = new adcounts;
					$data->post_id = $r->post_id;
					$data->user_id = $r->user_id;
					$data->favorite = '1';
					$data->save();

					// Send push notification
					$adsdetail =	Ads::where('id', $r->post_id)->where('delete_status', 1)->first();
					$user_detail = User::where('id', $r->user_id)->where('delete_status', 1)->first();

					//add notification  
					$this->add_notification($adsdetail->user_id, $r->post_id, $r->user_id, $user_detail->name . ' added ' . $adsdetail->ad_title . ' this ad to his favourites', '4');
					$this->sendPush($adsdetail->user_id, $user_detail->name . ' added ' . $adsdetail->ad_title . ' this ad to his favourites', $adsdetail);

					return response()->json(['data' => $data, 'status' => 'fail']);
				}
				break;

			case 'likes':
				$data = adcounts::where('post_id', $r->post_id)->where('user_id', $r->user_id)->first();
				if ($this->check_count($data) > 0) {
					if ($data->likes == '1') {
						// $data->likes='0';
						// $data->update();

						// $this->delete_coin($r->post_id,$r->user_id,'like');
						// $this->delete_notification($r->user_id,$r->post_id,'1');
						return response()->json(['data' => $data, 'status' => 'success']);
					} else {
						$data->likes = '1';
						$data->update();

						// add coine
						$this->add_coinv1('You have liked an Ad', '1', $r->post_id, $r->user_id, 'like', '0');

						// Send push notification
						$adsdetail =	Ads::where('id', $r->post_id)->where('delete_status', 1)->first();
						$user_detail = User::where('id', $r->user_id)->where('delete_status', 1)->first();

						//add notification 
						if ($this->check_count($adsdetail)) {
							$this->add_notification($adsdetail->user_id, $r->post_id, $r->user_id, $user_detail->name . ' like your ' . $adsdetail->ad_title . ' Ad', '1');
							$this->sendPush($adsdetail->user_id, $user_detail->name . ' like your ' . $adsdetail->ad_title . ' Ad', $adsdetail);
						}
						return response()->json(['data' => $data, 'status' => 'success']);
					}
				} else {
					$data = new adcounts;
					$data->post_id = $r->post_id;
					$data->user_id = $r->user_id;
					$data->likes = '1';
					$data->save();

					$this->add_coinv1('You have liked an Ad', '1', $r->post_id, $r->user_id, 'like', '0');


					// Send push notification
					$adsdetail =	Ads::where('id', $r->post_id)->where('delete_status', 1)->first();
					$user_detail = User::where('id', $r->user_id)->where('delete_status', 1)->first();

					//add notification  
					$this->add_notification($adsdetail->user_id, $r->post_id, $r->user_id, $user_detail->name . ' like your ' . $adsdetail->ad_title . ' Ad', '1');
					$this->sendPush($adsdetail->user_id, $user_detail->name . ' like your ' . $adsdetail->ad_title . ' Ad', $adsdetail);

					return response()->json(['data' => $data, 'status' => 'fail']);
				}
				break;

			default:
				break;
		}
	}


	// offer bid
	public function offer_bid(Request $r)
	{
		if (!empty($r->post_id) && !empty($r->bid_price) && !empty($r->timestamp) && !empty($r->user_id)) {
			$insert = new bids;
			$insert->user_id = $r->user_id;
			$insert->post_id = $r->post_id;
			$insert->bid_price = $r->bid_price;
			$insert->bid_type = $r->bid_type;
			$insert->timestamp = $r->timestamp;
			$insert->save();

			if ($this->check_count($insert) > 0) {
				$adsdetail =	Ads::where('id', $insert->post_id)->first();
				$user_detail = User::where('id', $insert->user_id)->first();

				Ads::where('id', $r->post_id)->increment('offer_count');
				if ($insert->bid_type == '0') {
					//add notification  
					$this->add_notification(
						$adsdetail->user_id,
						$insert->post_id,
						$insert->user_id,
						$user_detail->name . ' offer €' . $insert->bid_price . ' for your ' . $adsdetail->ad_title . ' ad',
						'2'
					);
					$this->sendPush($adsdetail->user_id, $user_detail->name . ' offer €' . $insert->bid_price . ' for your ' . $adsdetail->ad_title . ' ad', $adsdetail);
				} else {
					//add notification  
					$this->add_notification(
						$adsdetail->user_id,
						$insert->post_id,
						$insert->user_id,
						$user_detail->name . ' comment on your ' . $adsdetail->ad_title . ' ad',
						'3'
					);
					$this->sendPush($adsdetail->user_id, $user_detail->name . ' comment on your ' . $adsdetail->ad_title . ' ad', $adsdetail);
				}
				return response()->json(['data' => $insert, 'message' => 'Successfully added', 'status' => 'success']);
			} else {
				return response()->json(['message' => 'Not added', 'status' => 'fail']);
			}
		} else {
			return response()->json(['message' => 'Param Missing', 'status' => 'fail']);
		}
	}


	// get offer bid
	public function get_offer_bid(Request $r)
	{
		if (!empty($r->post_id)) {
			$limit = 10;
			if (isset($r->index)) {
				$offset = $r->index * $limit;
			} else {
				$offset = 0;
			}

			$data = bids::where('post_id', $r->post_id)->where('bid_type', $r->bid_type)->offset($offset)->limit($limit)->orderBy('id', 'desc')->get();
			foreach ($data as $value) {
				$user = User::where('id', $value->user_id)->where('delete_status', 1)->first();
				if ($user) {
					$value->user_name = $user->name;
					$value->user_image = $user->profile_image;
				}
			}

			if ($this->check_count($data) > 0) {
				return response()->json(['data' => $data, 'message' => 'Successfully fetch', 'status' => 'success']);
			} else {
				return response()->json(['message' => 'Not data', 'status' => 'fail']);
			}
		} else {
			return response()->json(['message' => 'Param Missing', 'status' => 'fail']);
		}
	}

	// Delete Offers	
	public function delete_offers(Request $r)
	{
		if (!empty($r->id) && !empty($r->post_id)) {
			$data = bids::where('id', $r->id)->delete();
			if ($data) {
				Notifications::where('bid_id', $r->id)->delete();
				Ads::where('id', $r->post_id)->decrement('offer_count');
				return response()->json(['status' => 'success']);
			} else {
				return response()->json(['status' => 'fail']);
			}
		} else {
			return response()->json(['message' => 'Param Missing', 'status' => 'fail']);
		}
	}

	public function delete_coin($refer_id, $user_id, $type)
	{
		// add coin
		mycoins::where('refer_id', $refer_id)->where('user_id', $user_id)->where('type', $type)->delete();
	}


	// fetech notification
	public function fetch_notification(Request $r)
	{
		if (!empty($r->user_id)) {
			$check = User::where('id', $r->user_id)->where('delete_status', 1)->first();
			if ($check) {
				if ($check->block_status == 1) {
					$limit = 20;
					if (isset($r->index)) {
						$offset = $r->index * $limit;
					} else {
						$offset = 0;
					}

					$notification = Notifications::where('user_id', $r->user_id)->offset($offset)->limit($limit)->orderBy('id', 'desc')->get();
					if ($this->check_count($notification)) {
						$val_array =  array();
						foreach ($notification as  $value) {
							$user_detail = User::where('id', $value->callby)->select('name', 'profile_image')->first();
							$value->sender_name = $user_detail->name;
							$value->sender_image = $user_detail->profile_image;
							$ads  = Ads::Select('ad_title')->where('id', $value->refer_id)->where('delete_status', 1)->where('block_status', 0)->first();
							$bid = bids::where('post_id', $value->refer_id)->where('user_id', $value->callby)->first();

							if ($ads != null) {
								if ($value->type == '1') {
									$value->tital = $user_detail->name . ' liked Your Ad ' . $ads->ad_title;
									$val_array[] = $value;
								} elseif ($value->type == '3') {
									$value->tital = $user_detail->name . ' comment on Your Ad ' . $ads->ad_title;
									$val_array[] = $value;
								} elseif ($value->type == '4') {
									$value->tital = $user_detail->name . ' added Your Ad ' . $ads->ad_title . ' to Favourites';
									$val_array[] = $value;
								} else if ($value->type == '2') {
									if ($bid != null) {
										$value->tital = $user_detail->name . '  offer € ' . $bid->bid_price . ' for your ' . $ads->ad_title . ' Ad';
										$val_array[] = $value;
									}
								}
							}
						}

						$deleted_ads = array();
						$user_adcount = Ads::where('user_id', $r->user_id)->get();
						if ($this->check_count($user_adcount) > 0) {
							foreach ($user_adcount as $adcount_value) {


								if ($adcount_value->delete_status == 0) {
									$deleted_ads[] = $adcount_value->id;
								}
							}
						}
						// get blocked notifications
						$blocked = User::where('delete_status', 0)->pluck('id');
						$noty = Notifications::where('user_id', $r->user_id)->whereNotIn('callby', $blocked)
							->whereNotIn('refer_id', $deleted_ads)->where('status', 0)->count();

						if ($this->check_count($val_array)) {
							return response()->json(['data' => $val_array, 'noty' => $noty, 'message' => 'data fetch', 'status' => 'success']);
						} else {
							return response()->json(['message' => 'No data', 'status' => 'fail']);
						}
					} else {
						return response()->json(['message' => 'No data', 'status' => 'fail']);
					}
				} else {
					return response()->json(['message' => 'You are blocked by admin. Please contact admin.', 'status' => 'fail']);
				}
			} else {
				return response()->json(['message' => 'invalid user', 'status' => 'false']);
			}
		} else {
			return response()->json(['message' => 'Param Missing', 'status' => 'fail']);
		}
	}

	// Get similar_ads
	public function similar_ads(Request $r)
	{
		if (!empty($r->category_id)) {
			//            $data=Ads::where('block_status','0')->where('activation_status','0')->where('mobile_verify','true')->where('delete_status',1)->where('user_id',$r->id)->where('category_id',$r->category_id)->limit('7')->orderBy('id', 'desc')->get();
//			$u = User::where('id', $r->id)->first();
//			$u_country = $u->user_country;
//
//            $data = Ads::where('block_status', 0)->where('activation_status', 0)->where('country_id', $u_country)->where('mobile_verify', 'true')->where('delete_status', 1)->where('category_id', $r->category_id)->limit('7')->orderBy('id', 'desc')->get();

			$data = Ads::where('block_status', 0)->where('activation_status', 0)->where('mobile_verify', 'true')->where('delete_status', 1)->where('category_id', $r->category_id)->limit('7')->orderBy('id', 'desc')->get();

			//            $latestPosts = DB::table('users','id')
			//                               ->select('user_country')
			//                               ->where('id', $r->id)
			//                               ;
			//
			//            $users = DB::table('ads')
			//                    ->joinSub($latestPosts, 'latest_posts', function ($join) {
			//                        $join->on('ads.user_id', '=', 'latest_posts.id')->where('ads.country_id','latest_posts.user_country');
			//                    })->where('block_status','0')->where('activation_status','0')->where('mobile_verify','true')->where('delete_status',1)->where('category_id',$r->category_id)->limit('7')->orderBy('id', 'desc')->get();


			foreach ($data as $d) {
				$user =	User::where('id', $d->user_id)->where('delete_status', 1)->first();
				// follow user -------------
				$follow_count = FollowUser::where('user_id', $r->user_id)->where('following_id', $d->user_id)->count();
				$follow_user = 'false';
				if ($follow_count > 0) {
					$follow_user = 'true';
				}
				$d->follow_user = $follow_user;

				if ($user) {
					$d->user_name = $user->name;
					$d->user_image = $user->profile_image;
					$d->user_regiterdate = date('Y-m-d H:i:s', strtotime($user->created_at));
					$d->ads_count = Ads::where('user_id', $d->user_id)->where('mobile_verify', 'true')->where('activation_status', 0)->where('block_status', 0)
						->where('delete_status', 1)->count();
				}
				$d->favorite_count = 0;
				$d->views_count = 0;
				$d->like_count = 0;
				$d->fav_stattus = 'No';
				$d->like_stattus = 'No';

				$like_datas = DB::table('adcounts')->where('post_id', $d->id)->get();
				$favorite_count = 0;
				$views_count = 0;
				$like_count = 0;
				$fav_stattus = 'No';
				$like_stattus = 'No';

				foreach ($like_datas as $like_data) {
					if ($like_data->favorite == 1) {
						$favorite_count++;
					}
					if ($like_data->views == 1) {
						$views_count++;
					}
					if ($like_data->likes == 1) {
						$like_count++;
					}

					if ($like_data->user_id == $r->user_id) {
						if ($like_data->favorite == 1) {
							$fav_stattus = 'Yes';
						}
						if ($like_data->likes == 1) {
							$like_stattus = 'Yes';
						}
					}

					$d->favorite_count = $favorite_count;
					$d->views_count = $views_count;
					$d->like_count = $like_count;
					$d->fav_stattus = $fav_stattus;
					$d->like_stattus = $like_stattus;
				}
			}

			if ($this->check_count($data) > 0) {
				return response()->json(['data' => $data, 'message' => 'Successfully', 'status' => 'success']);
			} else {
				return response()->json(['message' => 'No data found', 'status' => 'fail']);
			}
		} else {
			return response()->json(['message' => 'Param Missing', 'status' => 'fail']);
		}
	}

	// Chat Push
	public function chat_push(Request $r)
	{
		if (!empty($r->id)) {

			$this->sendPush_chat($r->id, $r->massage, $r->data);
			return response()->json(['message' => 'Push Sent', 'status' => 'success']);
		} else {
			return response()->json(['message' => 'Param Missing', 'status' => 'fail']);
		}
	}
}
