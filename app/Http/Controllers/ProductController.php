<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Auth;
use App\Category;
use App\SubCategory;
use App\Product;

class ProductController extends Controller
{
    public function add_product()
    {
        $data = Category::where('publish',1)->where('delete_status',1)->get();
        // $subcategory = SubCategory::all();
        return View::make('product.add-product',compact('data'));
    }

    public function edit_product($id)
    {
        $ids = decrypt($id);
        $data = Product::where('id',$ids)->where('delete_status',0)->first();
        if($data){
            $pro = json_decode($data->product,true);
            $prodd = json_decode($data->product_dd,true);
            $product1 =  $product2 = array();
            foreach($pro as $val => $value)
            {                
                if($val!="label" && $val!="image"){
                    $data->size = sizeof($value);
                    $data->productname = $val;
                    foreach($value as $vs1=> $v1){
                        $prod1 = array();
                        $prod1['label'] = $vs1;
                        $prod1['variant'] = implode(",",$v1);
                        $product1[] = $prod1;
                    }
                }elseif($val=="label"){
                    $data->labelname = $value;
                }else{
                    $data->image = $value;
                }
            }
            foreach($prodd as $valdd => $valuedd)
            {                
                if($valdd!="label" && $valdd!="image"){
                    $data->productddname = $valdd;
                    foreach($valuedd as $vs=> $v){
                        $prod = array();
                        $prod['label'] = $vs;
                        $prod['variant'] = implode(",",$v);
                        $product2[] = $prod;
                    }
                }elseif($valdd=="label"){
                    $data->labelddname = $valuedd;
                }
            }
            
            $onearr= array_merge($product1,$product2);
            $one=array();
            foreach ( $onearr as $z => $z1 ){
                $one[ $z+1 ] = $z1;
            }
            $data->variant = $one;
            // dd($data->variant);
            $category = Category::where('main_category',$data->main_category)->where('delete_status',1)->get();
            $sub = SubCategory::where('category_id',$data->category_id)->where('delete_status',1)->get();
            return View::make('product.edit-product',compact('data','category','sub'));
        }else{
            return back()->with('errror','Something went Wrong!!..');
        }
    }

    public function _editproduct(Request $req)
    {
        $pro = $prodd = array();
        $var = $vardd =array();
        $count=sizeof($req->all());
        for($i=$req->inc; $i>=0; $i--)
        {
            if($i==0){
                $pro['label']= $req['label_'.$i];
                $prodd['label'] = $req['labeldd_'.$i];

                if ($req->hasFile('image')) {
                    $image = $req->image;
                    $rand = rand(1, 1000000);
                    $destinationPath = public_path().'/images';
                    // Get the orginal filname or create the filename of your choice
                    $filename = $rand.date('ydmish').'.'.$image->getClientOriginalExtension();
                    $upload = $image->move($destinationPath, $filename);
                    $pro['image'] = $filename;
                    $prodd['image'] = $filename;
                }else{
                    $pro['image'] = $req->oldimage;
                    $prodd['image'] = $req->oldimage;
                }

                $pro[''.$req['variant_'.$i] ]= array_reverse($var);
                $prodd[''.$req['variantdd_'.$i] ]= array_reverse($vardd);
            }else{
                if($req['label_'.$i] != "" && $req['variant_'.$i] != "" && $req['labeldd_'.$i] !="" && $req['variantdd_'.$i] != ""){
                    $var[''.$req['label_'.$i] ]= explode(",",$req['variant_'.$i]);
                    $vardd[''.$req['labeldd_'.$i] ]= explode(",",$req['variantdd_'.$i]);
                }else{
                    continue;
                }
                
            }
        }
        
        $check = Product::where('id',$req->id)->where('delete_status',0)->first();
        // ->where('category_id',$req->category)->where('subcategory_id',$req->subcategory)
        if($check)
        {
            $check->main_category = $req->main_category;
            $check->category_id = $req->category;
            $check->subcategory_id = $req->subcategory;
            $check->product = json_encode($pro);
            $check->product_dd = json_encode($prodd);
            $check->publish = 0;
            $save = $check->save();
            if($save)
            {
                $subcate = SubCategory::where('id',$req->subcategory)->where('delete_status',1)->first();
                if($subcate)
                {
                    if($subcate->next_status == 0)
                    {
                        $subcate->next_status = 1;
                        $subcate->save();
                    }
                }
                return redirect()->route('view-product')->with('status','Product Updated Successfully!!..');
            }
            else{
                return back()->with('errror','Something went Wrong!!..');
            }
                // $product = json_decode($check->product,true);
                // $product_dd = json_decode($check->product_dd,true);
                // foreach($product as $p=>$per){
                //     for($i=$req->inc; $i>=0; $i--)
                //     {
                //         if($p == $req['variant_'.$i]){
                //             $pstatus = 0;
                //             $j = $i+1;
                //             foreach($per as $v=> $value){
                //                 for($j= $i+1; $j<=$req->inc; $j++){
                //                     if($v == $req['label_'.$j]){
                //                         $product[$p][$v] = array_values(array_unique(array_merge($product[$p][$v],$pro[$p][$v])));
                //                         $product_dd[$p][$v] = array_values(array_unique(array_merge($product_dd[$p][$v],$prodd[$p][$v])));
                //                     }else{
                //                         $product[$p][''.$req['label_'.$j]]=$pro[$p][''.$req['label_'.$j]];
                //                         $product_dd[$p][''.$req['labeldd_'.$j]]=$prodd[$p][''.$req['labeldd_'.$j]];
                //                     }
                //                 }
                //             }
                //         }else{
                //             $pstatus = 1;
                //         }
                //     }
                // }
                // dd($product);
                // dd($pro);
                // if($pstatus ==0){
                //     $check->product = json_encode($product);
                //     $check->product_dd = json_encode($product_dd);
                //     $save = $check->save();
                // }else{
                //     $check->product = json_encode($pro);
                //     $check->product_dd = json_encode($prodd);
                //     $save = $check->save();
                // }
                
            // if($save)
            // {
            //     return redirect()->route('view-product')->with('status','Product Updated Successfully!!..');
            // }else{
            //     return back()->with('errror','Something went Wrong!!..');
            // }
        }
        else{
            return back()->with('errror','Something went Wrong!!..');
        }
    }

    public function get_category(Request $req)
    {
        $check = Category::where('main_category',$req->id)->where('publish',1)->where('delete_status',1)->get();
        if(!$check->isEmpty())
        {
            return response()->json(['message'=>'success','status'=>'true','data'=>$check]);
        }
        else{
            return response()->json(['message'=>'errror','status'=>'false']);
        }
    }

    public function get_subcategory(Request $req)
    {
        $check = SubCategory::where('category_id',$req->id)->where('publish',1)->where('delete_status',1)->get();
        if(!$check->isEmpty())
        {
            return response()->json(['message'=>'success','status'=>'true','data'=>$check]);
        }
        else{
            return response()->json(['message'=>'errror','status'=>'false']);
        }
    }



    public function addproduct(Request $req)
    {
        $pro = $prodd = array();
        $var = $vardd =array();
        $count=sizeof($req->all());
        for($i=$req->inc; $i>=0; $i--)
        {
            if($i==0){
                $pro['label']= $req['label_'.$i];
                $prodd['label'] = $req['labeldd_'.$i];

                if ($req->hasFile('image')) {
                    $image = $req->image;
                    $rand = rand(1, 1000000);
                    $destinationPath = public_path().'/images';
                    // Get the orginal filname or create the filename of your choice
                    $filename = $rand.date('ydmish').'.'.$image->getClientOriginalExtension();
                    $upload = $image->move($destinationPath, $filename);
                    $pro['image'] = $filename;
                    $prodd['image'] = $filename;
                }else{
                    $pro['image'] = 'default.png';
                    $prodd['image'] = 'default.png';
                }

                $pro[''.$req['variant_'.$i] ]= array_reverse($var);
                $prodd[''.$req['variantdd_'.$i] ]= array_reverse($vardd);
            }else{
                $var[''.$req['label_'.$i] ]= explode(",",$req['variant_'.$i]);
                $vardd[''.$req['labeldd_'.$i] ]= explode(",",$req['variantdd_'.$i]);
            }
        }
        $check = Product::where('category_id',$req->category)->where('subcategory_id',$req->subcategory)->where('publish',0)->where('delete_status',0)->get();
        if(!$check->isEmpty())
        {
            $pstatus = array();
            foreach($check as $key)
            {
                $product = json_decode($key->product,true);
                $product_dd = json_decode($key->product_dd,true);
                foreach($product as $p=>$per){
                    for($i=$req->inc; $i>=0; $i--)
                    {
                        if($p == $req['variant_'.$i]){
                            $pstatus[] = 0;
                            $j = $i+1;
                            foreach($per as $v=> $value){
                                for($j= $i+1; $j<=$req->inc; $j++){
                                    if($v == $req['label_'.$j]){
                                        $product[$p][$v] = array_values(array_unique(array_merge($product[$p][$v],$pro[$p][$v])));
                                        $product_dd[$p][$v] = array_values(array_unique(array_merge($product_dd[$p][$v],$prodd[$p][$v])));
                                    }else{
                                        $product[$p][''.$req['label_'.$j]]=$pro[$p][''.$req['label_'.$j]];
                                        $product_dd[$p][''.$req['labeldd_'.$j]]=$prodd[$p][''.$req['labeldd_'.$j]];
                                    }
                                }
                            }
                        }else{
                            $pstatus[] = 1;
                        }
                    }
                }
                $key->product = json_encode($product);
                $key->product_dd = json_encode($product_dd);
                $key->save();
            }

            if(!in_array(0,$pstatus))
            {
                goto label;
            }else{
                return redirect()->route('view-product')->with('status','Product added Successfully!!..');
            }
        }else{
            label:
            $insert = new Product;
            $insert->main_category = $req->main_category;
            $insert->category_id = $req->category;
            $insert->subcategory_id = $req->subcategory;
            $insert->product = json_encode($pro);
            $insert->product_dd = json_encode($prodd);
            $insert->publish = 0;
            $save = $insert->save();
            if($save)
            {
                $subcate = SubCategory::where('id',$req->subcategory)->where('delete_status',1)->first();
                if($subcate)
                {
                    $subcate->next_status = 1;
                    $subcate->save();
                }
                    return redirect()->route('view-product')->with('status','Product added Successfully!!..');
            }
            else{
                return back()->with('errror','Something went Wrong!!..');
            }
        }

    }

    public function view_product()
    {
        $data = Product::where('delete_status',0)->orderBy('id','desc')->get();
        foreach($data as $key)
        {
            $product = json_decode($key->product,true);
            $productdd = json_decode($key->product_dd,true);
            foreach($product as $val => $value)
            {                
                if($val!="label" && $val!="image"){
                    $key->productname = $val;
                    $key->variant = $value;
                }else{
                    $key->image = $value;
                }
            }
            foreach($productdd as $valdd => $valuedd)
            {                
                if($valdd!="label" && $valdd!="image"){
                    $key->productddname = $valdd;
                    $key->variantdd = $valuedd;
                }
            }
            
            $category = Category::where('id',$key->category_id)->where('delete_status',1)->first();
            if($category)
            {
                $key->category_name = $category->en_name;
            }
            $subcategory = SubCategory::where('id',$key->subcategory_id)->where('delete_status',1)->first();
            if($subcategory)
            {
                $key->subcategory_name = $subcategory->en_name;
            }
        }
        return View::make('product.view-product',compact('data'));
    }   

    public function view_product_details($id)
    {
        $ids = decrypt($id);

        $data = Product::where('id',$ids)->where('delete_status',0)->first();
        $product = json_decode($data->product,true);
        $productdd = json_decode($data->product_dd,true);
        foreach($product as $val => $value)
        {                
            if($val!="label" && $val!="image"){
                $data->productname = $val;
                $data->variant = $value;
            }else{
                $data->image = $value;
            }
        }
        foreach($productdd as $valdd => $valuedd)
        {                
            if($valdd!="label" && $valdd!="image"){
                $data->productddname = $valdd;
                $data->variantdd = $valuedd;
            }
        }
        
        $category = Category::where('id',$data->category_id)->where('delete_status',1)->first();
        if($category){
            $data->category_name = $category->en_name;
        }
        $subcategory = SubCategory::where('id',$data->subcategory_id)->where('delete_status',1)->first();
        if($subcategory){
            $data->subcategory_name = $subcategory->en_name;
        }
        return View::make('product.view-product-details',compact('data'));
    }

    public function delete_product($id)
    {
        $check = Product::where('id',$id)->where('delete_status',0)->first();
        if($check)
        {
            $check->delete_status = 1;
            $delete = $check->save();
            // $delete = $check->delete();
            if($delete)
            {
                return redirect()->route('view-product')->with('status','Product deleted successfully!!..');
            }
            else{
                return back()->with('errror','Something went wrong!!..');
            }
        }
        else{
            return back()->with('warning','Product not found!!..');
        }
    }

    public function status_product(Request $req)
    {
        $data = Product::where('id',$req->id)->where('delete_status',0)->first();
        if($data)
        {
            if($data->publish==0)
            {
                $data->publish = 1;
                $save = $data->save();
            }
            else{
                $data->publish = 0;
                $save = $data->save();
            }
            
            if($save)
            {
                return response()->json(['message'=>'success','status'=>'true']);
            }
            else{
                return response()->json(['message'=>'warning','status'=>'false']);
            }
        }
        else{
            return response()->json(['message'=>'errror','status'=>'false']);
        }
    }

    public function product_delete_multiple(Request $req)
    {
        $ids = $req->id;
        $product = Product::whereIn('id', $ids)->where('delete_status',0)->get();
        foreach ($product as $p) {
            if ($p->delete_status == 0) {
                $p->delete_status = 1;
                $p->save();
            }
        }

        return Response($ids);
    }


    public function clone_product($id)
    {
        $ids = decrypt($id);
        $check = Product::where('id',$ids)->where('delete_status',0)->first();
        if($check){
            $insert = new Product;
            $insert->main_category = $check->main_category;
            $insert->category_id = $check->category_id;
            $insert->subcategory_id = $check->subcategory_id;
            $insert->product = $check->product;
            $insert->product_dd = $check->product_dd;
            $save = $insert->save();
            if($save){
                return redirect()->route('view-product')->with('status','Product Cloned Successfully!!..');
            }else{
                return back()->with('errror','Something went Wrong!!..');
            }
        }else{
            return back()->with('errror','Something went Wrong!!..');
        }
    }

        // public function addproduct(Request $req)
    // {
    //     // dd($req->all());
    //     $check = Product::where('category_id',$req->category)->where('subcategory_id',$req->subcategory)->where('publish',0)->get();
    //     $pro =array();
    //     $var =array();
    //     $count=sizeof($req->all());
    //     for($i=$req->inc; $i>=0; $i--)
    //     {
    //         if($i==0){
    //             $pro['label']= $req['label_'.$i];
    //             $pro[''.$req['variant_'.$i] ][]= array_reverse($var);
    //         }else{
    //             $var[''.$req['label_'.$i] ]= explode(",",$req['variant_'.$i]);
    //         }
    //     }
        
    //     $pstatus = array();

    //     for($i=$req->inc; $i>=0; $i--)
    //     {
    //         foreach($check as $s => $key){
    //             $product = json_decode($key->product,true);
                
    //             foreach($product as $p => $value)
    //             {
    //                 if($p == $req['variant_'.$i])
    //                 {
    //                     $pstatus[] = 0;
    //                     $status=0;
    //                     foreach($value as $value1){
    //                         foreach($value1 as $v=>$value2)
    //                         {
    //                             $j = $i+1;
    //                             if($v == $req['label_'.$j])
    //                             {
    //                                 if($value2 == $req['variant_'.$j]){
    //                                     if(!in_array($req['variant_'.$j],$value))
    //                                     {
    //                                         foreach($product[$p] as $last)
    //                                         {
    //                                             foreach($last as $l=>$last1)
    //                                             {
    //                                                 if($last1[0]== $req['variant_'.$j])
    //                                                 {
                                                        
    //                                                     for($copy = $j+1; $copy <= $req->inc; $copy++)
    //                                                     {
    //                                                         foreach($pro[$p][$req['label_'.$copy]] as $per)
    //                                                         {
    //                                                             if(in_array($per, $value1[$req['label_'.$copy]]))
    //                                                             {
    //                                                             continue;
    //                                                             }
    //                                                             else{
    //                                                                 array_push($last[$req['label_'.$copy]],$per);
    //                                                             }
    //                                                         }
    //                                                     }
    //                                                 }
    //                                             }
    //                                         }
    //                                     }
    //                                 }
    //                                 else 
    //                                 {
    //                                     $status = 1;
    //                                 }
    //                             }
    //                             else {
    //                                 continue;
    //                             }
    //                         }
    //                     }
    //                     if($status == 1)
    //                     {
    //                         array_push($product[$p],$pro[$p]);
    //                     }
    //                 }else{
    //                     $pstatus[] = 1;
    //                 }
    //             }   

    //             // save Product if brand exist
    //             $key->category_id = $req->category;
    //             $key->subcategory_id = $req->subcategory;
    //             $key->product = json_encode($product);
    //             // $insert->publish = 0;
    //             $save = $key->save();
    //         }
    //     }
    //     // save Product if brand or category and subcategory does not exist
    //     if(!in_array(0,$pstatus) || !$check->isEmpty())
    //     {
    //         $insert = new Product;
    //         $insert->category_id = $req->category;
    //         $insert->subcategory_id = $req->subcategory;
    //         // $latest[] = $pro;
    //         $insert->product = json_encode($pro);
    //         // $insert->publish = 0;
    //         $save = $insert->save();
    //         if($save)
    //         {
    //             return redirect()->route('add-product')->with('status','Product added Successfully!!..');
    //         }
    //         else{
    //             return back()->with('errror','Something went Wrong!!..');
    //         }
    //     }
    //     else {
    //         return redirect()->route('add-product')->with('status','Product added Successfully!!..');
    //     }
    // }
}

