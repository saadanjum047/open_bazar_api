<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Auth;
use App\Banner;


class BannerController extends Controller
{

    public $banner;
	public function __construct(Banner $banner)
	{
		$this->banner = $banner;
	}
    public function allbanners()
    {
        $data = $this->banner->getall();
        return View::make('banner.index',compact('data'));
    }

    public function add_banner(Request $request)
    {
        if($request->isMethod('post')) {
            if ($request->hasFile('image')) {
                $image = $request->image;
                $rand = rand(1, 1000000);
                $destinationPath = public_path().'/images/banner';
                // Get the orginal filname or create the filename of your choice
                $filename = $rand.date('ydmish').'.'.$image->getClientOriginalExtension();
                $upload = $image->move($destinationPath, $filename);
                $data = $this->banner->storeRecord($filename,'1',$request->sortnumber);  
                return redirect()->route('view-banner')->with('status','Banner successfully added');           
            }
        }
        return View::make('banner.add-banner');
    }

    public function edit_banner($id)
    {
        $data = $this->banner->find(decrypt($id));
        return View::make('banner.edit-banner',compact('data'));
    }

    public function _editbanner(Request $req)
    {
                $id = $req->input('id');
               
                if ($req->hasFile('image')) {
                    $image = $req->image;
                    $rand = rand(1, 1000000);
                    $destinationPath = public_path().'/images/banner';
                    // Get the orginal filname or create the filename of your choice
                    $filename = $rand.date('ydmish').'.'.$image->getClientOriginalExtension();
                    $upload = $image->move($destinationPath, $filename);
                    
                }else{
                    $filename = $req->oldimage;
                }

                $this->banner->updateRecord($id,$filename,'1',$req->sortnumber);
                return redirect()->route('view-banner')->with('status','Banner successfully updated ');  

        }
      
    

    


    public function addbanner(Request $req)
    {
        $pro = $prodd = array();
        $var = $vardd =array();
        $count=sizeof($req->all());
        for($i=$req->inc; $i>=0; $i--)
        {
            if($i==0){
                $pro['label']= $req['label_'.$i];
                $prodd['label'] = $req['labeldd_'.$i];

                if ($req->hasFile('image')) {
                    $image = $req->image;
                    $rand = rand(1, 1000000);
                    $destinationPath = public_path().'/images';
                    // Get the orginal filname or create the filename of your choice
                    $filename = $rand.date('ydmish').'.'.$image->getClientOriginalExtension();
                    $upload = $image->move($destinationPath, $filename);
                    $pro['image'] = $filename;
                    $prodd['image'] = $filename;
                }else{
                    $pro['image'] = 'default.png';
                    $prodd['image'] = 'default.png';
                }

                $pro[''.$req['variant_'.$i] ]= array_reverse($var);
                $prodd[''.$req['variantdd_'.$i] ]= array_reverse($vardd);
            }else{
                $var[''.$req['label_'.$i] ]= explode(",",$req['variant_'.$i]);
                $vardd[''.$req['labeldd_'.$i] ]= explode(",",$req['variantdd_'.$i]);
            }
        }
        $check = Product::where('category_id',$req->category)->where('subcategory_id',$req->subcategory)->where('publish',0)->where('delete_status',0)->get();
        if(!$check->isEmpty())
        {
            $pstatus = array();
            foreach($check as $key)
            {
                $product = json_decode($key->product,true);
                $product_dd = json_decode($key->product_dd,true);
                foreach($product as $p=>$per){
                    for($i=$req->inc; $i>=0; $i--)
                    {
                        if($p == $req['variant_'.$i]){
                            $pstatus[] = 0;
                            $j = $i+1;
                            foreach($per as $v=> $value){
                                for($j= $i+1; $j<=$req->inc; $j++){
                                    if($v == $req['label_'.$j]){
                                        $product[$p][$v] = array_values(array_unique(array_merge($product[$p][$v],$pro[$p][$v])));
                                        $product_dd[$p][$v] = array_values(array_unique(array_merge($product_dd[$p][$v],$prodd[$p][$v])));
                                    }else{
                                        $product[$p][''.$req['label_'.$j]]=$pro[$p][''.$req['label_'.$j]];
                                        $product_dd[$p][''.$req['labeldd_'.$j]]=$prodd[$p][''.$req['labeldd_'.$j]];
                                    }
                                }
                            }
                        }else{
                            $pstatus[] = 1;
                        }
                    }
                }
                $key->product = json_encode($product);
                $key->product_dd = json_encode($product_dd);
                $key->save();
            }

            if(!in_array(0,$pstatus))
            {
                goto label;
            }else{
                return redirect()->route('view-product')->with('status','Product added Successfully!!..');
            }
        }else{
            label:
            $insert = new Product;
            $insert->main_category = $req->main_category;
            $insert->category_id = $req->category;
            $insert->subcategory_id = $req->subcategory;
            $insert->product = json_encode($pro);
            $insert->product_dd = json_encode($prodd);
            $insert->publish = 0;
            $save = $insert->save();
            if($save)
            {
                $subcate = SubCategory::where('id',$req->subcategory)->where('delete_status',1)->first();
                if($subcate)
                {
                    $subcate->next_status = 1;
                    $subcate->save();
                }
                    return redirect()->route('view-product')->with('status','Product added Successfully!!..');
            }
            else{
                return back()->with('errror','Something went Wrong!!..');
            }
        }

    }

  
    public function deletebanner($id)
    {
        $check = $this->banner->where('bannerid',$id)->first();
        if($check)
        {   $delete = $this->banner->where('bannerid',$id)->delete();
            if($delete)
            {
                return redirect()->route('view-banner')->with('status','Banner deleted successfully!!..');
            }
            else{
                return back()->with('errror','Something went wrong!!..');
            }
        }
        else{
            return back()->with('warning','Banner not found!!..');
        }
    }

   
}

