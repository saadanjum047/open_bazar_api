<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Ads;
use App\bids;
use App\signups;
use App\adcounts;
use App\mycoins;
use DB;
use View;
use App\User;
use App\SubCategory;
use App\Product;
use App\PostReport;
use App\FollowUser;
use App\Notifications;

class AdsController extends Controller
{
    // Start Ads Functions
        // Start Ads Function 
            public function all_ads()
            {
                $data = Ads::orderBy('id','desc')->where('delete_status',1)->get();
                foreach($data as $key){
                    $user = User::where('id',$key->user_id)->first();
                    if($user){
                        $key->user_name = $user->name;
                    }
                }
                return View::make('ads.all-ads',compact('data'));
            }
        // End Function

        // Start Ads details function
            public function ads_details($id)
            {
                $ids = decrypt($id);
                $data = Ads::where('id',$ids)->where('delete_status',1)->first();
                if($data){
                    $user = User::where('id',$data->user_id)->first();
                    if($user){
                        $data->user_name = $user->name;
                    }
                    return View::make('ads.ads-details',compact('data'));
                }else{
                    return back()->with('errror','Something went wrong!!..');
                }
            }
        // End Function

        // Start Ad Block Status
            public function ad_block_status(Request $req)
            {
                $data = Ads::where('id',$req->id)->where('delete_status',1)->first();
                if($data)
                {
                    if($data->block_status==0)
                    {
                        $data->block_status = 1;
                        $save = $data->save();
                    }
                    else{
                        $data->block_status = 0;
                        $save = $data->save();
                    }
                    
                    if($save)
                    {
                        return response()->json(['message'=>'success','status'=>'true']);
                    }
                    else{
                        return response()->json(['message'=>'warning','status'=>'false']);
                    }
                }
                else{
                    return response()->json(['message'=>'errror','status'=>'false']);
                }
            }
        // End Function

        // Start Ad Activation status
            public function ad_activation_status(Request $req)
            {
                $data = Ads::where('id',$req->id)->where('delete_status',1)->first();
                if($data)
                {
                    if($data->activation_status==0)
                    {
                        $data->activation_status = 1;
                        $save = $data->save();
                    }
                    else{
                        $data->activation_status = 0;
                        $save = $data->save();
                    }
                    
                    if($save)
                    {
                        return response()->json(['message'=>'success','status'=>'true']);
                    }
                    else{
                        return response()->json(['message'=>'warning','status'=>'false']);
                    }
                }
                else{
                    return response()->json(['message'=>'errror','status'=>'false']);
                }   
            }
        // End Function

        // Start Delete Ads Function 
            public function delete_ads($id)
            {
                $check = Ads::where('id',$id)->where('delete_status',1)->first();
                if($check)
                {
                    $check->delete_status = 0;
                    $save = $check->save();
                    if($save)
                    {
                        return redirect()->route('all_ads')->with('status','Ads deleted successfully!!..');
                    }else{
                        return back()->with('errror','Something went wrong!!..');
                    }
                }else{
                    return back()->with('errror','Something went wrong!!..');
                }
            }
        // End FUnction

        // Start Delete Multiple ads Function
            public function ads_delete_multiple(Request $req)
            {
                $ids = $req->id;
                $Check = Ads::whereIn('id', $ids)->where('delete_status',1)->get();
                foreach ($Check as $p) {
                    if ($p->delete_status == 1) {
                        $p->delete_status = 0;
                        $p->save();
                    }
                }
        
                return Response($ids);
            }
        // End Function
    // End Ads Functions

    // Start Report Functions
        // Start All Reports Function
            public function all_reports()
            {
                $data = PostReport::where('delete_status',1)->orderBy('id','desc')->get();
                foreach($data as $key){
                    $user = User::where('id',$key->user_id)->first();
                    if($user){
                        $key->user_name = $user->name;
                    }
                    $ads = Ads::where('id',$key->post_id)->first();
                    if($ads){
                        $key->ad_title = $ads->ad_title;
                    }
                }
                // dd($data);
                return View::make('reports.all-reports',compact('data'));
            }
        // End Function

        // Start Delete Reports Function
            public function delete_reports($id)
            {
                $check = PostReport::where('id',$id)->where('delete_status',1)->first();
                if($check)
                {
                    $check->delete_status = 0;
                    $save = $check->save();
                    if($save)
                    {
                        return redirect()->route('all_reports')->with('status','Report deleted successfully!!..');
                    }else{
                        return back()->with('errror','Something went wrong!!..');
                    }
                }else{
                    return back()->with('errror','Something went wrong!!..');
                }   
            }
        // End Function

        // Start Multiple Delete Reports Function
            public function reports_delete_multiple(Request $req)
            {
                $ids = $req->id;
                $Check = PostReport::whereIn('id', $ids)->where('delete_status',1)->get();
                foreach ($Check as $p) {
                    if ($p->delete_status == 1) {
                        $p->delete_status = 0;
                        $p->save();
                    }
                }
        
                return Response($ids);
            }
        // End Function
    // End Report Functions

    // Start Coins Function
        // Start All Coins Function
            public function all_coins()
            {
                $data = mycoins::whereNotNull('user_id')->orderBy('id','desc')->get();
                foreach($data as $key){
                    $user = User::where('id',$key->user_id)->first();
                    if($user){
                        $key->user_name = $user->name;
                    }else{
                        $key->user_name = "NA";
                    }
                    if($key->refer_id!=0){
                        $ads = Ads::where('id',$key->refer_id)->first();
                        if($ads){
                            $key->ad_title = $ads->ad_title;
                        }
                    }
                    else{
                        $key->ad_title = "You have invited your friend";
                    }
                }
                return View::make('reports.all-coins',compact('data'));
            }
        // End Functions
    // End Coin Functions

    //Pagination for coin

    public function coin()
    {
        return View::make('reports.all-new-coin');
        
    }
   

    public function allcoins(Request $request)
    {

        
        
       
        $columns = array( 
            0=> "users.name" ,
            1=> "mycoins.tital" ,
            2=> "ad_title" ,
            3=> "mycoins.wallet_type",
            4=> "mycoins.coins" ,
            5=> "mycoins.type" ,
            6=> "mycoins.created_at" ,
          
        );
  
        $totalData = mycoins::whereNotNull('user_id')->count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');

        $or=$request->input('order.0.column');
        if($or!=''){
            $order = $columns[$request->input('order.0.column')];
        }
        else{
            $order="id";
        }

        
      
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {   
             
             $data = DB::table('mycoins')
            ->join('users', 'mycoins.user_id', '=', 'users.id')
            ->select('mycoins.*', 'users.name')
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();
           
        
        }
        else {
             
            $search = $request->input('search.value'); 

            $data = DB::table('mycoins')
            ->join('users', 'mycoins.user_id', '=', 'users.id')
            ->where('users.name','LIKE',"%{$search}%")
            ->orWhere('mycoins.tital', 'LIKE',"%{$search}%")
            ->orWhere('mycoins.type', 'LIKE',"%{$search}%")
            ->offset($start)
            ->limit($limit)
            ->select('mycoins.*', 'users.name')
            ->orderBy($order,$dir)
            ->get();
        
            $totalFiltered = DB::table('mycoins')
            ->join('users', 'mycoins.user_id', '=', 'users.id')
            ->where('users.name','LIKE',"%{$search}%")
            ->orWhere('tital', 'LIKE',"%{$search}%")
            ->orWhere('type', 'LIKE',"%{$search}%")
            ->count();
        }


        foreach($data as $key){
            $user = User::where('id',$key->user_id)->first();
            if($user){
                $key->user_name = $user->name;
            }else{
                $key->user_name = "NA";
            }
            if($key->refer_id!=0 && $key->refer_id!=''){
                $ads = Ads::where('id',$key->refer_id)->first();
                if($ads){
                    $key->ad_title = $ads->ad_title;
                }
            }
            else if($key->type=='credit')
                {
                    $key->ad_title = "Credits Purchased";

                }
            else if($key->type=='upgrade')
                {
                    $key->ad_title = "Account Upgraded";

                }
            else{
                $key->ad_title = "You have invited your friend";
            }
        }

     
        




        $data_user = array();
        if(!empty($data))
        {
            foreach ($data as $post)
            {
            if($post->wallet_type==0) {
                $wallet_type='<span class="uk-badge uk-badge-danger md-bg-blue-grey-600"> Coins </span>'; 
            }
            else{
                $wallet_type='<span class="uk-badge uk-badge-primary md-bg-brown-600"> Credits </span>';
            }

            

            if($post->type=="like") {
                $type='<span class="uk-badge uk-badge-danger md-bg-red-600">'. ucfirst($post->type).'</span>' ;
            }
            elseif($post->type=="post"){
                $type='<span class="uk-badge uk-badge-primary md-bg-purple-600">'. ucfirst($post->type).'</span>';
            }
            elseif($post->type=="invite"){
                $type='<span class="uk-badge uk-badge-primary md-bg-cyan-600">'.ucfirst($post->type).'</span>';
            }
            elseif($post->type=="credit"){
               $type=' <span class="uk-badge uk-badge-primary md-bg-pink-600">'. ucfirst($post->type).'</span>';
            }
            else{
                $type='<span class="uk-badge uk-badge-primary md-bg-teal-600">'.ucfirst($post->type) .'</span>';
            }

           if($post->tital!=''){
            $title=$post->tital;
           }
           else{
               $title="N/A";
           }
           
            $date=date('d-m-Y',($key->timestamp/1000));
            $nestedData['username'] =$post->user_name;
            $nestedData['title'] = $title;
            $nestedData['ad_title'] = $post->ad_title;
            $nestedData['wallet_type']=$wallet_type;
            $nestedData['coin']=$post->coins;
            $nestedData['type']=$type;
            $modal='{target:"#12"}';
            $nestedData['date']=$date;


            $username=htmlspecialchars(json_encode($post->user_name));
            $title=htmlspecialchars(json_encode($title));
            $at=htmlspecialchars(json_encode($post->ad_title));
            $refer=htmlspecialchars(json_encode($post->type));
            $d=htmlspecialchars(json_encode($date));
            
            $nestedData['action']='<a href="#" title="View" id="openmodal" onClick="openModal('.$post->coins.','.$username.','.$title.','.$at.','.$post->wallet_type.','.$refer.','.$d.');"><i class="uk-icon-eye uk-icon-small"></i></a>';

         
        
           
            $data_user[] = $nestedData;

            }

            
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data_user   
                    );
            
        echo  json_encode($json_data); 
        
    }
}
