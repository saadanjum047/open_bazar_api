<?php

namespace App\Http\Controllers;

use Auth;
use App\User;

class LoginController extends Controller
{
    public function login()
    {
        // $role = Auth::user()->role;
        if (Auth::check()) {
            
            return redirect()->route('home');
        } else {
            return view('login');
        }
    }
}
