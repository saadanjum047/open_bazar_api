<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Auth;
use App\Category;
use App\SubCategory;
use App\Product;
use App\User;
use App\Ads;
use App\mycoins;
use App\ContactUs;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     */

    public function terms_of_use()
    {
        echo "hi";
        // return View::make('terms_of_use');
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    

    public function privacy_policy()
    {
        return View::make('privacy_policy');
    }

    public function index()
    {
        $milliseconds = round(microtime(true) * 1000);
        $date = date('Y-m-d');
        $user = User::where('role','!=','admin')->where('delete_status',1)->count();

        $android = User::where('app_type','Android')->where('delete_status',1)->count();
        $ios = User::where('app_type','!=','Android')->where('delete_status',1)->count();

        $ads = Ads::where('delete_status',1)->count();
        $today_ads = Ads::whereDate('created_at',$date)->where('delete_status',1)->count();
        $today_user = User::whereDate('created_at',$date)->where('delete_status',1)->count();
        $today_msg = ContactUs::whereDate('created_at',$date)->where('status',1)->orderBy('id','desc')->count();

        $data = Ads::whereDate('created_at',$date)->orderBy('id','desc')->where('delete_status',1)->orderBy('id','desc')->limit(5)->get();
                foreach($data as $key){
                    $use = User::where('id',$key->user_id)->first();
                    if($use){
                        $key->user_name = $use->name;
                    }
                }
            // for today user commented where('timestamp',$milliseconds)
        $data_user = User::whereDate('created_at',$date)->where('delete_status',1)->orderBy('id','desc')->limit(5)->get();
        // $all = User::where('delete_status',1)->orderBy('id','desc')->limit(5)->get();
        // foreach($all as $key)
        //         {
        //             $coin = mycoins::where('user_id',$key->id)->where('type','!=','redeem')->where('wallet_type',0)->get();
        //             $sum = 0;
        //             $credit = 0;
        //             foreach($coin as $c){
        //                 if($c->wallet_type==0){
        //                     $sum = (int)$c->coins+ $sum;
        //                 }else{
        //                     $credit = (int)$c->coins + $credit;
        //                 }
                        
        //             }
        //             $key->coin = $sum;
        //             $key->credit = $credit;
        //         }       
        return view('index',compact('user','today_ads','today_user','today_msg','ads','data','data_user','android','ios'));
    }


    // public function my_profile()
    // {
    //     $id = Auth::User()->id;
    //     $data = User::where('id',$id)->first();
    //     if($data){
    //         return View::make('my_profile',compact('data'));
    //     }else{
    //         return back()->with('errror','Something went wrong!!..');
    //     }
    // }

    public function add_category()
    {
        return View::make('category.add-category');
    }

    public function _addcategory(Request $req)
    {
        $check = Category::where('en_name',$req->en_name)->where('ar_name',$req->ar_name)->where('delete_status',1)->first();
        if($check){
            return back()->with('warning','Category already exist!!..');
        }
        else{
            // dd($req->all());
            $insert = new Category;
            $insert->main_category = $req->main_category;
            $insert->en_name = $req->en_name;
            $insert->ar_name = $req->ar_name;
            $insert->position = $req->position;
            
            if ($req->hasFile('image')) {
                $image = $req->image;
                $rand = rand(1, 1000000);
                $destinationPath = public_path().'/images';
                // Get the orginal filname or create the filename of your choice
                $filename = $rand.date('ydmish').'.'.$image->getClientOriginalExtension();
                $upload = $image->move($destinationPath, $filename);
                $insert->image = $filename;
            }
            $insert->next_status = 0;
            $insert->publish = 0;
            $save = $insert->save();
            if($save)
            {
                return redirect()->route('view-category')->with('status','Category added Successfully!!..');
            }else{
                return back()->with('errror','Something went Wrong!!..');
            }
        }
    }

    public function view_category()
    {
        $data = Category::orderBy('position','ASC')->where('delete_status',1)->get();

        return View::make('category.view-category',compact('data'));
    }

    public function edit_category($id)
    {
        $ids = decrypt($id);
        $data = Category::where('id',$ids)->where('delete_status',1)->first();
        if($data)
        {
            return View::make('category.edit-category',compact('data'));
        }
        else{
            return back()->with('errror','Something went wrong!!..');
        }
    }

    public function _editcategory(Request $req)
    {
        $data = Category::where('id',$req->id)->where('delete_status',1)->first();
        if($data)
        {
            $data->main_category = $req->main_category;
            $data->en_name = $req->en_name;
            $data->ar_name = $req->ar_name;
            $data->position = $req->position;

            if ($req->hasFile('image')) {
                $image = $req->image;
                $rand = rand(1, 1000000);
                $destinationPath = public_path().'/images';
                // Get the orginal filname or create the filename of your choice
                $filename = $rand.date('ydmish').'.'.$image->getClientOriginalExtension();
                $upload = $image->move($destinationPath, $filename);
                $data->image = $filename;
            }            
            else{
                $data->image = $req->oldimage;
            }
            $save = $data->save();
            if($save)
            {
                return redirect()->route('view-category')->with('status','Category updated successfully!!..');
            }
            else{
                return back()->with('errror','Something went wrong!!..');
            }
        }
        else{
            return back()->with('errror','Something went wrong!!..');
        }
    }


    public function delete_category($id)
    {
        $check = Category::where('id',$id)->where('delete_status',1)->first();
        if($check)
        {
            $sub = SubCategory::where('category_id',$id)->where('delete_status',1)->get();
            foreach($sub as $key)
            {
                $key->delete_status = 0;
                $key->save();
                // $key->delete();
            }

            $pro = Product::where('category_id',$id)->where('delete_status',0)->get();
            foreach($pro as $p)
            {
                $p->delete_status = 1;
                $p->save();
            }


            $check->delete_status = 0;
            $delete = $check->save();
            if($delete)
            {
                return redirect()->route('view-category')->with('status','Category deleted successfully!!..');
            }
            else{
                return back()->with('errror','Something went wrong!!..');
            }
        }
        else{
            return back()->with('warning','Category not found!!..');
        }
    }

    public function status_category(Request $req)
    {
        $data = Category::where('id',$req->id)->where('delete_status',1)->first();
        if($data)
        {
            if($data->publish==0)
            {
                $data->publish = 1;
                $save = $data->save();
            }
            else{
                $data->publish = 0;
                $save = $data->save();
            }
            
            if($save)
            {
                return response()->json(['message'=>'success','status'=>'true']);
            }
            else{
                return response()->json(['message'=>'warning','status'=>'false']);
            }
        }
        else{
            return response()->json(['message'=>'errror','status'=>'false']);
        }
    }


    // Sub Category Functions 
    Public function add_subcategory($id)
    {
        $ids = decrypt($id);
        $data = Category::where('id',$ids)->where('publish',1)->where('delete_status',1)->first();
        if($data)
        {
            return View::make('sub-category.add-subcategory',compact('data'));
        }else{
            return back()->with('errror','Something went Wrong!!..');
        }
    }

    public function _addsubcategory(Request $req)
    {
        $check = SubCategory::where('category_id',$req->category)->where('en_name',$req->en_name)->where('ar_name',$req->ar_name)->where('delete_status',1)->first();
        if($check){
            return back()->with('warning','Sub-Category already exist!!..');
        }
        else{
            // dd($req->all());
            $insert = new SubCategory;
            $insert->category_id = $req->category;
            $insert->en_name = $req->en_name;
            $insert->ar_name = $req->ar_name;
            $insert->position = $req->position;
            
            if ($req->hasFile('image')) {
                $image = $req->image;
                $rand = rand(1, 1000000);
                $destinationPath = public_path().'/images';
                // Get the orginal filname or create the filename of your choice
                $filename = $rand.date('ydmish').'.'.$image->getClientOriginalExtension();
                $upload = $image->move($destinationPath, $filename);
                $insert->image = $filename;
            }
            $insert->next_status = 0;
            $insert->publish = 0;

                $category = Category::where('id',$req->category)->first();
                $category->next_status = 1;
                $category->save();

            $save = $insert->save();
            if($save)
            {
                return redirect()->route('view-subcategory',['id'=>encrypt($req->category)])->with('status','Sub-Category added Successfully!!..');
            }else{
                return back()->with('errror','Something went Wrong!!..');
            }
        }   
    }

    public function view_subcategory($id)
    {
        $ids = decrypt($id);
        $data = SubCategory::where('category_id',$ids)->where('delete_status',1)->orderBy('position','asc')->get();
        foreach($data as $key)
        {
            $category = Category::where('id',$key->category_id)->where('delete_status',1)->first();
            if($category)
            {
                $key->category = $category->en_name;
                $key->main_category = $category->main_category;
            }
        }
        return View::make('sub-category.view-subcategory',compact('data','ids'));
    }

    public function delete_subcategory($id)
    {
        $check = SubCategory::where('id',$id)->where('delete_status',1)->first();
        if($check)
        {
            $pro = Product::where('subcategory_id',$id)->where('delete_status',0)->get();
            foreach($pro as $p)
            {
                $p->delete_status = 1;
                $p->save();
            }
            $check->delete_status = 0;
            $delete = $check->save();
            if($delete)
            {
                return redirect()->route('view-subcategory',['id'=>encrypt($check->category_id)])->with('status','Sub-Category deleted successfully!!..');
            }
            else{
                return back()->with('errror','Something went wrong!!..');
            }
        }
        else{
            return back()->with('warning','Sub-Category not found!!..');
        }
    }

    public function edit_subcategory($id)
    {
        $ids = decrypt($id);
        $data = SubCategory::where('id',$ids)->where('delete_status',1)->first();
        $category = Category::where('publish',1)->where('delete_status',1)->get();
        if($data)
        {
            return View::make('sub-category.edit-subcategory',compact('data','category'));
        }
        else{
            return back()->with('errror','Something went wrong!!..');
        }
    }

    public function _editsubcategory(Request $req)
    {
        $data = SubCategory::where('id',$req->id)->where('delete_status',1)->first();
        if($data)
        {
            $data->category_id = $req->category;
            $data->en_name = $req->en_name;
            $data->ar_name = $req->ar_name;
            $data->position = $req->position;

            if ($req->hasFile('image')) {
                $image = $req->image;
                $rand = rand(1, 1000000);
                $destinationPath = public_path().'/images';
                // Get the orginal filname or create the filename of your choice
                $filename = $rand.date('ydmish').'.'.$image->getClientOriginalExtension();
                $upload = $image->move($destinationPath, $filename);
                $data->image = $filename;
            }            
            else{
                $data->image = $req->oldimage;
            }
            $save = $data->save();
            if($save)
            {
                return redirect()->route('view-subcategory',['id'=>encrypt($req->category)])->with('status','Sub-Category updated successfully!!..');
            }
            else{
                return back()->with('errror','Something went wrong!!..');
            }
        }
        else{
            return back()->with('errror','Something went wrong!!..');
        }
    }

    public function status_subcategory(Request $req)
    {
        $data = SubCategory::where('id',$req->id)->where('delete_status',1)->first();
        if($data)
        {
            if($data->publish==0)
            {
                $data->publish = 1;
                $save = $data->save();
            }
            else{
                $data->publish = 0;
                $save = $data->save();
            }
            
            if($save)
            {
                return response()->json(['message'=>'success','status'=>'true']);
            }
            else{
                return response()->json(['message'=>'warning','status'=>'false']);
            }
        }
        else{
            return response()->json(['message'=>'errror','status'=>'false']);
        }
    }
}
