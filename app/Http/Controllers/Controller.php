<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\notification_id;
use App\Notifications;
use App\Ads;
use App\FollowUser;
use DB;
use App\adboost;
use App\mycoins;
use App\User;
use ImageOptimizer;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  // Add Coin
  public function add_coin($msg,$coin,$user_id,$refer_id,$type,$wallet_type)
  {
    // add coin
    $milliseconds = round(microtime(true) * 1000);
    $addcoins=new mycoins;	
    $addcoins->user_id=$user_id;
    $addcoins->tital=$msg;
    $addcoins->refer_id=$refer_id;
    $addcoins->type=$type;
    $addcoins->coins=$coin;
    $addcoins->wallet_type=$wallet_type;
    $addcoins->timestamp=$milliseconds;
    $addcoins->save();

    return $addcoins;
  }


  public function add_coinv1($msg,$coin,$refer_id,$user_id,$type,$wallet_type)
  {
    // add coin
    $milliseconds = round(microtime(true) * 1000);
    $addcoins=new mycoins;	
    $addcoins->user_id=$user_id;
    $addcoins->tital=$msg;
    $addcoins->refer_id=$refer_id;
    $addcoins->type=$type;
    $addcoins->coins=$coin;
    $addcoins->wallet_type=$wallet_type;
    $addcoins->timestamp=$milliseconds;
    $addcoins->save();

    return $addcoins;
  }

  public function compress_image($image_path)
  {
    // the image will be replaced with an optimized version which should be smaller
    ImageOptimizer::optimize($image_path);
  }

  public function check_count($check)
	{
		if($check)
		{
			$val =  sizeof(json_decode(json_encode($check),true));
		}
		else
		{
			$val =  0;
		}
		return $val;
  }
  

  //  send push notification on adding adds
  public function add_posted_push($id,$massage,$data)
  {
    $followers=FollowUser::where('following_id',$id)->pluck('user_id')->toArray();
    $count = User::whereIn('id',$followers)->increment('notify_count');
    $players_id=notification_id::whereIn('user_id',$followers)->get();
    if (sizeof($players_id)>0) 
    {

      foreach($players_id as $play)
      {
        $SUM = 1;
        $count = User::where('id',$play->id)->first();
        if($count)
        {
          $SUM = ((int)$count->notify_count ?? 0) + $SUM;
          $count->notify_count = $SUM;
          $count->save();
        }

          $content = array("en" => $massage);
          $fields = array(
            'app_id' => 'b31c1980-f8db-47fe-bb76-b29236584dd8',
            'include_player_ids' =>array($play->push_id),
            'contents' => $content,
            'data' =>array("data"=>$data),
            'ios_badgeType'=>'SetTo',
            'ios_badgeCount'=>$SUM,
            'content_available'=>'true'
          );
          $fields = json_encode($fields);

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                        'Authorization: Basic YTU5ZTk3MTktZDE4Mi00NjVhLThiYmItNTU3MzUwZjgwOTc3'));

          curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
          curl_setopt($ch, CURLOPT_HEADER, FALSE);
          curl_setopt($ch, CURLOPT_POST, TRUE);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

          $response = curl_exec($ch);
          curl_close($ch);

          return $response;
      }
    }
  }


//  send push notification
  public function sendPush($id,$massage,$data)
  {
    $SUM = 1;
    $count = User::where('id',$id)->first();
    if($count)
    {
      $SUM = ((int)$count->notify_count ?? 0) + $SUM;
      $count->notify_count = $SUM;
      $count->save();
    }
    $push=notification_id::where('user_id',$id)->select('push_id')->get();
    if ($this->check_count($push)>0) 
    {
      foreach($push as $d)
      {
          // if($d->push_id!='')
          // {
              $tokens[] = $d->push_id;
          // }
      }

        $content = array(

        "en" => $massage

        );

      // print_r($cat_data);
      $fields = array(
        'app_id' => 'b31c1980-f8db-47fe-bb76-b29236584dd8',
        'include_player_ids' =>$tokens,
          'contents' => $content,
          'data' =>array("data"=>$data),
          'ios_badgeType'=>'SetTo',
          'ios_badgeCount'=>$SUM,
          'content_available'=>'true'
        );

        $fields = json_encode($fields);
          //print("\nJSON sent:\n");
        //print($fields);


      $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                    'Authorization: Basic YTU5ZTk3MTktZDE4Mi00NjVhLThiYmItNTU3MzUwZjgwOTc3'));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
        }
  }


  public function add_notification($user_id,$post_id,$otheruser_id,$msg,$type)
  {
    
      $milliseconds = round(microtime(true) * 1000);
      $data=new Notifications;
      $data->refer_id=$post_id;
      $data->user_id=$user_id;
      $data->callby=$otheruser_id;
      $data->tital=$msg;
      $data->type=$type;
      $data->timestamp=$milliseconds;
      $data->save();

  }

  public function delete_notification($callby,$post_id,$type)
  {
      Notifications::where('callby',$callby)->where('refer_id',$post_id)->where('type',$type)->delete();
  }


  public function getadposts($user_id,$id)
  {
  
    $date = date('Y-m-d');

    $data=Ads::orderBy('id', 'desc')->whereIn('id',$id)->where('block_status','0')->where('delete_status',1)->where('activation_status','0')->where('mobile_verify','true')
    ->get();
   

    foreach($data as $d)
    {
      $boost = adboost::where('post_id',$d->id)->where('status',0)->whereDate('expiry_date','>',$date)->first();
      if($boost)
      {
        $d->days = $boost->days;
        $d->boost_type = $boost->boost_type;
      }
      else {
        $d->days = "";
        $d->boost_type = "";
      }
    $user=  DB::table('users')
              ->leftJoin('signups', 'signups.user_id', '=', 'users.id')
              ->select('users.id','users.name','users.profile_image','users.created_at')
              ->where('users.id',$d->user_id)->where('delete_status',1)->first();
    // 'signups.ads_count'
    $user_adcount=Ads::where('user_id',$d->user_id)->where('activation_status',0)->where('block_status',0)->where('delete_status',1)->count();
    $d->ads_count = $user_adcount;
    // follow user -------------
      $follow_count = FollowUser::where('user_id',$user_id)->where('following_id',$d->user_id)->count();

        $follow_user='false';
          if ($follow_count>0) {
            $follow_user='true';
          }
        $d->follow_user = $follow_user;

    // // follow userr end----------


      if($user)
      {
        $d->user_name = $user->name;
        $d->user_image = $user->profile_image;
    
        $d->user_regiterdate = $user->created_at;
        // $d->ads_count = $user->ads_count;

      }
      $d->favorite_count=0;
      $d->views_count=0;
      $d->like_count=0;
      $d->fav_stattus='No';
      $d->like_stattus='No';

      $like_datas=DB::table('adcounts')->where('post_id',$d->id)->get();
        $favorite_count=0;
        $views_count=0;
        $like_count=0;
        $fav_stattus='No';
        $like_stattus='No';
        $view_stattus='No';
       
      foreach ($like_datas as $like_data) 
      {
        if ($like_data->favorite==1) 
        {
          $favorite_count++;
        }
        if ($like_data->views==1) 
        {
          $views_count++;
        }
        if ($like_data->likes==1) 
        {
          $like_count++;
        }

        if ($like_data->user_id==$user_id) 
        {
          if ($like_data->favorite==1) 
          {           
            $fav_stattus='Yes';
          }
          if ($like_data->likes==1) 
          {           
            $like_stattus='Yes';
          }

           if ($like_data->views==1) 
          {           
            $view_stattus='Yes';
          }
         
        }

        $d->favorite_count=$favorite_count;
        $d->views_count=$views_count;
        $d->like_count=$like_count;
        $d->fav_stattus=$fav_stattus;
        $d->like_stattus=$like_stattus;
		$d->view_stattus=$view_stattus;
      }
      
    }

    
          return $data ;
       
  }


//  send push notification
  public function sendPush_chat($id,$massage,$data)
  {
   
    $push=notification_id::where('user_id',$id)->select('push_id')->get();
    if ($this->check_count($push)>0) 
    {
      foreach($push as $d)
      {
          // if($d->push_id!='')
          // {
              $tokens[] = $d->push_id;
          // }
      }

        $content = array(

        "en" => $massage

        );

      // print_r($cat_data);
      $fields = array(
        'app_id' => 'b31c1980-f8db-47fe-bb76-b29236584dd8',
        'include_player_ids' =>$tokens,
          'contents' => $content,
          'data' =>array("data"=>$data),
          'content_available'=>'true'
        );

        $fields = json_encode($fields);
          //print("\nJSON sent:\n");
        //print($fields);


      $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                    'Authorization: Basic YTU5ZTk3MTktZDE4Mi00NjVhLThiYmItNTU3MzUwZjgwOTc3'));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
        }
  }


}
